# CS 373 Software Development Technical Report for the Budget for Space Web Application

## Document Contents
1.    Introduction –  
    a.    Motivation  
    b.    Phase 1  
    c.    Phase 2
2.    User Stories –  
    a.    Phase 1 User Stories to Developers  
    b.    Phase 1 User Stories from Customers  
    c.    Phase 2 User Stories to Developers  
    d.    Phase 2 User Stories from Customers  
    e.    Phase 3 User Stories to Developers  
    f.    Phase 3 User Stories to Customers
3.    RESTful API –  
    a.    Documentation  
4.    Models –  
    a.    The Missions Model  
    b.    The Destinations Model  
    c.    The Organizations Model  
5.    Tools –  
6.    Hosting –  
7.  Pagination –
8.  Searching -
9.  Filtering -
10. Sorting -
11. Testing –
12. Database –  
    a.    UML Diagram  
13. Visualizations -
14. Critiques -
    a. Self-Critiques
    b. Critiques to Our Provider


## 1. Introduction
This technical report entails the intricacies of the Budget for Space project. This project was implemented for CS373: Software Engineering by Alan Zeng, Anthony Liu, Bill Yang, Michael Molash, and Sahil Parikh. The domain is https://www.budgetfor.space.

### Motivation
As climate change becomes an increasing concern for the future of our planet, space exploration is more important than ever. However, government support for space-related technologies continues to fall. In order to increase public awareness about this lack of governmental concern, we decided to create a web application that'd display information related to space exploration and technologies. Users will be able to explore historical and current data on missions to space, the organizations involved in these missions, and the destinations traveled to. The missions shown will focus specifically on the exploration of potential solutions to major problems (climate change, overpopulation, etc.) that may no longer be viable to solve on earth.

### Phase 1
The web application we’re building is a semester-long project that will be completed in four phases, with each improving on the previous stage with some sort of new functionality and robustness. This technical report will primarily focus on the first phase. The first phase focuses primarily on collecting three instances of each of our three models, designing a RESTful API using Postman, creating a static website that’s hosted on Amazon Web Services (AWS), supporting https, deriving stats from GitLab dynamically, and using Bootstrap as a CSS framework. Our public GitLab repository can be accessed on [GitLab](https://gitlab.com/mmolash/budgetfor.space).

### Phase 2
Phase 2 of this project involved making our webpage more dynamic. The first component involved collecting data on many instances of our models. After collecting this data, we stored it in a database and wrote a server which queries our database to provide an API. Finally, we modified the frontend website to render the data from the API.

## 2. User Stories

### Phase 1

#### User Stories Provided to Developers
**User Story 1 –  Inconsistent Stylings**  
The webpage had a few CSS inconsistencies that are a tad unclean to look at. While it is minor changes in font, font-weight, and font-size, this can still result in poor user experience. This was likely due to the fact that multiple members were creating the websites in parallel, with different stylings. Having a more universal CSS setting for content that should remain consistent among all pages, such as Header fonts (and their weight/size) is necessary for a good looking website. Overall, it shouldn't be too difficult to fix provided that the initial implementation of styling isn't messy.  
*Estimated Completion Time: 20 minutes*    

**User Story 2 –  Make listing pages into multiple columns**  
Currently, the website displays each instance of a model in one column, with a long card. Perhaps it would be cleaner to shrink the cards and put them into 3 columns in order to show more information for less real estate. This isn't an absolutely vital change, but I would imagine as the website scales up it would be easier for the user to consume the information rather than having to page through many different pages.  
*Estimated Completion Time: 20 minutes*    

**User Story 3 –  Add a Map to Spaces page**  
What would be really nice is for local users to be able to pinpoint Spaces to go to on the map. Sometimes you just feel like checking out the closest Space, or perhaps you would want a relative idea of how far a certain Space is from your current location. Regardless, having a map makes navigating and viewing the spaces page so much quicker rather than having to manually check every instance for its location, and then having to calculate that distance (especially for users that don't have street addresses memorized).  
*Estimated Completion Time: 25 minutes*

**User Story 4 – Make landing page responsive to rescaling**  
As a user with some old mobile devices, I'd like the landing page to be more responsive so that I can view the page better on all of my devices. Specifically, the landing page image and title scale horizontally, but do not scale with browser height, making it hard to see all of the information presents on the landing page. If the font resized and the image height shrunk, it would the control flow would make a lot more sense to me.  
*Estimated Completion Time: 15 minutes*

**User Story 5 – Add functionality or remove search bars**  
As a user who really likes to interact with everything on the page, I'd like your search bars to have some effect or be removed. Specifically, your search bars on the Events, Groups, and Spaces pages do not do anything when used. In this phase of the project, it doesn't seem like they'd be super useful, but if they're there, I think that they should have some effect.  
*Estimated Completion Time: 10 minutes to remove, 2 hours to add functionality*

#### User Stories Received From Customers

**User Story 1 – Destination Significance**  
According to your About page, the purpose of your website is to encourage space travel as a solution (?) to climate change. To that end, it would be nice to maybe include for each destination how getting there would make progress toward this goal. For example, I get why the moon would be a logical destination for this purpose as a second place to inhabit, but I don't really see why getting into sub-orbit or geostationary transfer orbit would be significant.  
*Estimated Completion Time: 30 min*


**Response:** Completed with commit #64f5c014. Added significance manually as a new section for each instance. Unsure if this request will be viable dynamically, as going forward, finding APIs with the significance might not be possible.  
*Actual Completion Time: 15 minutes*

**User Story 2 – Destinations Details/Organizations not Loading (User Story)**  
As a user, it would be cool to see how the different destinations would be possible opportunities in the future. If we at some point need to leave the earth, what are the advantages and disadvantages for each? What problems does each destination solve? How much would each cost (hypothetically)? That will probably require some more research, but I think that those would be important for getting us users want to actively engage in budgeting for space.
On a more functionality note, the Organizations page doesn't load, but it seems like you have stuff for it based on the links in your Destination pages. That shouldn't be too hard to add/update.  
*Estimated Completion Time: 1-2 hours*

**Response:** Part 1: Unreasonable, our APIs won't cover the scope of being able to get opportunities about the destinations. The site is more about providing the user about current information that they can use to inform themselves on the state of things. Current space exploration hasn't quite reached the point where we can see which planets are viable.
Part 2: Resolved, added organization page as asked.
*Actual Completion Time: 1 hour*

**User Story 3 – Improve aesthetics for links, complete the Organizations page**  
I like the overall feel of the website, but I'd suggest definitely making the links look better. Rather than just a small blue font, I would try and mix it up more to match the theme. Furthermore, I suggest completing the Organizations page. Lastly, I would make a logo to go along with the title.
*Estimated Completion Time: None provided*

**Response:** Link aesthetics changed (increased font size, we feel like the blue matches the theme), organizations page completed per another User Story. Not enough time to create a logo. See User Story 2 for Organization page.
*Actual Completion Time: 5 minutes*

**User Story 4 - Search-ability for missions**  
It would be cool if you could search through missions by destinations. A search bar at the top would be great. This way a user could specifically look at missions that are related to places a user is interested in.  
*Estimated Completion Time: 3 hours*

**Response:** The functionality for this user story was added by creating a search bar on each
of the pages that hold lists of models. The search bar dynamically takes user
input and updates the models listed on the page if the name or description of
that model contains the typed in word/phrase. The feature was expanded by
including it for all models, rather than just the mission models. However,
it was also changed to search specifically through the name and description
rather than for destinations. The functionality was simple to add because
we already construct each "card" on the page with a loop, so an if statement
was added that only constructs that component if the search term is contained
within the information for that model.  
*Actual Completion Time: 30 minutes*

**User Story 5 - Navbar behavior**  
On mobile when I open the navbar it will open the nav options but scrolling only hides the top navbar. Which takes up a large part of the screen when I want to read. And then it becomes harder to hide it because the sandwich menu is hidden. I would like to see the nav options slowly hide away the further I scroll down. Same behavior as the navbar. Also, a good idea to fix centering, your postman link is messing it up.  
*Estimated Completion Time: None provided*

**Response:** The functionality for this user story was added by automatically closing the navbar
when a link is clicked by the user. This allows the user to access all of the options
in the hamburger bar menu without impacting their ability to read the page that
they navigate to. This was simple enough to implement because of the built-in
features of bootstrap that implement that functionality.  
*Actual Completion Time: 5 minutes*



### Phase 2

#### User Stories Provided to Developers

**User Story 1 - Fix links to an external page**  
In the spaces category, the external links are broken. When you click it, it takes you to something like
"https://catchmeoutside.today/spaces/http://austintexas.gov/department/zilker-metropolitan-park"
and nothing pops up.  
*Estimated completion time: 30 minutes*

**User Story 2 - Multimedia**  
I'm not sure if this is available but aside from pictures, it'd be cool to see different forms of media like videos of people doing an activity in a space, etc.  
*Estimated Completion Time: 2 hours*

**User Story 3 - More attributes on each instance of the model page**
Right now, there aren't many attributes in each instance. For example, in the spaces instances, the information seems to be a little sparse. There's currently only a picture, location, phone number, website, and then links to the other models. Seeing more information for each instance would be good to see (e.g. maybe the history of the space, etc.).
I'm not sure if data for more attributes are available though.  
*Estimated Completion Time: 3 hours (finding the data may be time-consuming)*

**User Story 4 - Carousel on Home Page**  
On the home page, there's only one picture of people kayaking. It'd be cool to see a carousel implemented on the home page, scrolling through various pictures of the different possible activities, etc. that can be done around in Austin!  
*Estimated Completion Time: 1 hour*

**User Story 5 - Picture Alignment on the About Page**  
For the photos of the team members, the pictures are not aligned with one another (i.e. the white space above a photo differs for each person). It'd look a lot cleaner if the pictures were aligned.  
*Estimated Completion Time: 1 hour*

#### User Stories Received From Customers

**User Story 1 - Multimedia Improvements Possible**  
One thing that I think could really improve the website as a user is the use of more multimedia in your instance pages. Right now, there is only one picture per page. I think in the future you could have multiple making it more fun, and for things like the Apollo 11 mission, add Armstrong's "one step" quote as audio (videos are another good thing).  
*Estimated Completion Time: None provided*

**Response:** The bottleneck on improving the multimedia is finding APIs that support the kind of multimedia mentioned by the customer. We tried our best with finding the multimedia but did not come up with the most-desired results. We settled for adding a lot more images to each instance as well as adding some map/routing data. The customer later suggested newsfeed as another example of multimedia, which is a great idea! We looked into it, and if we don't get to it, we'll definitely try to implement it in the next phase of the project.  
*Our Estimate: 2 hours*;  
*Actual Completion Time: in progress*

**User Story 2 - Model Grid System**  
Models would be easier to view if the instances were in a grid pattern. About 3 across and 5 down would be good. This would lead you to create pagination too.  
*Estimated time: 2 hours*

**Response:** We have migrated our bootstrap Grid into Material-UI Grids and Cards. This provides a cleaner look and would better allow the instances to be viewed. Pagination was implemented as well, and we went for a 3x3 grid as it feels like better user experience.  
*Actual time: 2 hours*

**User Story 3 - Links to Related Instances**  
I see that you currently have carousels on each instance page titled with the names of the two other models so maybe you already planned on doing this, but it might be nice to actually have links to related instances. For example, the Nasa organization instance page, have a list of the missions that Nasa has carried out with links to the pages of those mission instances if that makes sense.  
*Estimated time: 1-2 hours*

**Response:** The links will be dynamically updated. Since we were overhauling our backend to be dynamic(and have the frontend reflect that) we used prop images and links. Now that our database is all connected, each instance will dynamically have links to the two other models based on the database.  
*Actual time: 3 hours*

**User Story 4 - Links to Tools**  
It would be nice if, on the about page, there was a convenient link to the different tools that you used. This would make it easier for us to press on it and see why these tools were important. Documentation or references would work.  
*Estimated time: 30 minutes(by us, none given from user)*

**Response:** Done. Each tool on the about page now links to its respective website.  
*Actual time: 20 minutes*

**User Story 5 - Add Pagination**  
Also, the current website has mostly 404's  
*Estimated time: 1 hour(by us, none given from user)*

**Response:** The prod and dev websites at the time was going through AWS overhauls to account for API endpoints. Pagination has been implemented on top of Material-UI cards. We did not use a 3rd party component, rather we custom built our own to suit the website better.  
*Actual time: 1 hour*

**User Story 6 - API Documentation**  
https://www.budgetfor.space/api
It would be nice if the objects would stay on the screen longer. Currently, they just fall and disappear  
*Estimated Time: Not Given*

**Response:** Since this was a 3rd party component, we currently don't feel like this is a needed feature to spend time on. Contributing to their open source or digging through the node_module would be more work versus the expected results.  
*Actual time: N/A*

### Phase 3

#### User Stories Provided to Developers

**User Story 1 - Sort by distance**  
As a user who would like to prioritize events and spaces that are close to me, it would be nice if I could sort specifically based on distance for those two models. This would allow me to make better-informed decisions based on your data without having to search through many pages.  
*Estimated Completion Time: 1 hour*

**User Story 2 - Event Tags**  
I really like the idea of the tags on different events so that I can quickly get a better idea of what to expect out of it. However, it currently looks like every event has the same tags (Fitness, Community & Environment, and Outdoors & Adventure). It would be nice for these labels to be more accurate!  
*Estimated Completion Time: 2 hours*

**User Story 3 - Past Event Indication**  
As a user who quickly scans through events, it was confusing for me to see some events that had already occurred still at the front of the events list. I understand the difficulty of modifying the database to remove events when they occur, but it would be nice to move them to the back on the front end or clearly indicate that they have already passed.  
*Estimated Completion Time: 1 hour*

**User Story 4 - Searching**  
As someone who really likes to get information fast, it would be super cool if there was a search bar I could use to search your entire website for events, spaces, and groups that might interest me. The results could look like google's highlighting the places in your data where my search term occurs.  
*Estimated Completion Time: 6 hours*


**User Story 5 - Filter Based on Group Labels**  
As a user who has very specific interests (and only wants to pursue those interests), it would be nice for me to be able to filter groups based on the labels shown on the model list page. For instance, I am interested in groups focused on Food & Drink, but not on those focused on Parents & Family. I would love to be able to have boxes I could checkmark to indicate which types of groups I want to see and which I do not.  
*Estimated Completion Time: 3 hours*

#### User Stories Provided from Customers

**User Story 1 - Style Issues**  
On each of the model pages, the attributes are all lower case. This doesn't seem to go well with the rest of the page, and should probably start with an uppercase letter. Also, related instead of crew being false or true it should be manned or unmanned, and instead of num-missions it should be Mission Count or # of Missions.  
*Estimated Time: 45 mins (given by us)*

**Response:** Thanks for another story. Definitely agree that the lowercase doesn't seem to fit with the theme of the rest of the website. These stylings are a large part of the time crunch before the last deadline, so we will definitely work to make them look better and be more clear this time around.  
*Actual time: 30 mins*

**User Story 2 - Sorting Model Pages**  
I am not sure what the order is behind each of the model pages. It would be cool to be able to add some type of ordering system, where I could see the biggest organizations, or the most recent missions, etc. Adding those could make it much easier to browse your website.  
*Estimated Time: 3 hours (given by us)*

**Response:** We had been working on this feature, and it should be complete as of commit 9a65fbb4. The sorting allows you to sort in ascending or descending order on any of the attributes for each model. Feel free to take a look at the code on branch search/michael. Otherwise, we will let you know when it's merged into our master branch.
Since this was completed (though not pushed) before this story, I will add estimates for what I thought it would take and how long it actually took.  
*Actual time: 1 hour 30 mins*

**User Story 3 - Logo**  
Less important than the others, but it would be cool if there was some type of logo showing off your brand. Having a recognizable image could help with remembering your organization. Putting it on the top left corner with a link to your home page could be a good way to do it.  
*Estimated Time: 20 minutes (given by us)*

**Response:** Definitely agree that a logo is essential to our brand. We already have a very small logo as our favicon, but we will add it to the navigation bar next to our website name to reinforce the brand concept.  
*Actual time: 15 mins*

**User Story 4 - Filtering on Model Pages**  
I think that it would be really cool to be able to filter the results such as see only organizations from a specific country or with at least a certain amount of missions. For missions, a good filter could be manned or unmanned. For destinations, perhaps the type of destination or the date discovered.  
*Estimated Time: 5 hours (given by us)*

**Response:** Hi Sahil, thank you so much for the user story. We are currently working on implementing a filtering feature on our website. It will allow you to filter based on any of the attributes on the list pages. We anticipate it to be something like you would find on Amazon, where you can select certain attributes you want or don't want.  
*Actual time: 5 hours*

**User Story 5 - Missing search bar**  
Your really awesome search bar from an earlier user story has disappeared, please bring it back. Can you try to have it search upon hitting enter, rather than as the user is typing? I remember the functionality of the first one would disappear as soon as the user hit enter, which is kind of counter-intuitive because of how most search bars work. Thank you! :)  
*Estimated Time: 4 hours (given by us)*

**Response:** Thank you for the user story, Brandi. We're currently working on an implementation of a search bar for model lists and will let you know when it is done so you can give some feedback!
Will keep in mind the behavior you specified because I agree it didn't make sense before. The searching is now live on the website. Let me know what you think!  
*Actual Time: 2 hours 30 mins*

## 3. RESTful API

### Documentation
For this portion, we used Postman to create documentation for the API. We created 6 GET requests and 3 POST requests, detailed the possible parameters, and published the docs via the Postman service. The specific documentation is available [here](https://documenter.getpostman.com/view/6487730/S11LrcXh). The general idea of the API is that users will want to be able to search through lists of each type of model based on certain features, select one of the matching instances, and drill deeper into that specific instance. Because of this, three endpoints are provided to search instance lists based on features, and three more are provided to get specific information about an instance given the `id` of the instance which is provided by the list endpoints. We also made the choice, for our sake, to make a default *pagination* feature within the API which allows users to select how many results they want per page and which page they want. Each endpoint is discussed briefly below:

**/missions**  
This endpoint allows you to get a list of missions that match certain, optional feature parameters (name, year of launch, whether it was manned, its status, the rocket it launched on, and the country it launched from).
```
GET http://api.budgetfor.space/missions?name=Apollo+11
```

**/organizations**  
This endpoint similarly allows you to get a list of organizations that match optional feature parameters (in this case, name, nationality, whether it is public or private, year founded, number of missions, and number of employees).
```
GET http://api.budgetfor.space/organizations?public=true
```

**/destinations**  
This is the last of these endpoints that allow optional feature matching (name, type of object, distance from earth, date discovered, composition, and radius).
```
GET http://api.budgetfor.space/destinations?distance=1000,10000
```

**/mission, /organization, /destination**  
Each of these endpoints is used to find more specific information about a single instance of a model. Each takes an `id` parameter which corresponds to that single model instance.
```
GET http://api.budgetfor.space/mission?id=15
```
Each of these endpoints also allow for POST requests as well to get the specific
information for one instance. It takes an `id` parameter, `name` parameter, or both. If an id and name
are both specified, then they must both match the same instance.
```
POST http://api.budgetfor.space/mission
body = {
  'id': 1,
  'name': 'Nuclear Spectroscopic Telescope Array (NuSTAR)'
}
```


## 4. Models
We have three distinct models designed in an IMDB style web application.

### The Missions Model
This model represents the various space missions that have taken place, with each different mission being a different instance of the model. For each instance of the model, there’s a picture as well as some information about the mission itself such as a brief description, the people (crew) involved, etc. Each model instance is linked to its corresponding destination and organization instance.

**Launch/Mission Data**  
This data was collected from the [launchlibrary](https://launchlibrary.net/docs/1.4/api.html) API. This was a very rich API and contained many of the official mission attributes like name, launch date, country, rocket. Additionally, it provided a Wikipedia link to for each mission, which was then parsed to locate destination data.

Given the URL of the Wikipedia page, we used the Wikipedia API to pull that page and used BeautifulSoup (a Python library) to parse the destination data and crew data from it (specifically from the data table that is on the right-hand side of Wikipedia pages). We also grabbed all images on the Wikipedia page to use as multimedia.

**Status**  
Status was determined dynamically during the data collection by getting today's date, creating a custom Date object in Python, and then loading in the launch start and launch end dates provided by the API. Then, the dates were compared using a custom compare function to determine if the launches were in the past, future, or ongoing. This will be updated to dynamically refresh on the page in a later iteration.

The model attributes are:
* Name
* Date (Launch Date)
* Crew
* Status
* Rocket
* Country
* Destinations
* Organizations

### The Destinations Model
The destinations model represents the various destinations in space that missions have made their way to. It contains relevant information about the target location or type of orbit. This model is connected to the mission instance with this particular destination as well as the instance of the organization involved in making the mission happen.

The data for this model was collected using 4 API's to get the 4 types of data. The 4 types of data we retrieved were exoplanets, minor planets (i.e. asteroids), meteorites, and orbits/some planets relevant to our solar system.

**Exoplanet**  
We got exoplanet data from [exoplanetarchive](https://exoplanetarchive.ipac.caltech.edu/cgi-bin/nstedAPI/nph-nstedAPI?table=exoplanets) API. The exoplanet archive provided us the name, distance, date-discovered, radius, and orbital period. It was in different units, so we simply converted it to the units we were using.

The URL of the Wikipedia page was not provided, so we used Python's Wikipedia import to perform a search on the name. The first result is the most closely related Wikipedia page, so we used that and grabbed the images on the Wikipedia page to use as multimedia.

**Minor Planets**  
For the minor planets, we pulled data from [minorplanetcenter](https://minorplanetcenter.net/web_service) API. The minorplanetcenter provided us the name and orbital period of the asteroids. A lot of the other information was not available, and we couldn't search for images of these asteroids because a lot of them have not been named yet.

**Meteorites**  
For the meteorites, we pulled data from [NASA](data.nasa.gov) API using Socrata. NASA provided us the name and date the meteorite was discovered. Some of the meteorites have not been named yet, however. The NASA API did not provide us the information for the other parameters. Because the meteorites were not consistently named, we were unable to pull images from Wikipedia.

**Orbits/Planets Relevant to our Solar System**  
For the orbits and planets relevant to our solar system, we pulled data from [Wikipedia](https://en.wikipedia.org/w/api.php). Luckily, the orbits and planets in our solar system have been more extensively researched than the other type of destinations, so the Wikipedia page provided quite a lot of content. We used Python's Wikipedia import to look up the different orbits and planets. Wikipedia provided us the name, distance, date-discovered, composition, radius and orbital-period when applicable (i.e. for orbits they don't have composition). If there were any images on the Wikipedia page, we used those as the multimedia.

The model attributes are:
* Name
* Type
* Distance
* Date Discovered
* Composition
* Radius
* Missions
* Organizations

### The Organizations Model
The organizations model represents the different space organizations that have made/are marking notable contributions towards space exploration. This model contains information about the organization. Each instance of a destination model is connected to corresponding instances of destinations and missions that involve this particular organization.

The data for this model was collected mainly from the [launchlibrary](https://launchlibrary.net/) API. In particular, we selected organizations that are launch service providers (LSPs) because they would have missions associated with them. The launchlibrary API provided the names, nationalities, and whether each was public or private. In addition, they provided external links (to social media, websites, etc.) and the URL of the Wikipedia page.

Given the URL of the Wikipedia page, we used the Wikipedia API to pull that page and used BeautifulSoup (a Python library) to parse the founding year and number of employees from it (specifically from the data table that is on the right-hand side of Wikipedia pages). We also grabbed all images on the Wikipedia page to use as multimedia.

Finally, the number of missions was calculated based on the mission models that we collected which related to the specific organizations.

The model attributes are:
* Name
* Nationality
* Public/Private
* Year Founded
* Number of Missions
* Number of Employees
* Missions
* Destinations

## 5. Tools

There were several tools that were utilized in order to implement the first phase of our web application.

### Amazon Elastic Beanstalk
Elastic Beanstalk was used to host our web server and do automatic load balancing.

### Amazon RDS
RDS was used to launch and host our PostgreSQL database.

### Axios
Axios was used as a tool for our React frontend to make requests to our API.

### Bootstrap
Bootstrap was used for some styling elements on our website.

### Docker
Docker was used for development and CI purposes to ensure a shared environment.

### Flask
Flask was used to create the server that is running on Elastic Beanstalk.

### Flask Restful
Flask Restful was used to help in creating our REST API on the Flask server.

### Flask CORS
Flask CORS was used to allow cross-origin requests so that our front end could access the data from our API.

### GitLab
GitLab was used to host our code, track issues, communicate with our customers, and perform continuous integration and deployment.

### Grammarly
Grammarly was used to ensure our technical report had good grammar and spelling.

### MaterialUI
MaterialUI was also used for some styling elements on our website.

### Mocha
Mocha was used to create tests for our front end.

### Namecheap
Namecheap was used to acquire our domain name and create CNAME records to allow AWS to host the content on it.

### Postman
Postman was used to define our API documentation and write tests of our API.

### React
React was used to create the front end client (website).

### Slack
Slack was used as a communication tool for our development team.

### Selenium
Selenium was used to perform GUI tests.

### SQLAlchemy
SQLAlchemy was used to allow our backend server to communicate with our database, as well as to push our collected data to our database.

## 6. Hosting
The website was hosted on AWS. This not only saved us the trouble of having to set up our own server, but it also gave us scalability should the amount of data we store needs to increase or if web traffic increases.  

We use one central IAM user with permissions to all of our AWS services. All services are hosted in the West Virginia region except for Elastic Beanstalk and Certificate Manager for EB.

We use an S3 bucket to host our website. It holds all the source files and other assets used in our frontend. It has public read access. This allows us to handle the distribution of the front end with CloudFront.  CloudFront was used to verify the domain and configure https. Certificate Manager is used to create a certificate for our domain, budgetfor.space, to use in CloudFront. Our domain is hosted through Namecheap. We added CNAMES, for our CloudFront and Elastic Beanstalk.

Elastic Beanstalk hosts our backend python flask server. It has its own separate Certificate for api.budgetfor.space subdomain and added CNAME in Namecheap as well. There is also a listener in the Load Balancer to configure https settings for the subdomain to serve the server.

AWS RDS is used to host our PostgreSQL database. Nothing special with the hosting was used here. We created a separate IAM user with only read access to RDS, to use in our server. This way we are not concerned with the public nature of this IAM user since we are allowing our database to be public.

## 7. Pagination

### Process
There are 9 instances per page and regardless of whichever model you go to, it starts off on page 1. As a user clicks onto another page, the instances associated with that page are grabbed and loaded onto the page. The pages are shown at the bottom and the instances are shown on the page are updated each time a user clicks onto a different page.

### Tools
The tools used to implement pagination were React and CSS. We used React to build the UI component of handling going from one page to another and loading the instances corresponding to each page. Once we grabbed the instances for each page, we used CSS to style/format the data in an aesthetic way.

### How it Works in the Code
In the constructor, we instantiate the current page ("currentPage"), the number of instances per page ("instancesPerPage"), and how we handle clicks onto the different pages ("this.handleClick"). To be more specific on how we handle the clicks, we write a method that simply changes the current page we're on to be the page number the user clicks on. The various instances we have of a particular model are stored in "previewCards". Based on the number of instances available, we do the math to determine the total number of page numbers and we push those values into "pageNumbers". The "pageNumbers" are in a div class "pages" that we wrote in CSS that simply ensures that the page numbers appear side by side as opposed to appearing on top of one another. Using "pageNumbers" as a map, "renderPageNumbers" simply takes all the page numbers and maps information for them (i.e. key, id, etc.). We have a variable, "renderInstances" that takes in the instances for a page and just pushes all the instances; because we know how many instances there are per page, we can figure out what range of instances we need for any given page, so "currentInstance" grabs the slice of "previewCards" that we need for the page and passes it into "renderInstances".

When we return the model, we load all the instances and place them in a grid format, and we put the page numbers at the bottom of the page as a "footer", a div class we wrote in CSS to help format the page numbers (i.e. that they appear on the bottom, color, etc.).

It's fairly straight forward to change things if necessary. For example, if someone wanted to change the number of instances per page, all they would have to do is change what we instantiate the number of instances per page ("instancesPerPage") to be.

## 8. Searching

### Process
There are two places that searching is allowed on our website. First, on the landing page, there is a search bar that allows a user to search across all three models and see results from each. Second, a user can search on an individual model list page. This will return only results for that single model. The search itself will look over the name and description of any of the models.

### Tools
Searching was implemented completely on the front end because it was the simplest and cleanest way to add it given our current code. Because of this, it was built using React, with the search results styled with CSS.

### How it Works in the Code
Searching works largely the same for site-wide search and for each model list page. Essentially, whenever a search is made, React makes a get call to our server to get all of the model data again. It then takes the value currently in the search box and splits it into words. Duplicates are removed (by creating a set) so redundant searching is not done. We then loop over every search term (word) and every instance of the models, checking if the `indexOf` the element is 0 or greater (implying that it exists somewhere in the word). The search is also case insensitive because we convert all data to lowercase. If the search term does exist in the data, we record the index where it occurred and the length of the search term (useful later for highlighting). This data is stored in a list inside of the model data itself. In addition, before actually storing the data, we check to see if there already exists a search index that overlaps with our current search index. For instance, if in the word "unanimous", we searched for "una" for "animous", we would normally get two search indices, one at 0 of length 3 and one at 2 of length 7. However, since we find the existing index, it would be merged into one index at 0 of length 9.

When we actually render the data on the page, we look to see if there are indices recorded for the name or the description. If so, that part of the name and description are wrapped in a span that has the highlighting styles applied to them.

The only difference for the site-wide search is that the entire process is done in a loop that goes over every type of model.

## 9. Filtering
Filtering was implemented by modifying the active state in the frontend. Since there's only a limited number of options to filter on(that are consistent with our database schemas), we can hardcode the filtering options. However, what is done dynamically is the available options to filter on. In order to do this, we run the gatherData() method in ModelList.js in order to loop through all of the searched/filtered data shown in the ModelData object and generate a map containing all of the possible options for each filter. This map is then passed to the Dropdown component and allows the filtering buttons to dynamically show the correct possible options.

The actual data is filtered on change to any of the filtered buttons. To do this, we loop through the current ModelData state and generate a filtered copy by only including the correct features(which are passed in as parameters-- String eventKey). At the end, we setState to the new filtered ModelState, which will trigger an update on the frontend. The callbacks are passed into the Dropdown and subsequently used in the parent component, after being triggered in the dropdown.

Finally, there is a reset button that sets each button selections and filters back to default, and also resets the modelData back to the default by requerying our database. The search field is also cleared.

## 10. Sorting

### Process

Sorting is provided in a ribbon at the top of the model list pages. Clicking on any attribute will switch between sorting by ascending or descending order for that attribute. It immediately sorts the models on that page.

### Tools

Sorting again was done completely on the front end using react because it was the simplest and cleanest solution for our website.

### How it Works in the Code

The sorting in the code is actually very simple. Because our data is already in the form of an array, we use the built-in JavaScript `sort()` function which allows us to define a function to sort elements. Thus, whenever one of the attributes it clicked, our own `sortBy()` function is called which takes our model data and uses sort to sort it either alphabetically or numerically, depending on the type of the data.


## 11. Testing

### Testing Structure
**GUI Tests (Selenium)** budgetfor.space/test/guitests.py  
**Server Tests (Unittest)** budgetfor.space/server/test/apitests.py  
**JS Tests (Mocha)** budgetfor.space/client/\__tests__/\*.spec.js  
**API Tests (Postman)** budgetfor.space/postman.json  

### GUI Tests (Selenium)
Our GUI tests are written using the python selenium package and make use of the Chrome web browser (via the chromedriver file included in the testing directory). The main target of the tests is user experience. There are four main types of tests.

1. Navigation Bar (*6 tests*): Tests that there are the correct number of elements on the navigation bar is present on every page of the site.
2. Navigation Button (*12 tests*): Tests that each button on the navigation bar (tested in two different ways) and the "Learn more about our mission" button on the homage page can be used to navigate to each page.
3. Content (*6 tests*): Ensures that each of the page has images on it.
4. Title (*1 test*): Tests that the home page has a descriptive title at the top for easy tab switching with other pages open.

### Server Tests (Unittest)
The server testing makes use of the built-in Python unittest framework, including making heavy use of its object mocking capabilities. There are two main categories of tests here.

1. Argument Set Up (*6 tests*): Ensures that each server endpoint is set up with the proper arguments according to our Postman documentation
2. SQL Calls (*7 tests*): Tests that each endpoint, when given valid arguments, constructs the expected SQL statement and executes
3. Attribute Calls (*22 tests*): Tests the content and ability to search each
model on each specific attribute, and ensures that the proper query is executed.

### JS Tests (Jest)
The Javascript testing was implemented using Jest, a testing framework designed for React. We decided to use Jest over Mocha because Mocha is not as compatible and required many dependencies to work. The tests are located in the client/__tests__ folder. Each file is associated with a certain .js file that tests various aspects of the HTML code, seeing if it all renders properly. In order to simulate the DOM instance of the website, we use Enzyme. Enzyme provides methods like shallow() that will render a given React component that you can then call methods such as find() on. This will allow you to test your parsed code by picking out subcomponents and seeing if they correctly render. In this iteration, we refined further by using the simulate() functionality to simulate clicking to  see if the correct components were rendered. We also mocked API calls using a mock-axios node module that helped in checking the validity of the information.

### API Tests (Postman)
The API testing was done using Postman, an API development environment. We decided to use Postman because Postman makes it really easy to make requests and it gives you the option to write tests that it'll run whenever those requests are being made. The tests are located in the budgetfor.space folder. There are six main
groups of tests, one for each endpoint.

1. organization (*4 tests*): Tests a GET request on the organization endpoint.
2. organizations (*6 tests*): Tests a GET request on organizations endpoint.
3. mission (*7 tests*): Tests a GET request on the mission endpoint.
4. missions (*12 tests*): Tests a GET request on the missions endpoint. From the parameters specified, there are two results, so it checks to see if the two expected results are correct.
5. destination (*7 tests*): Tests a GET request on the destination endpoint.
6. destinations (*14 tests*): Tests a GET request on the destinations endpoint. From the parameters specified, there are six results, so it checks to see if the six expected results are correct.

## 12. Database
We set up the database with RDS on Amazon Web Services. We decided on PostgreSQL because it is supposed to be easier to set up. Overall, this was probably true because it went smoothly.

After the database was set up on AWS, we used the tool pgAdmin4 to easily interact with the database. This was used to create the different tables to hold data and set up their schema. We also frequently used it to add (and later remove) test rows from the database, to conduct test queries before adding them to the server code, and for general maintenance of the database.

After having written the queries in pgAdmin4 that selected the information we needed to return for each of our server endpoints, it was simple enough to execute these with SQLAlchemy. The main challenge was parsing the arguments passed into the API call and adding the necessary `where` clauses to the SQL query. To simplify the approach, every query starts with a sort of *no-op* (namely `where id > 0`) which is guaranteed to return every result so that we don't have to worry about when to append the `where` and when to append the `and`.

Another challenge was pagination. This was initially implemented pretty naively, by just appending another `where` clause which checked if the `id` was within the range that of those that should be on the page. This quickly revealed itself to be a bug because it only searched the other attributes for instances that were on that page. Because of this, we had to introduce a temporary column corresponding to the row numbers after the attribute search was complete, filter pagination based on that column, and then remove it from the returned result.



### UML Diagram
![UML Diagram](https://gitlab.com/mmolash/budgetfor.space/raw/dev/uml.png)

## 13. Visualizations

The visualizations can be found on the visualizations tab of our website, which you can get to with the navigation bar.

#### Visualization 1 - Heat Map Visualization:
This visualization was implemented with a 3rd party package Datamaps, which is a wrapper on top of D3. We used their default world map svg and converted our data into statstics that could be consumed by their package. In particular, we had to get the counts of each country for all organizations, convert the country name into useable country codes(ex. United State == USA), then input that data dynamically into the Datamap object. This was all wrapped in a react component in order to be displayed on the front end.
#### Visualization 2 - Line Chart
This visualization shows the number of missions that have occurred each year since around the 1960s. The x-axis shows a linear progression of each year, and the y-axis has the number of missions that occurred in that particular year. We chose this visualization in order to show the trends in "popularity" of space travel. It paints a pretty clear picture of how space travel began as a very uncommon occurrence (only a few organizations were capable of sending objects to space in the early years), was very stagnant for a long time, and has recently exploded in terms of missions as more countries and private companies become interested in going to space.

#### Visualization 3 - Horizontal Bar Chart
This visualization shows the number of missions that each organization has accomplished. It has the organizations along the y-axis and the number of missions they have completed along the upper x-axis. We chose this visualization because it's interesting to see which organizations do the most space exploration. Those countries that send more missions into space in turn have a very interesting impact on what is explored. This was additionally wrapped in a React component so that it could be rendered on the page.

## 14. Critiques

### Self-Critiques

#### What did we do well?

**IDB4**  
We were tasked with adding finishing touches in this last phase of the project. We split up the tasks and worked together to complete this final phase. As always, we communicated often via Slack. Looking back to where we started, we've definitely grown as a team over the course of the 4 phases of this project. Our team dynamic improved, and we grew closer together as friends. Through our ups and downs, we've shared countless memories on this project. We can say with the upmost confidence that we're all happy that we ended up on a team together for IDB!

**IDB3**  
Phase 3 consisted of refining a lot of the work we had done in phase 2 of the project. We met as a group and divided up the work. We lost some points on the project rubric, so we decided to make sure we wouldn't make the same mistake twice. We achieved this by making a greater effort towards reviewing the project rubric and creating stories for each requirement of the project ahead of time. We learned a lot of lessons in regards to time management from IDB2 and decided to create a checklist early on. As a result, nobody was scrambling last-minute, and everyone was a lot less stressed.

**IDB2**  
In this phase of the project, we had to do many things such as making our website dynamic, storing our data in a database, adding unit tests, etc. We listened to the advice the TAs gave us in IDB1. As a result, it made this phase of the project a little easier as we had already done some of the requirements of this phase. Because there were more requirements of this phase, we weren't entirely sure how we should split up the material. However, everyone was willing to contribute and whenever someone finished their tasks, they would simply claim another one. We heavily relied on Slack to communicate with each other on our progress.

**IDB1**  
Before we began writing any code, we met up as a group, created stories for the tasks we had to resolve, and divided the responsibilities up. We split into different groups to work on different aspects of the project (i.e. frontend and backend).
Coming into this project, everyone varied in skill and experience, which made it a daunting task to divide the work up. However, something we thought we did really well was guiding one another. Everyone was willing to lend a hand with another's work if they ever struggled. Communication was extremely important to ensure nobody was blocking somebody else as well as being able to provide help when needed. We utilized Slack, talking on a daily basis. It ended up being an essential tool to our project.

#### What did we learn?

**IDB4**  
In IDB4, we saw the perks of creating GitLab issues and dividing up the work early on. We learned the importance of communication and working effectively as a team on this phase too. Being able to meet up as a group and to collectively discuss what kinds of visualizations we wanted to create for our website made it a lot easier to actually carry out the work. As we looked through the code of different parts of the project that we had to refactor, we all gained a deeper understanding of how each component was implemented and how it all came together.


**IDB3**  
During this phase of the project, we gained a deeper understanding of the tools that we had already used in the prior 2 phases. In order to implement searching, sorting, filtering, and pagination, we had to learn about the relationship between the frontend and the backend and how to connect the two (i.e. searching for something in the database and showing the results on the website). In addition to implementing these features, we focused on improving the design of our website so that the user experience would be better.

**IDB2**  
Like phase 1, we learned how to utilize new tools for phase 2 as well. Scraping the data we'd use for our database, storing the data in the database, and creating the endpoints for our API was all relatively new for us. We learned how to use PostgreSQL, SQLAlchemy, Axios, among many other tools.

**IDB1**  
The first phase of the project had a steep learning curve. We were asked to use React, Slack, GitLab, and many other tools. Most of our group members were inexperienced with these tools. However, we learned how to utilize these tools as we worked on the project. Transitioning from not knowing what the tools were to being able to use them to create a website was an amazing feeling and it was extremely rewarding.

Learning how to use the GitLab board was also very gratifying. Creating our own stories, assigning tasks to one another, and using the other features that GitLab provides gave us insight into how software engineers worked on a daily basis.

None of us had worked on a semester-long project before, so it was a learning experience for us to communicate with one another effectively.

#### What can we do better?

**IDB4**  
One of our biggest issues for this phase of the project was motivation. After phase 2 of the project, there was a decline in terms of the amount of work we had to complete. Because of this, most of us ended up procrastinating on the project until the week that it was due. Our lesson from this is that for future projects, we will hold each other more accountable and try to push one another to finishing stories in a timely manner.

We could definitely improve on having a code standard. Most of our code was poorly documented. Because there wasn't a consistent standard, it made it confusing for us to look back at one anothers code as we had to spend some time figuring out what was going on. Our lesson from this is to create guidelines for everyone in the group to follow before actually writing code so that it'll be less of a headache later on.


**IDB3**  
We had great communication throughout this project, but as the semester progressed, we began meeting less and less in person. Although our communication was strong, there were still areas for improvement. Meeting in person more often to discuss the project would definiitely be beneficial.

Working at different times also became a slight issue. Our group would be blocked by one another and would sometimes have to wait for the other member to finish before they could proceed with what they were doing. However, we overcame this by planning ahead early, and rather than waiting around, we would simply just work on another issue until we could come back to what we were originally working on.

We made a better effort towards logging our issues and the time we spent towards each story, but it still wasn't perfect. Some of us would sometimes forget to do this.

**IDB2**  
The biggest issue we had in this phase of the project is definitely time management. Spring Break was during the middle of this phase, and all of us chose to put off this project until after the break as opposed to working on it incrementally. Procrastinating ended up backfiring as we realized how much work we actually had to complete for this phase of the project. Phase 2 was definitely the hardest phase because of our procrastination and the all the requirements we had to have completed for it. The biggest lesson we learned from this phase is that we should start early and to plan ahead. We don't want to be put in a similar time crunch for future phases of the project.

**IDB1**  

We wish we had figured out how to better utilize our project board. We didn't do a good job of estimating and keeping track of the time spent on the various stories each of us completed.

To add to that, we wish we had closed issues as soon as we had completed them. As we were finishing up this phase of the project, we went to check to see if we had resolved all the issues we had created. Because we didn't close issues from commits and pull requests, we would have to manually check whether or not we had finished the issue by looking through our code.

Something we didn't do that well in was commenting. Due to the lack of commenting, if any of us ever needed to use the code that somebody else did, we would spend quite a bit of time understanding their code and what was going on before we could use it. If the code had been commented, this would've saved some time.

#### What puzzles us?

**IDB4**  
Now that we were in the final phase of the project, being able to properly present the work we had done was an interesting challenge. We had to come up with the best way to present our data. We're content with the results of our work, but we know that there is still room to improve. Coming up with a better design is something that comes with experience.

**IDB3**  
For IDB3, something that gave us trouble was improving the user experience of our website. After we had implemented features such as filtering, sorting, and searching, we realized that we wanted to make the features easy to use and the instances presentable. Finding the ideal way we wanted to achieve this was something we figured out through trial and error.

**IDB2**  
Something that troubled us during this phase of the project was how to split up the work evenly amongst members. We had divvied up the tasks to frontend and backend roles, but some people definitely pulled their weight more than others. Everyone on the team was very appreciative whenever somebody stepped up and put the team on their back, but in order to maintain a healthy team dynamic, everyone needs to consistently contribute towards the project.

**IDB1**  
Something that puzzled us was figuring out how to use GitLab properly. There were a lot of features that GitLab provided, and all of us had never used this tool before. We figured that we'd gain a better understanding of it as the semester progressed.


### Critiques of https://catchmeoutside.today/

#### What did they do well?

**IDB4**  
Watching CatchMeOutSide's website come together throughout the course of the semester was very entertaining. With each phase, we were surprised with something new. They were able to integrate a lot of components seamlessly such as the maps component, reviews, and the multimedia. We were impressed with how user friendly the website was. Their website is something that we'll definitely be using whenever we're trying to go out and enjoy the outdoors!

**IDB3**  
The CatchMeOutside team did a great job on satisfying the requirements of this project. We really liked the design of their tags that were put on each instance. It was very aesthetic and easily recognizable. The searching, sorting, and filtering features on their website was very user friendly and easy to use. Overall, we were extremely impressed by their well-designed implementation.

**IDB2**  

For IDB2, they integregated maps for all their model pages. It was really awesome to be able to see where an event was taking place! They resolved the user stories that we gave them. We were already impressed with what they had thus far, but they told us that they had even more in store. They were a really creative team and being able to see their ideas come to fruition was truly a sight to behold.

**IDB1**  
The CatchMeOutside team made a very aesthetic website. Early on, they came up with a design pattern that was very simple and clean. Because they did this, they didn't have to spend time redesigning their website later on. Instead, they were able to refine what they had already completed.

#### What did we learn from their API / website?

**IDB4**  
After reviewing their website one more time, we learned that there is always room for improvement. The features we already have can always be improved on. For example, their searching, sorting, and filtering are all located together, and are accessible when you press the dropdown arrow. It's extremely user friendly and is something one naturally interacts with. Although our features are togehter as well, it isn't as intuitive as their website. They've definitely changed the way we view design and have taught us to consider various aspects when trying to make a user friendly website.

**IDB3**  
As we were implementing features such as searching, we wanted to create a more user friendly website. We wanted to make a minimal website that was aesthetically pleasing, so we decided to use CatchMeOutside as a reference. By examining their project, we got a better sense for design, and we're really grateful.

**IDB2**  
Aftering watching them demonstrate their API, we decided to incorporate some of their features in our own API whenever we were refining it in the next phase of the project. For example, we realized that we could use some of the parameters that had in our own.

**IDB1**  
On our navigation bar, we had two home bars. One of them was on the right side of our navigation bar and was labeled "home". The other one was on the left side with the website's name. We hadn't considered the fact that we had two home bars; when we saw their website and noticed this, we opted to delete the home tab on the right side of the navigation bar as it was redundant.

#### What can they do better?

**IDB4**  
Overall, we were really impressed by the user experience and design of their website. They resolved all of our issues and expanded on their project. We don't really have any critiques on this last phase of the project. Instead, we have some suggestions that they might be able to implement in the future. For example, maybe they could integrate some social media for the different events that took place (i.e. if their was a tagged post for a specific event).

**IDB3**  
When we met with CatchMeOutside for this phase of the project, we noticed that they had really good ideas for the searching, sorting, and filtering features, but it wasn't up and running when they were presenting. They had designed the frontend aspect and setup the backend part of it, but hadn't connected the two at the time, so we were unable to see the searching, sorting, or filtering actually in action during their presentation.

**IDB2**  
For the second phase of the project, they could've used more content in each of their instances. For example, they weren't as rich in multimedia as they have and they could've used more attributes on each instance of their model pages.

**IDB1**  
Though their website was really well designed, we noticed that there were some bugs present on their website. Our user stories for the next phase were primarily geared towards resolving those issues.

#### What puzzles us about their API / website?

**IDB4**
They did a splendid job with their website, so nothing really puzzled me about their API / website during this final phase of the project. However, something that did nag on me a little was for their instances of the models for spaces. The weather seemingly never changed, I'm not sure if this is because it hasn't been implemented yet or if the weather on each of the icon is just for the design, but it'd be great to see the weather of each of the spaces update on a real-time basis.

**IDB3**
Something that puzzled us during this phase of the project was during the presentation. As mentioned before, they had a great idea of how they wanted to design the features of searching, sorting, and filtering and how to implement it. The only problem was that they didn't get around to connecting the frontend and backend before they presented to us. If they had chose to design one of the features all together (i.e. finish all of the sorting), then they would've been able to present it to us. This would've given us a better idea of how it worked.

**IDB2**  
We were very ill-informed for the different instances they had for each of their model pages. When going outdoors, multimedia is what first grabs people's attention. We wished they had more multimedia of the place or events they had on their website. We also were a little puzzled by how to find particular events. During this phase of the project, they didn't have as many attributes, so it was difficult to find a particular event or place.

**IDB1**  
We knew all of their events and places were located in or near Austin. However, most of us have never heard of the different instances they provided. We wanted to know the location of the different events or places, so we provided this as a user story for them.


