import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import { expect } from 'chai'
import Adapter from "enzyme-adapter-react-16"
import Home from "../src/views/Home"
import Particles from 'react-particles-js'
import {Button} from 'react-bootstrap'

configure({ adapter: new Adapter() });

describe('Home', () => {
    // Creator: Sahil
    it('displays button and text correctly', () => {
        const wrapper = shallow(<Home />);
        expect(wrapper.find(Button).text()).to.equal("Learn more about our mission");
    })
    // Creator: Sahil
    it('displays model cards correctly', () => {
        const wrapper = shallow(<Home />);
        expect(wrapper.find('LinkContainer').length).to.equal(4);
    });
});
