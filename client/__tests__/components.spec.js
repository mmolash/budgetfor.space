import React from 'react';
import { configure, shallow, mount } from 'enzyme';
import { expect } from 'chai'
import Adapter from "enzyme-adapter-react-16"
import PreviewCard from "../src/components/PreviewCard"
import CompCarousel from "../src/components/CompCarousel"
import DropdownComp from "../src/components/DropdownComp"
import { Dropdown } from 'react-bootstrap';

configure({ adapter: new Adapter() });

const missionData = {
  "missions": [
      {
          "id": 1,
          "name": "Nuclear Spectroscopic Telescope Array (NuSTAR)",
          "crew": false,
          "status": "past",
          "rocket": "Pegasus XL",
          "country": "United States",
          "organizations": [
              7
          ],
          "destinations": [
              75
          ],
          "date": 2012,
          "images": [
              "https://nustar.caltech.edu/assets/nustar_default_social_image-cc7ff08dd5dd420340f03ed6b62f38ac.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c2/Transiting_Exoplanet_Survey_Satellite_artist_concept_%28transparent_background%29.png",
              "https://upload.wikimedia.org/wikipedia/en/f/fd/Portal-puzzle.svg",
              "https://upload.wikimedia.org/wikipedia/commons/8/8b/NuSTAR_illustration_%28transparent_background%29.png",
              "https://upload.wikimedia.org/wikipedia/commons/f/fe/NuSTAR_detector.JPG",
              "https://upload.wikimedia.org/wikipedia/commons/d/d9/PIA18467-NuSTAR-Plot-BlackHole-BlursLight-20140812.png",
              "https://upload.wikimedia.org/wikipedia/commons/f/f7/Nustar_mast_deployed.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f0/Black_Holes_-_Monsters_in_Space.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/a/ad/NuSTAR%27s_Russian_Doll-like_Mirrors_%28PIA15631%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e5/NASA_logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/6/63/Pointing_X-ray_Eyes_at_our_Resident_Supermassive_Black_Hole.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/NuSTAR",
              "http://www.nasa.gov/mission_pages/nustar/overview/index.html"
          ],
          "description": "NuSTAR (Nuclear Spectroscopic Telescope Array) is a space-based X-ray telescope that uses a conical approximation to a Wolter telescope to focus high energy X-rays from astrophysical sources, especially for nuclear spectroscopy, and operates in the range of 3 to 79 keV.NuSTAR is the eleventh mission of NASA's Small Explorer satellite program (SMEX-11) and the first space-based direct-imaging X-ray telescope at energies beyond those of the Chandra X-ray Observatory and XMM-Newton. It was successfully launched on 13 June 2012, having previously been delayed from 21 March due to software issues with the launch vehicle.The mission's primary scientific goals are to conduct a deep survey for black holes a billion times more massive than the Sun, to investigate how particles are accelerated to very high energy in active galaxies, and to understand how the elements are created in the explosions of massive stars by imaging the remains, which are called supernova remnants.\nAfter a primary mission lasting two years (to 2014) it is now in its fifth year in space."
      },
      {
          "id": 2,
          "name": "WGS-4 (USA-233)",
          "crew": false,
          "status": "past",
          "rocket": "Delta IV M+(5,4)",
          "country": "United States",
          "organizations": [
              27,
              33
          ],
          "destinations": [
              78
          ],
          "date": 2012,
          "images": [
              "https://space.skyrocket.de/img_sat/wgs-4__1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b0/WGS-8_logo.png",
              "https://upload.wikimedia.org/wikipedia/commons/2/2b/WGS-7_Logo.png",
              "https://upload.wikimedia.org/wikipedia/commons/1/11/WGS-2_logo.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/9/95/WGS-1_logo.gif",
              "https://upload.wikimedia.org/wikipedia/commons/0/0c/WGS-4_logo.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3c/WGS-3_logo.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/72/MC-2941_Wideband_Global_SATCOM_Satellite.png",
              "https://upload.wikimedia.org/wikipedia/commons/4/49/WGS-9_logo.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/50/WGS-10_logo.png",
              "https://upload.wikimedia.org/wikipedia/commons/6/69/USAF_logo.png",
              "https://upload.wikimedia.org/wikipedia/commons/4/41/WGS-6_logo.png",
              "https://upload.wikimedia.org/wikipedia/commons/4/4e/WGS-5_logo.png"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Wideband_Global_SATCOM",
              "http://www.af.mil/AboutUs/FactSheets/Display/tabid/224/Article/104512/wideband-global-satcom-satellite.aspx"
          ],
          "description": "The Wideband Global SATCOM system (WGS) is a high capacity satellite communications system planned for use in partnership by the United States Department of Defense (DoD) and the Australian Department of Defence.  The system is composed of the Space Segment satellites, the Terminal Segment users and the Control Segment operators.DoD wideband satellite communication services are currently provided by a combination of the existing Defense Satellite Communications System (DSCS) and Global Broadcast Service (GBS) satellites.  According to United Launch Alliance, quoted on Spaceflight Now, \"A single WGS spacecraft has as much bandwidth as the entire existing DSCS constellation.\"\n\n"
      },
      {
          "id": 3,
          "name": "JUICE (JUpiter ICy moons Explorer)",
          "crew": false,
          "status": "future",
          "rocket": "Ariane 5 ECA",
          "country": "European Union",
          "organizations": [
              7
          ],
          "destinations": [
              77
          ],
          "date": 2022,
          "images": [
              "http://sci.esa.int/science-e-media/img/1f/59935_JUICE_exploring_Jupiter_565.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/24/Wikinews-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/6/66/JUICE_spacecraft.png",
              "https://upload.wikimedia.org/wikipedia/commons/7/75/Portrait_of_Jupiter_from_Cassini.jpg",
              "https://upload.wikimedia.org/wikipedia/en/2/2c/JUICE_insignia.svg",
              "https://upload.wikimedia.org/wikipedia/en/f/fd/Portal-puzzle.svg",
              "https://upload.wikimedia.org/wikipedia/commons/7/71/JUICE_spacecraft_concept.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f2/Ganymede_g1_true-edit1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4f/New_Horizons_Transparent.png",
              "https://upload.wikimedia.org/wikipedia/commons/c/cb/NEAR_Shoemaker_spacecraft_model.png",
              "https://upload.wikimedia.org/wikipedia/commons/e/e9/Callisto.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/45/Europa_g1_true.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/54/Europa-moon.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/8/89/Symbol_book_class2.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/People_icon.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Jupiter_Icy_Moons_Explorer",
              "http://sci.esa.int/juice/",
              "https://science.nasa.gov/missions/juice"
          ],
          "description": "The JUpiter ICy moons Explorer (JUICE) is an interplanetary spacecraft in development by the European Space Agency (ESA) with Airbus Defence and Space as the main contractor. The mission is being developed to visit the Jovian system and is focused on studying three of Jupiter's Galilean moons: Ganymede, Callisto, and Europa (excluding the more volcanically active Io) all of which are thought to have significant bodies of liquid water beneath their surfaces, making them potentially habitable environments. The spacecraft is set for launch in June 2022 and would reach Jupiter in October 2029 after five gravity assists and 88 months of travel. By 2033 the spacecraft should enter orbit around Ganymede for its close up science mission and becoming the first spacecraft to orbit a moon other than the moon of Earth. The selection of this mission for the L1 launch slot of ESA's Cosmic Vision science programme was announced on 2 May 2012. Its period of operations will overlap with NASA's Europa Clipper mission, also launching in 2022."
      },
      {
          "id": 4,
          "name": "SpX CRS-1",
          "crew": false,
          "status": "past",
          "rocket": "Falcon 9 v1.0",
          "country": "United States",
          "organizations": [
              7
          ],
          "destinations": [
              83
          ],
          "date": 2012,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/thumb/b/b8/SpaceX_CRS-1_approaches_ISS-cropped.jpg/260px-SpaceX_CRS-1_approaches_ISS-cropped.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/1/14/Progress-HTV-Dragon-ATV_Collage.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/71/SpaceX_CRS-1_launch_cropped.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b7/SpX_CRS-1_berthed_-_cropped.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3e/SpX-1_Dragon_at_port.1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/09/SpaceX_CRS-1_Launch.ogv",
              "https://upload.wikimedia.org/wikipedia/commons/3/36/SpaceX-Logo-Xonly.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d9/CRS_SpX-1_Dragon_int.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/28/Falcon_9_logo_by_SpaceX.png",
              "https://upload.wikimedia.org/wikipedia/commons/b/b8/SpaceX_CRS-1_approaches_ISS-cropped.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/21/SpaceX_CRS-1_Patch.png"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/SpaceX_CRS-1"
          ],
          "description": "SpaceX CRS-1, also known as SpX-1, was the third flight for Space Exploration Technologies Corporation's (SpaceX) uncrewed Dragon cargo spacecraft, the fourth overall flight for the company's two-stage Falcon 9 launch vehicle, and the first SpaceX operational mission under their Commercial Resupply Services contract with NASA. The launch occurred on 7 October 2012 at 20:34 EDT (8 October 2012 at 00:34 UTC)."
      },
      {
          "id": 5,
          "name": "TDRS-K",
          "crew": false,
          "status": "past",
          "rocket": "Atlas V 401",
          "country": "United States",
          "organizations": [
              7
          ],
          "destinations": [
              78
          ],
          "date": 2013,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a8/TDRS-K_satellite_before_launch.jpg/260px-TDRS-K_satellite_before_launch.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c1/Telstar.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/db/TDRS_gen1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/2f/US-Satellite.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/c/cb/TDRS_K_Project_fairing_logo.png",
              "https://upload.wikimedia.org/wikipedia/commons/a/a8/TDRS-K_satellite_before_launch.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/TDRS-11",
              "http://tdrs.gsfc.nasa.gov/tdrs/136.html"
          ],
          "description": "TDRS-11, known before launch as TDRS-K, is an American communications satellite which is operated by NASA as part of the Tracking and Data Relay Satellite System. The eleventh Tracking and Data Relay Satellite is the first third-generation spacecraft.TDRS-11 was constructed by Boeing, and is based on the BSS-601HP satellite bus. Fully fuelled, it has a mass of 3,454 kilograms (7,615 lb), and is expected to operate for 15 years. It carries two steerable antennas capable of providing S, Ku and Ka band communications for other spacecraft, plus an array of additional S-band transponders to allow communications at a lower data rate with greater numbers of spacecraft.TDRS-11 was launched at 01:48 UTC on 31 January 2013, at the beginning of a 40-minute launch window. United Launch Alliance performed the launch using an Atlas V carrier rocket, tail number AV-036, flying in the 401 configuration. Liftoff occurred from Space Launch Complex 41 at the Cape Canaveral Air Force Station, and the rocket placed its payload into a geostationary transfer orbit.\nFollowing its arrival in geosynchronous orbit, the satellite underwent on-orbit testing. It was handed over to NASA in August 2013, receiving its operational designation TDRS-11. After its arrival on-station at 171 degrees west the satellite began its final phase of testing prior to entry into service at the end of November."
      },
      {
          "id": 6,
          "name": "SARAL",
          "crew": false,
          "status": "past",
          "rocket": "PSLV-CA",
          "country": "India",
          "organizations": [
              1,
              8
          ],
          "destinations": [
              84
          ],
          "date": 2013,
          "images": [
              "https://altika-saral.cnes.fr/sites/default/files/styles/large/public/drupal/201506/image/bpc_saral-illustration_p43253.jpg?itok=ADrZulTz",
              "https://upload.wikimedia.org/wikipedia/en/4/41/Flag_of_India.svg",
              "https://upload.wikimedia.org/wikipedia/en/1/17/SARAL.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/50/Aryabhata_Satellite.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/b/bd/Indian_Space_Research_Organisation_Logo.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/SARAL",
              "http://www.isro.gov.in/Spacecraft/saral",
              "https://altika-saral.cnes.fr/en/SARAL/GP_satellite.htm"
          ],
          "description": "SARAL or Satellite with ARgos and ALtiKa is a cooperative altimetry technology mission of Indian Space Research Organisation (ISRO) and CNES (Space Agency of France). SARAL will perform altimetric measurements designed to study ocean circulation and sea surface elevation. The payloads of SARAL are The ISRO built satellite with payloads modules (ALTIKA altimeter), DORIS, Laser Retro-reflector Array (LRA) and ARGOS-3 (Advanced Research and Global Observation Satellite) data collection system provided by CNES was launched by Indian Polar Satellite Launch Vehicle rocket into the Sun-synchronous orbit (SSO). ISRO is responsible for the platform, launch, and operations of the spacecraft. A CNES/ISRO MOU (Memorandum of Understanding) on the SARAL mission was signed on Feb. 23, 2007.SARAL was successfully launched on 25 February 2013, 12:31 UTC."
      },
      {
          "id": 7,
          "name": "Eutelsat 115 West B & ABS-3A",
          "crew": false,
          "status": "past",
          "rocket": "Falcon 9 v1.1",
          "country": "United States",
          "organizations": [
              24
          ],
          "destinations": [
              78
          ],
          "date": 2015,
          "images": [
              "https://space.skyrocket.de/img_sat/eutelsat-115-west-b__1.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/28/Falcon_9_logo_by_SpaceX.png",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/f/fb/Launch_of_Falcon_9_carrying_ABS-EUTELSAT_%2816510241270%29.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/ABS-3A",
              "http://www.eutelsat.com/en/satellites/EUTELSAT-115WB.html"
          ],
          "description": "ABS-3A is a communications satellite that is operated by Asia Broadcast Satellite, providing coverage in the Americas, the Middle East, and Africa, as well as globally for TV distribution, cellular services, and maritime services. The satellite is the first commercial communications satellite in orbit to use electric propulsion, providing a significant weight savings."
      },
      {
          "id": 8,
          "name": "Resurs-P No.1",
          "crew": false,
          "status": "past",
          "rocket": "Soyuz 2.1b",
          "country": "Russian Federation",
          "organizations": [
              9
          ],
          "destinations": [
              84
          ],
          "date": 2013,
          "images": [
              "http://www.russianspaceweb.com/images/spacecraft/application/remote_sensing/resurs_p/P2/design_1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/archive/d/d6/20150227043030%21RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/archive/d/d6/20150212082719%21RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/archive/d/d6/20150209231237%21RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/archive/d/d6/20090604151519%21RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/archive/d/d6/20080905204005%21RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/archive/d/d6/20080102200227%21RocketSunIcon.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Resurs-P_No.1",
              "http://www.russianspaceweb.com/resurs_p.html"
          ],
          "description": "Resurs-P No.1 is a Russian commercial earth observation satellite capable of acquiring high-resolution imagery (resolution up to 1.0 m). The spacecraft is operated by Roscosmos as a replacement of the Resurs-DK No.1 satellite.\nThe satellite is designed for multi-spectral remote sensing of the Earth's surface aimed at acquiring high-quality visible images in near real-time as well as on-line data delivery via radio link and providing a wide range of consumers with value-added processed data.\nIn March 2014, Resurs-P No.1 was ordered to help find possible debris of Malaysia Flight 370."
      },
      {
          "id": 9,
          "name": "CBERS-3",
          "crew": false,
          "status": "past",
          "rocket": "Long March 4B",
          "country": "China",
          "organizations": [
              7
          ],
          "destinations": [
              84
          ],
          "date": 2013,
          "images": [
              "https://upload.wikimedia.org/wikipedia/pt/b/b9/Cbers1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/5c/CBERS_line_draw.jpeg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/CBERS-3"
          ],
          "description": "China\u2013Brazil Earth Resources Satellite 3 (CBERS-3), also known as Ziyuan I-03 or Ziyuan 1D, was a remote sensing satellite intended for operation as part of the China\u2013Brazil Earth Resources Satellite programme between the China Centre for Resources Satellite Data and Application and Brazil's National Institute for Space Research. The fourth CBERS satellite to fly, it was lost in a launch failure in December 2013."
      },
      {
          "id": 10,
          "name": "SpX CRS-2",
          "crew": false,
          "status": "past",
          "rocket": "Falcon 9 v1.0",
          "country": "United States",
          "organizations": [
              7,
              24
          ],
          "destinations": [
              83
          ],
          "date": 2013,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/thumb/2/2a/SpX_CRS-2_berthing.jpg/260px-SpX_CRS-2_berthing.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/1/14/Progress-HTV-Dragon-ATV_Collage.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3f/SpX_CRS-2_launch_-_cropped.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/2a/SpX_CRS-2_berthing.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/6/6c/SpaceX_CRS-2_Patch.png",
              "https://upload.wikimedia.org/wikipedia/commons/3/36/SpaceX-Logo-Xonly.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/28/Falcon_9_logo_by_SpaceX.png",
              "https://upload.wikimedia.org/wikipedia/commons/1/15/Samsung_Galaxy_S5_Vector.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a8/SpaceX_CRS-2_capsule_at_docks.1.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/SpaceX_CRS-2",
              "http://www.nasa.gov/multimedia/imagegallery/image_feature_2461.html"
          ],
          "description": "SpaceX CRS-2, also known as SpX-2, was the fourth flight for SpaceX's uncrewed Dragon cargo spacecraft, the fifth and final flight for the company's two-stage Falcon 9 v1.0 launch vehicle, and the second SpaceX operational mission contracted to NASA under a Commercial Resupply Services contract.\nThe launch occurred on March 1, 2013. A minor technical issue on the Dragon spacecraft involving the RCS thruster pods occurred upon reaching orbit, but it was recoverable. The vehicle was released from the station on March 26, 2013, at 10:56 UTC and splashed down in the Pacific Ocean at 16:34 UTC."
      },
      {
          "id": 11,
          "name": "SBIRS GEO Flight 2 (USA-241) (SBIRS GEO-2)",
          "crew": false,
          "status": "past",
          "rocket": "Atlas V 401",
          "country": "United States",
          "organizations": [
              27
          ],
          "destinations": [
              78
          ],
          "date": 2013,
          "images": [
              "https://space.skyrocket.de/img_sat/sbirs-geo-1__1.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/6/69/USAF_logo.png",
              "https://upload.wikimedia.org/wikipedia/commons/c/cf/SBIRS-Low.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/38/Lockheed_Martin_headquarters.jpg",
              "https://upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/SBIRS-GEO.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b7/SBIRS-Architecture.png"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Space-Based_Infrared_System",
              "http://www.lockheedmartin.com/us/products/sbirs.html"
          ],
          "description": "The Space-Based Infrared System (SBIRS) is a consolidated system intended to meet the United States' infrared space surveillance needs through the first two to three decades of the 21st century. The SBIRS program is designed to provide key capabilities in the areas of missile warning, missile defense and battlespace characterization via satellites in geosynchronous earth orbit (GEO), sensors hosted on satellites in highly elliptical orbit (HEO), and ground-based data processing and control. SBIRS ground software integrates infrared sensor programs of the U.S. Air Force (USAF) with new IR sensors. \nAs of  January 2018, a total of ten satellites carrying SBIRS or STSS payloads had been launched: GEO-1 (USA-230, 2011), GEO-2 (USA-241, 2013), GEO-3 (USA-273, 2017), GEO-4 (USA-282, 2018), HEO-1 (USA-184, 2006), HEO-2 (USA-200, 2008), HEO-3 (USA-259, 2014), STSS-ATRR (USA-205, 2009), STSS Demo 1 (USA-208, 2009) and STSS Demo 2 (USA-209, 2009). The manufacturing contract for GEO-5 and GEO-6 was awarded in 2014, with the two satellites scheduled for delivery to the Air Force in 2022."
      },
      {
          "id": 12,
          "name": "Apollo\u2013Soyuz Test Project",
          "crew": true,
          "status": "past",
          "rocket": "Saturn IB",
          "country": "United States",
          "organizations": [
              7,
              10
          ],
          "destinations": [
              79
          ],
          "date": 1975,
          "images": [
              "https://www.nasa.gov/sites/default/files/images/467434main_astp03_crew_full.jpg",
              "https://upload.wikimedia.org/wikipedia/en/a/ae/Flag_of_the_United_Kingdom.svg",
              "https://upload.wikimedia.org/wikipedia/en/f/fd/Portal-puzzle.svg",
              "https://upload.wikimedia.org/wikipedia/commons/f/fa/Flag_of_the_People%27s_Republic_of_China.svg",
              "https://upload.wikimedia.org/wikipedia/commons/5/5b/Greater_coat_of_arms_of_the_United_States.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d4/Flag_of_Israel.svg",
              "https://upload.wikimedia.org/wikipedia/commons/f/fc/Flag_of_Mexico.svg",
              "https://upload.wikimedia.org/wikipedia/commons/9/9f/Flag_of_Indonesia.svg",
              "https://upload.wikimedia.org/wikipedia/commons/c/ca/Flag_of_Iran.svg",
              "https://upload.wikimedia.org/wikipedia/commons/0/09/Flag_of_South_Korea.svg",
              "https://upload.wikimedia.org/wikipedia/commons/4/49/Flag_of_Ukraine.svg",
              "https://upload.wikimedia.org/wikipedia/commons/5/51/Flag_of_North_Korea.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/32/Flag_of_Pakistan.svg",
              "https://upload.wikimedia.org/wikipedia/en/6/62/PD-icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/9/97/The_Earth_seen_from_Apollo_17.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b7/Flag_of_Europe.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d9/Flag_of_Canada_%28Pantone%29.svg",
              "https://upload.wikimedia.org/wikipedia/en/c/c3/Flag_of_France.svg",
              "https://upload.wikimedia.org/wikipedia/commons/1/1a/Flag_of_Argentina.svg",
              "https://upload.wikimedia.org/wikipedia/commons/7/73/Blue_pencil.svg",
              "https://upload.wikimedia.org/wikipedia/commons/7/78/Bean_Descends_Intrepid_-_GPN-2000-001317.jpg",
              "https://upload.wikimedia.org/wikipedia/en/7/7c/MAVENnMars.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/00/Apollo_program.svg",
              "https://upload.wikimedia.org/wikipedia/commons/c/cf/Apollo_manned_development_missions_insignia.png",
              "https://upload.wikimedia.org/wikipedia/commons/2/2d/Apollo_lunar_landing_missions_insignia.png",
              "https://upload.wikimedia.org/wikipedia/commons/0/00/Apollo11-04.png",
              "https://upload.wikimedia.org/wikipedia/commons/8/88/Flag_of_Australia_%28converted%29.svg",
              "https://upload.wikimedia.org/wikipedia/commons/0/0b/Apollo11-07.png",
              "https://upload.wikimedia.org/wikipedia/commons/5/50/Apollo11-05.png",
              "https://upload.wikimedia.org/wikipedia/commons/d/d9/Apollo-Moon-mission-profile.png",
              "https://upload.wikimedia.org/wikipedia/commons/3/39/Apollo11-02.png",
              "https://upload.wikimedia.org/wikipedia/commons/5/5e/Apollo11-01.png",
              "https://upload.wikimedia.org/wikipedia/commons/d/d5/Apollo11-09.png",
              "https://upload.wikimedia.org/wikipedia/commons/7/70/Apollo11-08.png",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/Apollo_unmanned_launches.png",
              "https://upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg",
              "https://upload.wikimedia.org/wikipedia/commons/0/04/Apollo_11_Lunar_Lander_-_5927_NASA.jpg",
              "https://upload.wikimedia.org/wikipedia/en/b/ba/Flag_of_Germany.svg",
              "https://upload.wikimedia.org/wikipedia/commons/5/51/Apollo_Direct_Ascent_Concept.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/9/96/Apollo_landing_sites.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/dc/Apollo11-03.png",
              "https://upload.wikimedia.org/wikipedia/commons/9/9b/Apollo_1_fire.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/6/68/Apollo_7_launch2.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c0/Apollo_CSM_lunar_orbit.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/51/Apollo_Direct_Ascent_Concept.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/7d/Apollo_11_Saturn_V_lifting_off_on_July_16%2C_1969.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/1/1e/Apollo_11_first_step.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/2e/Apollo_15_Lunar_Rover_and_Irwin.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/5e/Apollo_1_Prime_Crew_-_GPN-2000-001159.jpg",
              "https://upload.wikimedia.org/wikipedia/en/0/05/Flag_of_Brazil.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/Apollo11-11.png",
              "https://upload.wikimedia.org/wikipedia/commons/8/83/Apollo11-10.png",
              "https://upload.wikimedia.org/wikipedia/commons/8/8c/Apollo11-13.png",
              "https://upload.wikimedia.org/wikipedia/commons/4/4e/Apollo11-12.png",
              "https://upload.wikimedia.org/wikipedia/commons/a/a7/Apollo11-15.png",
              "https://upload.wikimedia.org/wikipedia/commons/2/2e/Apollo11-14.png",
              "https://upload.wikimedia.org/wikipedia/commons/1/19/Apollo11-LRO-March2012.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a8/Apollo11-16.png",
              "https://upload.wikimedia.org/wikipedia/commons/3/3d/Apollo_11_Crew.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e8/Apollo17_plaque.jpg",
              "https://upload.wikimedia.org/wikipedia/en/9/94/Symbol_support_vote.svg",
              "https://upload.wikimedia.org/wikipedia/en/9/9e/Flag_of_Japan.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a8/NASA-Apollo8-Dec24-Earthrise.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/0d/Apollo_15_Genesis_Rock.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/6/63/LADEE_fires_small_engines.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e1/FullMoon2010.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d0/President_Kennedy_speech_on_the_space_effort_at_Rice_University%2C_September_12%2C_1962.ogv",
              "https://upload.wikimedia.org/wikipedia/commons/2/24/Wikinews-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4c/Wikisource-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/Winslow-Meteor_Crater-_Apollo_Test_Capsule.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/41/Flag_of_India.svg",
              "https://upload.wikimedia.org/wikipedia/en/f/f3/Flag_of_Russia.svg",
              "https://upload.wikimedia.org/wikipedia/en/0/03/Flag_of_Italy.svg",
              "https://upload.wikimedia.org/wikipedia/commons/b/be/LunarEclipse20070303CRH.JPG",
              "https://upload.wikimedia.org/wikipedia/commons/9/9c/Aldrin_Apollo_11.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e5/NASA_logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f5/Lunar_Ferroan_Anorthosite_%2860025%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4e/John_C._Houbolt_-_GPN-2000-001274.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/38/Irwin_i_Bull_testuj%C4%85_kombinezony_kosmiczne_S68-15931.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/7d/Apollo_11_Saturn_V_lifting_off_on_July_16%2C_1969.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/bb/Kennedy_Giving_Historic_Speech_to_Congress_-_GPN-2000-001658.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/ca/Saturnsandlittlejoe2.gif",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/People_icon.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/4c/Flag_of_Sweden.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3d/Apollo_11_Crew.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Apollo%E2%80%93Soyuz_Test_Project"
          ],
          "description": "The Apollo program, also known as Project Apollo, was the third United States human spaceflight program carried out by the National Aeronautics and Space Administration (NASA), which succeeded in landing the first humans on the Moon from 1969 to 1972. First conceived during Dwight D. Eisenhower's administration as a three-man spacecraft to follow the one-man Project Mercury which put the first Americans in space, Apollo was later dedicated to President John F. Kennedy's national goal of \"landing a man on the Moon and returning him safely to the Earth\" by the end of the 1960s, which he proposed in an address to Congress on May 25, 1961. It was the third US human spaceflight program to fly, preceded by the two-man Project Gemini conceived in 1961 to extend spaceflight capability in support of Apollo.\nKennedy's goal was accomplished on the Apollo 11 mission when astronauts Neil Armstrong and Buzz Aldrin landed their Apollo Lunar Module (LM) on July 20, 1969, and walked on the lunar surface, while Michael Collins remained in lunar orbit in the command and service module (CSM), and all three landed safely on Earth on July 24. Five subsequent Apollo missions also landed astronauts on the Moon, the last in December 1972. In these six spaceflights, twelve men walked on the Moon.\n\nApollo ran from 1961 to 1972, with the first manned flight in 1968. It achieved its goal of manned lunar landing, despite the major setback of a 1967 Apollo 1 cabin fire that killed the entire crew during a prelaunch test. After the first landing, sufficient flight hardware remained for nine follow-on landings with a plan for extended lunar geological and astrophysical exploration. Budget cuts forced the cancellation of three of these. Five of the remaining six missions achieved successful landings, but the Apollo 13 landing was prevented by an oxygen tank explosion in transit to the Moon, which destroyed the service module's capability to provide electrical power, crippling the CSM's propulsion and life support systems. The crew returned to Earth safely by using the lunar module as a \"lifeboat\" for these functions. Apollo used Saturn family rockets as launch vehicles, which were also used for an Apollo Applications Program, which consisted of Skylab, a space station that supported three manned missions in 1973\u201374, and the Apollo\u2013Soyuz Test Project, a joint US-Soviet Union Earth-orbit mission in 1975.\nApollo set several major human spaceflight milestones. It stands alone in sending manned missions beyond low Earth orbit. Apollo 8 was the first manned spacecraft to orbit another celestial body, while the final Apollo 17 mission marked the sixth Moon landing and the ninth manned mission beyond low Earth orbit. The program returned 842 pounds (382 kg) of lunar rocks and soil to Earth, greatly contributing to the understanding of the Moon's composition and geological history. The program laid the foundation for NASA's subsequent human spaceflight capability and funded construction of its Johnson Space Center and Kennedy Space Center. Apollo also spurred advances in many areas of technology incidental to rocketry and manned spaceflight, including avionics, telecommunications, and computers."
      },
      {
          "id": 13,
          "name": "Vostok 1",
          "crew": true,
          "status": "past",
          "rocket": "Vostok-K",
          "country": "European Union",
          "organizations": [
              10
          ],
          "destinations": [
              79
          ],
          "date": 1961,
          "images": [
              "https://airandspace.si.edu/sites/default/files/images/collection-objects/record-images/A19700319000d1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c7/Vostokpanel.JPG",
              "https://upload.wikimedia.org/wikipedia/en/9/99/Question_book-new.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/df/Vostok_spacecraft.jpg",
              "https://upload.wikimedia.org/wikipedia/en/b/b6/Vostok1_big.gif",
              "https://upload.wikimedia.org/wikipedia/en/b/b1/Vostok1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d1/Gagarin_field.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/6/6d/Gagarin_Capsule.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/8/87/Gnome-mime-sound-openclipart.svg",
              "https://upload.wikimedia.org/wikipedia/commons/c/cc/Gagarin_in_Sweden.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/59/Electrocardiogram_of_Gagarin.JPG",
              "https://upload.wikimedia.org/wikipedia/commons/c/cc/Gagarin-Poyekhali.ogg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a9/Flag_of_the_Soviet_Union.svg",
              "https://upload.wikimedia.org/wikipedia/commons/0/0e/Vostok_1_orbit_english.png",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Vostok_1"
          ],
          "description": "Vostok 1 (Russian: \u0412\u043e\u0441\u0442\u043e\u0301\u043a, East or Orient 1) was the first spaceflight of the Vostok programme and the first manned spaceflight in history. The Vostok 3KA space capsule was launched from Baikonur Cosmodrome on April 12, 1961, with Soviet cosmonaut Yuri Gagarin aboard, making him the first human to cross into outer space.\nThe orbital spaceflight consisted of a single orbit around Earth which skimmed the upper atmosphere at 169 kilometers (91 nautical miles) at its lowest point. The flight took 108 minutes from launch to landing. Gagarin parachuted to the ground separately from his capsule after ejecting at 7 km (23,000 ft) altitude."
      },
      {
          "id": 14,
          "name": "GSAT-14",
          "crew": false,
          "status": "past",
          "rocket": "GSLV Mk II",
          "country": "United States",
          "organizations": [
              7
          ],
          "destinations": [
              78
          ],
          "date": 2014,
          "images": [
              "https://space.skyrocket.de/img_sat/gsat-14__1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/41/Flag_of_India.svg",
              "https://upload.wikimedia.org/wikipedia/commons/5/50/Aryabhata_Satellite.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/bd/Indian_Space_Research_Organisation_Logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b7/Flag_of_Europe.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/GSAT-14",
              "http://www.isro.gov.in/Spacecraft/gsat-14-0"
          ],
          "description": "The GSAT satellites are India's indigenously developed communications satellites, used for digital audio, data and video broadcasting. As of 5 December 2018, 20 GSAT satellites of ISRO have been launched out of which 14 satellites are currently in service."
      },
      {
          "id": 15,
          "name": "Swarm A, B, C",
          "crew": false,
          "status": "past",
          "rocket": "Rokot / Briz-KM",
          "country": "France",
          "organizations": [
              21
          ],
          "destinations": [
              80
          ],
          "date": 2013,
          "images": [
              "https://space.skyrocket.de/img_sat/swarm__1.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/en/7/7b/Swarm_spacecraft.jpg",
              "https://upload.wikimedia.org/wikipedia/en/9/94/Swarm_logo.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/People_icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/24/Wikinews-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Swarm_(spacecraft)",
              "http://www.esa.int/Our_Activities/Observing_the_Earth/The_Living_Planet_Programme/Earth_Explorers/Swarm/ESA_s_magnetic_field_mission_Swarm"
          ],
          "description": "Swarm is a European Space Agency (ESA) mission to study the Earth's magnetic field. High-precision and high-resolution measurements of the strength, direction and variations of the Earth's magnetic field, complemented by precise navigation, accelerometer and electric field measurements, will provide data for modelling the geomagnetic field and its interaction with other physical aspects of the Earth system. The results offer a view of the inside of the Earth from space, enabling the composition and processes of the interior to be studied in detail and increase our knowledge of atmospheric processes and ocean circulation patterns that affect climate and weather.\n\n"
      },
      {
          "id": 16,
          "name": "Proba-V and VNREDSat 1A",
          "crew": false,
          "status": "past",
          "rocket": "Vega",
          "country": "European Union",
          "organizations": [
              20
          ],
          "destinations": [
              84
          ],
          "date": 2013,
          "images": [
              "https://forum.nasaspaceflight.com/index.php?action=dlattach;topic=30232.0;attach=515596;image",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/en/7/7b/Proba-V_satellite.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/24/Wikinews-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c3/Image_of_Belgium%2C_acquired_by_ESA%E2%80%99s_Proba-V_satellite.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/People_icon.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Proba-V",
              "http://www.esa.int/Our_Activities/Observing_the_Earth/Proba-V/About_Proba-V",
              "https://en.wikipedia.org/wiki/VNREDSat_1A",
              "https://en.wikipedia.org/wiki/ESTCube-1"
          ],
          "description": "PROBA-V is the fourth satellite in the European Space Agency's PROBA series; the V standing for vegetation.\n\n"
      },
      {
          "id": 17,
          "name": "CASSIOPE",
          "crew": false,
          "status": "past",
          "rocket": "Falcon 9 v1.1",
          "country": "Canada",
          "organizations": [
              24
          ],
          "destinations": [
              79
          ],
          "date": 2013,
          "images": [
              "https://directory.eoportal.org/documents/163813/3428695/Cassiope_Auto20.jpeg",
              "https://upload.wikimedia.org/wikipedia/commons/9/9d/SpaceX_Falcon_9_Cassiope_Launch_29_Sep_2013.webm",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/8/80/CASSIOPE_launch_001.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/f/fc/Maple_Leaf_%28from_roundel%29.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/28/Falcon_9_logo_by_SpaceX.png"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/CASSIOPE",
              "http://www.asc-csa.gc.ca/eng/satellites/cassiope.asp"
          ],
          "description": "CASSIOPE, or CAScade, Smallsat and IOnospheric Polar Explorer, is a Canadian Space Agency (CSA) multi-mission satellite operated by MacDonald, Dettwiler and Associates (MDA). The mission is funded through CSA and the Technology Partnerships Canada program. It was launched September 29, 2013, on the first flight of the SpaceX Falcon 9 v1.1 launch vehicle.  CASSIOPE is the first Canadian hybrid satellite to carry a dual mission in the fields of telecommunications and scientific research. The main objectives are to gather information to better understand the science of space weather, while verifying high-speed communications concepts through the use of advanced space technologies.\nThe satellite was deployed in an elliptical polar orbit and carries a commercial communications system called Cascade as well as a scientific experiment package called e-POP (enhanced Polar Outflow Probe).Following staging, the Falcon 9's first stage was used by SpaceX for a controlled descent and landing test. While the first stage was destroyed on impact with the ocean, significant data was acquired and the test was considered a success."
      },
      {
          "id": 18,
          "name": "Interface Region Imaging Spectrograph (IRIS)",
          "crew": false,
          "status": "past",
          "rocket": "Pegasus XL",
          "country": "United States",
          "organizations": [
              7
          ],
          "destinations": [
              84
          ],
          "date": 2013,
          "images": [
              "https://iris.lmsal.com/images/iris_instrument.png",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3c/Sun_-_August_1%2C_2010.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4d/The_Day_the_Earth_Smiled_-_PIA17172.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/06/Relative_sizes_of_all_of_the_habitable-zone_planets_discovered_to_date_alongside_Earth.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/6/63/LADEE_fires_small_engines.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/Skylab_Solar_flare.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a8/A_Slice_of_Light_How_IRIS_Observes_the_Sun.webm",
              "https://upload.wikimedia.org/wikipedia/commons/7/77/Kepler-62f_with_62e_as_Morning_Star.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/ca/IRIS_%28Explorer%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c2/Transiting_Exoplanet_Survey_Satellite_artist_concept_%28transparent_background%29.png"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Interface_Region_Imaging_Spectrograph",
              "https://www.nasa.gov/mission_pages/iris/overview/index.html"
          ],
          "description": "The Interface Region Imaging Spectrograph (IRIS), also called Explorer 94, is a NASA solar observation satellite. The mission was funded through the Small Explorer program to investigate the physical conditions of the solar limb, particularly the chromosphere of the Sun. The spacecraft consists of a satellite bus and spectrometer built by the Lockheed Martin Solar and Astrophysics Laboratory (LMSAL), and a telescope provided by the Smithsonian Astrophysical Observatory. IRIS is operated by LMSAL and NASA's Ames Research Center.\nThe satellite's instrument is a high-frame-rate ultraviolet imaging spectrometer, providing one image per second at 0.3 arcsecond angular resolution and sub-\u00e5ngstr\u00f6m spectral resolution.\nNASA announced on 19 June 2009 that IRIS was selected from six Small Explorer mission candidates for further study, along with the Gravity and Extreme Magnetism (GEMS) space observatory.The spacecraft arrived at Vandenberg Air Force Base, California, on 16 April 2013 and was successfully launched on 27 June 2013 by a Pegasus-XL rocket."
      },
      {
          "id": 19,
          "name": "Shenzhou-10",
          "crew": true,
          "status": "past",
          "rocket": "Long March 2F",
          "country": "China",
          "organizations": [
              13
          ],
          "destinations": [
              79
          ],
          "date": 2013,
          "images": [
              "https://thumbs-prod.si-cdn.com/HeiVsyOmqNbrXBbMYr_3mZzH8e0=/420x240/https://public-media.si-cdn.com/filer/Shenzhou10-launch--flash.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/29/Tiangong_1_drawing.png",
              "https://upload.wikimedia.org/wikipedia/commons/f/fa/Flag_of_the_People%27s_Republic_of_China.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3d/Shenzhou-10.png",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Shenzhou_10"
          ],
          "description": "Shenzhou 10 (Chinese: \u795e\u821f\u5341\u53f7; pinyin: Sh\u00e9nzh\u014du Sh\u00edh\u00e0o) was a manned spaceflight of China's Shenzhou program that was launched on 11 June 2013. It was China's fifth manned space mission. The mission had a crew of three astronauts: Nie Haisheng, who was mission commander and previously flew on Shenzhou 6; Zhang Xiaoguang, a former PLAAF squadron commander who conducted the rendezvous and docking; and Wang Yaping, the second Chinese female astronaut. The Shenzhou spacecraft docked with the Tiangong-1 trial space laboratory module on 13 June, and the astronauts performed physical, technological, and scientific experiments while on board. Shenzhou 10 was the final mission to Tiangong 1 in this portion of the Tiangong program. On 26 June 2013, after a series of successful docking tests, Shenzhou 10 returned to Earth."
      },
      {
          "id": 20,
          "name": "SES-8",
          "crew": false,
          "status": "past",
          "rocket": "Falcon 9 v1.1",
          "country": "Luxembourg",
          "organizations": [
              7
          ],
          "destinations": [
              78
          ],
          "date": 2013,
          "images": [
              "https://ses-footprint-assets.s3.amazonaws.com/uploads/satellite_images/SES-8.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/9/99/Falcon_9_carrying_SES-8_%2804%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/28/Falcon_9_logo_by_SpaceX.png",
              "https://upload.wikimedia.org/wikipedia/commons/4/4c/Falcon_9_carrying_SES-8_%2808%29.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/SES-8",
              "http://www.ses.com/4629034/ses-8"
          ],
          "description": "For the Australian TV station, go to SES/RTS.\n\nSES-8 is a geostationary communication satellite operated by SES. SES-8 was successfully launched on SpaceX Falcon 9 v1.1 on 3 December 2013, 22:41 UTC.It was the first flight of any SpaceX launch vehicle to a supersynchronous transfer orbit,\nan orbit with a somewhat larger apogee than the more usual Geostationary transfer orbit (GTO) typically utilized for communication satellites."
      },
      {
          "id": 21,
          "name": "Kounotori 4 (HTV-4)",
          "crew": false,
          "status": "past",
          "rocket": "H-IIB 304",
          "country": "Japan",
          "organizations": [
              4
          ],
          "destinations": [
              83
          ],
          "date": 2013,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/thumb/e/ed/ISS-36_HTV-4_berthing_2.jpg/1200px-ISS-36_HTV-4_berthing_2.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/0/04/International_Space_Station_after_undocking_of_STS-132.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/bb/ISS_insignia.svg",
              "https://upload.wikimedia.org/wikipedia/commons/1/1a/Progress_M-52.jpg",
              "https://upload.wikimedia.org/wikipedia/en/f/fd/Portal-puzzle.svg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b7/HTV-6_grappled_by_the_International_Space_Station%27s_robotic_arm_%282%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/1/1e/HTV-2_Kounotori_2_grappled_by_Canadarm2.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/24/ISS-26_HTV-2_Exposed_Pallet_grappled_by_Canadarm2.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4b/HTV_from_inside_02_-_cropped_and_rotated.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e9/H-IIB_F2_launching_HTV2.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/38/Iss020e0413802_-_cropped.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/9/99/HTV-1_close-up_view.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/bc/H-II_Transfer_Vehicle_diagram.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4e/ISS-44_Purple_Aurora_australis.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/ce/ISS-32_HTV-3_berthing_1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/8/89/Symbol_book_class2.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/H-II_Transfer_Vehicle",
              "http://iss.jaxa.jp/en/htv/mission/htv-4/"
          ],
          "description": "The H-II Transfer Vehicle (HTV), also called Kounotori (\u3053\u3046\u306e\u3068\u308a, K\u014dnotori, \"Oriental stork\" or \"white stork\"), is an automated cargo spacecraft used to resupply the Kib\u014d Japanese Experiment Module (JEM) and the International Space Station (ISS). The Japan Aerospace Exploration Agency (JAXA) has been working on the design since the early 1990s. The first mission, HTV-1, was originally intended to be launched in 2001. It launched at 17:01 UTC on 10 September 2009 on an H-IIB launch vehicle. The name Kounotori was chosen for the HTV by JAXA because \"a white stork carries an image of conveying an important thing (a baby, happiness, and other joyful things), therefore, it precisely expresses the HTV's mission to transport essential materials to the ISS\"."
      },
      {
          "id": 22,
          "name": "LADEE",
          "crew": false,
          "status": "past",
          "rocket": "Minotaur V",
          "country": "United States",
          "organizations": [
              7
          ],
          "destinations": [
              83
          ],
          "date": 2013,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/thumb/4/48/LADEE_w_flare_-_cropped.jpg/260px-LADEE_w_flare_-_cropped.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/41/Flag_of_India.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/1/11/Flag_of_Lithuania.svg",
              "https://upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg",
              "https://upload.wikimedia.org/wikipedia/commons/8/81/PSLV.svg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f3/Flag_of_Switzerland.svg",
              "https://upload.wikimedia.org/wikipedia/en/9/9a/Flag_of_Spain.svg"
          ],
          "externallinks": [
              "http://www.nasa.gov/mission_pages/ladee/mission-overview",
              "http://www.nasa.gov/mission_pages/ladee/mission-overview"
          ],
          "description": "The PSLV-C45 will be the 47th mission of the Indian Polar Satellite Launch Vehicle (PSLV) program. India will launch the Polar Satellite Launch Vehicle (PSLV)-C45 on 1 April 2019 which will put 29 satellites, including one for electronic intelligence,along with 28 customer satellites from other countries."
      },
      {
          "id": 23,
          "name": "AEHF-1 (USA-214)",
          "crew": false,
          "status": "past",
          "rocket": "Atlas V 531",
          "country": "United States",
          "organizations": [
              7
          ],
          "destinations": [
              78
          ],
          "date": 2012,
          "images": [
              "https://space.skyrocket.de/img_sat/aehf-1__1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/1/18/AEHF_1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/USA-214"
          ],
          "description": "USA-214, known before launch as Advanced Extremely High Frequency 1 or AEHF SV-1, is a military communications satellite operated by the United States Air Force. It is the first of four spacecraft to be launched as part of the Advanced Extremely High Frequency program, which will replace the earlier Milstar system.The USA-214 spacecraft was constructed by Lockheed Martin, and is based on the A2100 satellite bus. The spacecraft has a mass of 6,168 kilograms (13,598 lb) and a design life of 14 years. It will be used to provide super high frequency and extremely high frequency communications for the armed forces of the United States, as well as those of the United Kingdom, the Netherlands, and Canada."
      },
      {
          "id": 24,
          "name": "Gaia",
          "crew": false,
          "status": "past",
          "rocket": "Soyuz STB/Fregat-MT",
          "country": "European Union",
          "organizations": [
              7
          ],
          "destinations": [
              81
          ],
          "date": 2013,
          "images": [
              "https://images-na.ssl-images-amazon.com/images/I/911YWBva3EL._SY606_.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4d/The_Day_the_Earth_Smiled_-_PIA17172.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/6/63/LADEE_fires_small_engines.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/2a/Keplerspacecraft-FocalPlane-cutout.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/People_icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/7/78/M4-au-plan-focal.png",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/en/0/01/Gaia_spacecraft.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/f/fa/VST_Snaps_Gaia_en_Route_to_a_Billion_Stars.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/1/1a/Sch%C3%A9ma-gaia.png",
              "https://upload.wikimedia.org/wikipedia/commons/2/24/Wikinews-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/f/f7/Gaia_insignia.png",
              "https://upload.wikimedia.org/wikipedia/commons/9/9c/Gaia_Scan.svg",
              "https://upload.wikimedia.org/wikipedia/commons/1/11/Gaia_Milky_Way_star_density_map_2015-07-03.png",
              "https://upload.wikimedia.org/wikipedia/commons/5/5b/HST_SWEEPS_Detail_2006.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/07/Gaia_observatory_trajectory.svg",
              "https://upload.wikimedia.org/wikipedia/commons/9/96/Animation_of_Gaia_trajectory_-_Equatorial_view.gif",
              "https://upload.wikimedia.org/wikipedia/commons/c/c5/Comparison_optical_telescope_primary_mirrors.svg",
              "https://upload.wikimedia.org/wikipedia/commons/8/8f/Animation_of_Gaia_trajectory_-_Polar_view.gif",
              "https://upload.wikimedia.org/wikipedia/commons/b/be/LombergA1024.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/77/Kepler-62f_with_62e_as_Morning_Star.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/e/ed/How_many_stars_will_there_be_in_the_second_Gaia_data_release%3F_ESA392158.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/06/Relative_sizes_of_all_of_the_habitable-zone_planets_discovered_to_date_alongside_Earth.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Gaia_(spacecraft)",
              "http://sci.esa.int/gaia/"
          ],
          "description": "Gaia is a space observatory of the European Space Agency (ESA), launched in 2013 and expected to operate until c. 2022. The spacecraft is designed for astrometry: measuring the positions, distances and motions of stars with unprecedented precision. The mission aims to construct by far the largest and most precise 3D space catalog ever made, totalling approximately 1 billion astronomical objects, mainly stars, but also planets, comets, asteroids and quasars among others.The spacecraft will monitor each of its target objects about 70 times over a period of five years to study the precise position and motion of each target. The spacecraft has enough consumables to operate until about November 2024. As its detectors are not degrading as fast as initially expected, the mission could therefore be extended. The Gaia targets represent approximately 1% of the Milky Way population with all stars brighter than magnitude 20 in a broad photometric band that covers most of the visual range. Additionally, Gaia is expected to detect thousands to tens of thousands of Jupiter-sized exoplanets beyond the Solar System, 500,000 quasars outside our galaxy and tens of thousands of new asteroids and comets within the Solar System.Gaia will create a precise three-dimensional map of astronomical objects throughout the Milky Way and map their motions, which encode the origin and subsequent evolution of the Milky Way. The spectrophotometric measurements will provide the detailed physical properties of all stars observed, characterizing their luminosity, effective temperature, gravity and elemental composition. This massive stellar census will provide the basic observational data to analyze a wide range of important questions related to the origin, structure, and evolutionary history of our galaxy.\nThe successor to the Hipparcos mission (operational 1989\u201393), Gaia is part of ESA's Horizon 2000+ long-term scientific program. Gaia was launched on 19 December 2013 by Arianespace using a Soyuz ST-B/Fregat-MT rocket flying from Kourou in French Guiana. The spacecraft currently operates in a Lissajous orbit around the Sun\u2013Earth L2 Lagrangian point.\n\n"
      },
      {
          "id": 25,
          "name": "Chang'e 3 & Yutu",
          "crew": false,
          "status": "past",
          "rocket": "Long March 3B",
          "country": "Belarus",
          "organizations": [
              7
          ],
          "destinations": [
              83
          ],
          "date": 2013,
          "images": [
              "https://planetary.s3.amazonaws.com/assets/images/3-moon/2016/20160129_TCAM-I-143_SCI_P_20131223174541_0010_A_2C_stitch.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/0/06/Relative_sizes_of_all_of_the_habitable-zone_planets_discovered_to_date_alongside_Earth.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4d/The_Day_the_Earth_Smiled_-_PIA17172.jpg",
              "https://upload.wikimedia.org/wikipedia/en/e/e0/Yutu_rover_on_the_Moon.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/f/fa/Flag_of_the_People%27s_Republic_of_China.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a9/Chang%27e_3_landing_site.png",
              "https://upload.wikimedia.org/wikipedia/commons/6/63/LADEE_fires_small_engines.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/77/Kepler-62f_with_62e_as_Morning_Star.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c0/Animation2.gif",
              "https://upload.wikimedia.org/wikipedia/commons/0/06/Sinus_Iridum%2C_Chang%27e_3_%26_Lunokhod_1_landing_sites.png",
              "https://upload.wikimedia.org/wikipedia/commons/a/aa/Chang%27e-3_lunar_landing_site.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/78/Bean_Descends_Intrepid_-_GPN-2000-001317.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4d/Moon-Mdf-2005.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/be/LunarEclipse20070303CRH.JPG",
              "https://upload.wikimedia.org/wikipedia/en/e/e0/Yutu_rover_on_the_Moon.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Yutu_(rover)",
              "https://en.wikipedia.org/wiki/Chang%27e_3"
          ],
          "description": "Yutu (Chinese: \u7389\u5154; pinyin: Y\u00f9t\u00f9; literally: 'Jade Rabbit') was a robotic  lunar rover that formed part of the Chinese Chang'e 3 mission to the Moon. It was launched at 17:30 UTC on 1 December 2013, and reached the Moon's surface on 14 December 2013. The mission marks the first soft landing on the Moon since 1976 and the first rover to operate there since the Soviet Lunokhod 2 ceased operations on 11 May 1973.The rover encountered operational difficulties toward the end of the second lunar day after surviving and recovering successfully from the first 14-day lunar night. It was unable to move after the end of the second lunar night, though it continued to gather useful information for some months afterward. In October 2015, Yutu set the record for the longest operational period for a rover on the Moon. On 31 July  2016, Yutu ceased to operate after a total of 31 months, well beyond its original expected lifespan of three months.\nIn 2018 the follow-on to the Yutu rover, the Yutu-2 rover, launched as part of the Chang'e 4 mission."
      },
      {
          "id": 26,
          "name": "MAVEN",
          "crew": false,
          "status": "past",
          "rocket": "Atlas V 401",
          "country": "United States",
          "organizations": [
              7
          ],
          "destinations": [
              82
          ],
          "date": 2013,
          "images": [
              "https://www.nasa.gov/sites/default/files/maven_mars_arrival_comp-1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4d/The_Day_the_Earth_Smiled_-_PIA17172.jpg",
              "https://upload.wikimedia.org/wikipedia/en/f/fd/Portal-puzzle.svg",
              "https://upload.wikimedia.org/wikipedia/en/a/a4/Flag_of_the_United_States.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/2d/Solar_Energetic_Particles.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/74/Targeting_Mars.ogv",
              "https://upload.wikimedia.org/wikipedia/commons/7/74/Targeting_Mars.ogv",
              "https://upload.wikimedia.org/wikipedia/commons/9/99/Wiktionary-logo-en-v2.svg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4d/Phoenix_landing_%28PIA09943_cropped%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/42/Pia17952_electra_transceiver_dsc09326_0.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/06/Relative_sizes_of_all_of_the_habitable-zone_planets_discovered_to_date_alongside_Earth.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/29/MAVEN_%E2%80%94_Magnetometer.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/dc/MAVEN_spacecraft_model.png",
              "https://upload.wikimedia.org/wikipedia/commons/d/d4/Main_swea5_full.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/5c/MSL_Artist_Concept_%28PIA14164_crop%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/76/Mars_Hubble.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/cb/MarsMaven-Orbit-Insertion-142540-20140917.png",
              "https://upload.wikimedia.org/wikipedia/commons/a/ac/NASA-14090-Comet-C2013A1-SidingSpring-Hubble-20140311.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f1/MavenAerobrakingDiagram-20190211.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/00/PIA18613-MarsMAVEN-Atmosphere-3UV-Views-20141014.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e5/NASA_logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/4/43/Asteroid2006DP14.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/38/Artist_concept_of_MAVEN_spacecraft.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/6/63/LADEE_fires_small_engines.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/77/Kepler-62f_with_62e_as_Morning_Star.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c5/2001_mars_odyssey_wizja.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/06/Animation_of_MAVEN_trajectory_around_Mars.gif",
              "https://upload.wikimedia.org/wikipedia/commons/3/3c/67PNucleus.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/70/MAVEN_Mission_Logo.png",
              "https://upload.wikimedia.org/wikipedia/commons/d/df/MAVEN_Launch2-full.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/MAVEN",
              "http://www.nasa.gov/mission_pages/maven/main/index.html"
          ],
          "description": "Mars Atmosphere and Volatile EvolutioN (MAVEN) mission was developed by NASA to study the Martian atmosphere while orbiting Mars. Mission goals include determining how the planet's atmosphere and water, presumed to have once been substantial, were lost over time.MAVEN was launched aboard an Atlas V launch vehicle at the beginning of the first launch window on November 18, 2013.  Following the first engine burn of the Centaur second stage, the vehicle coasted in low Earth orbit for 27 minutes before a second Centaur burn of five minutes to insert it into a heliocentric Mars transit orbit.\nOn September 22, 2014, MAVEN reached Mars and was inserted into an areocentric elliptic orbit 6,200 km (3,900 mi) by 150 km (93 mi) above the planet's surface. The principal investigator for the spacecraft is Bruce Jakosky of the Laboratory for Atmospheric and Space Physics at the University of Colorado Boulder.On November 5, 2015, NASA announced that data from MAVEN shows that the deterioration of Mars' atmosphere increases significantly during solar storms. That loss of atmosphere to space likely played a key role in Mars' gradual shift from its carbon dioxide-dominated atmosphere \u2013 which had kept Mars relatively warm and allowed the planet to support liquid surface water \u2013 to the cold, arid planet seen today. This shift took place between about 4.2 and 3.7 billion years ago."
      },
      {
          "id": 27,
          "name": "Soyuz MS (MS-01)",
          "crew": true,
          "status": "past",
          "rocket": "Soyuz-FG",
          "country": "Russian Federation",
          "organizations": [
              9,
              7,
              4
          ],
          "destinations": [
              83
          ],
          "date": 2016,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/e/e0/Soyuz_MS-01_docked_to_the_ISS.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/1/1d/Soyuz_rocket_and_spaceship_V1-1.svg",
              "https://upload.wikimedia.org/wikipedia/commons/b/bc/Soyuz_TMA-7_spacecraft2edit1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e0/Soyuz_MS-01_docked_to_the_ISS.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3b/Soyuz-TMA_propulsion_module.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/e/ec/Expedition_49_Preflight_%28NHQ201609150017%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c7/Soyuz-TMA_orbital_module.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/23/Soyuz-TMA_descent_module.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Soyuz_MS-01",
              "http://www.nasa.gov/mission_pages/station/expeditions/expedition48/index.html"
          ],
          "description": "The Soyuz-MS (Russian: \u0421\u043e\u044e\u0437 \u041c\u0421, GRAU: 11F732A48) is the latest revision of the Soyuz spacecraft. It is an evolution of the Soyuz TMA-M spacecraft, with modernization mostly concentrated on the communications and navigation subsystems.\nIt is used by the Russian Federal Space Agency for human spaceflight. Soyuz-MS has minimal external changes with respect to the Soyuz TMA-M, mostly limited to antennas and sensors, as well as the thruster placement.The first launch was Soyuz MS-01 on July 7, 2016 aboard a Soyuz-FG launch vehicle towards the ISS. The trip included a two-day checkout phase for the design before docking with the ISS on July 9."
      },
      {
          "id": 28,
          "name": "SpX CRS-3",
          "crew": false,
          "status": "past",
          "rocket": "Falcon 9 v1.1",
          "country": "United States",
          "organizations": [
              7,
              24
          ],
          "destinations": [
              83
          ],
          "date": 2014,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/thumb/e/e3/Arrival_of_CRS-3_Dragon_at_ISS_%28ISS039-E-013475%29.jpg/1200px-Arrival_of_CRS-3_Dragon_at_ISS_%28ISS039-E-013475%29.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/1/14/Progress-HTV-Dragon-ATV_Collage.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a9/Launch_of_SpaceX_CRS-3.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/6/6b/SpaceX_CRS-3_Patch.png",
              "https://upload.wikimedia.org/wikipedia/commons/3/36/SpaceX-Logo-Xonly.svg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e3/Arrival_of_CRS-3_Dragon_at_ISS_%28ISS039-E-013475%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/28/Falcon_9_logo_by_SpaceX.png",
              "https://upload.wikimedia.org/wikipedia/commons/c/c5/CRS-3_Dragon_mating_with_Falcon_9_rocket_in_SLC-40_hangar_%2816649075267%29.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/SpaceX_CRS-3",
              "http://www.spacex.com/news/2014/05/18/crs-3-mission-overview"
          ],
          "description": "SpaceX CRS-3, also known as SpX-3, was a Commercial Resupply Service mission to the International Space Station, contracted to NASA, which was launched on 18 April 2014.  It was the fifth flight for SpaceX's uncrewed Dragon cargo spacecraft and the third SpaceX operational mission contracted to NASA under a Commercial Resupply Services contract.\nThis was the first launch of a Dragon capsule on the Falcon 9 v1.1 launch vehicle, as previous launches used the smaller v1.0 configuration. It was also the first time the F9 v1.1 has flown without a payload fairing, and the first experimental flight test of an ocean landing of the first stage on a NASA/Dragon mission.The Falcon 9 with CRS-3 on board launched on time at 19:25 UTC on 18 April 2014, and was grappled on 20 April at 11:14 UTC by Expedition 39 commander Koichi Wakata. The spacecraft was berthed to the ISS from 14:06 UTC on that day to 11:55 UTC on 18 May 2014. CRS-3 then successfully de-orbited and splashed down in the Pacific Ocean off the coast of California at 19:05 UTC on 18 May."
      },
      {
          "id": 29,
          "name": "OptSat 3000 & VEN\u00b5S (VENUS)",
          "crew": false,
          "status": "past",
          "rocket": "Vega",
          "country": "France",
          "organizations": [
              8
          ],
          "destinations": [
              84
          ],
          "date": 2017,
          "images": [
              "https://i.ytimg.com/vi/LWuDHLRx8t0/maxresdefault.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/en/9/94/Symbol_support_vote.svg",
              "https://upload.wikimedia.org/wikipedia/en/9/95/Selena_-_Baila_Esta_Cumbia.ogg",
              "https://upload.wikimedia.org/wikipedia/en/0/08/Venconmigo1.jpg",
              "https://upload.wikimedia.org/wikipedia/en/f/fd/Portal-puzzle.svg",
              "https://upload.wikimedia.org/wikipedia/commons/4/49/Star_empty.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a8/Office-book.svg",
              "https://upload.wikimedia.org/wikipedia/commons/8/89/Symbol_book_class2.svg",
              "https://upload.wikimedia.org/wikipedia/commons/5/51/Star_full.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/Conga.svg",
              "https://upload.wikimedia.org/wikipedia/commons/8/87/Gnome-mime-sound-openclipart.svg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f9/Double-dagger-14-plain.png"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/VEN%C2%B5S",
              "http://www.airforce-technology.com/projects/optsat-3000-earth-observation-satellite/",
              "https://venus.cnes.fr/en/VENUS/index.htm"
          ],
          "description": "Ven Conmigo (English: Come with Me) is the second studio album by American singer Selena, released on October 6, 1990, by EMI Latin. The singer's brother, A.B. Quintanilla remained her principal record producer and songwriter after her debut album's moderate success. Selena's Los Dinos band composed and arranged seven of the album's ten tracks; local songwriter Johnny Herrera also provided songs for Selena to record. Ven Conmigo contains half cumbias and half rancheras, though the album includes other genres. Its musical compositions are varied and demonstrate an evolving maturity in Selena's basic Tejano sound. The album's structure and track organization were unconventional compared with other Tejano music albums. The songs on Ven Conmigo are mostly love songs or songs following people's struggles after many failed relationships.\nAfter Ven Conmigo's release, the band hired guitarist Chris P\u00e9rez who introduced his hard rock sound to the band's music and performances, and further diversified Selena's repertoire. Her promotional tour for the album attracted upwards of 60,000 attendees to her shows, and critics praised the singer's stage presence. The album's single, \"Baila Esta Cumbia\" was the most played song on local Tejano music radio stations for over a month and helped Selena to tour in Mexico. Ven Conmigo peaked at number three on the US Billboard Regional Mexican Albums chart, her then-highest peaking album. It received critical acclaim, bringing Selena recognition as a Tejano singer and establishing her as a commercial threat.\nIn October 1991, Ven Conmigo went gold for sales exceeding 50,000 units, making Selena the first female Tejano singer to receive the honor. The event dissolved the male hierarchy in the Tejano music industry, which saw women as commercially inferior. Ven Conmigo received a nomination for the Tejano Music Award for Album of the Year \u2013 Orchestra at the 1992 annual event. The album peaked at number 22 on the US Billboard Top Pop Catalog Albums chart after it was ineligible to chart on the Billboard 200. In October 2017, the Recording Industry Association of America (RIAA) certified the album triple platinum, denoting  180,000 album-equivalent units sold in the United States."
      },
      {
          "id": 30,
          "name": "Ofeq-11",
          "crew": false,
          "status": "past",
          "rocket": "Shavit-2",
          "country": "Israel",
          "organizations": [
              14
          ],
          "destinations": [
              79
          ],
          "date": 2016,
          "images": [
              "https://space.skyrocket.de/img_sat/ofeq-10__1.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/41/Flag_of_India.svg",
              "https://upload.wikimedia.org/wikipedia/en/a/ae/Flag_of_the_United_Kingdom.svg",
              "https://upload.wikimedia.org/wikipedia/en/0/03/Flag_of_Italy.svg",
              "https://upload.wikimedia.org/wikipedia/en/c/c3/Flag_of_France.svg",
              "https://upload.wikimedia.org/wikipedia/en/9/9e/Flag_of_Japan.svg",
              "https://upload.wikimedia.org/wikipedia/commons/f/fa/Flag_of_the_People%27s_Republic_of_China.svg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b4/Flag_of_Turkey.svg",
              "https://upload.wikimedia.org/wikipedia/commons/9/99/Shavit_Ofek7a.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d9/Flag_of_Canada_%28Pantone%29.svg",
              "https://upload.wikimedia.org/wikipedia/commons/0/09/Flag_of_South_Korea.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d4/Flag_of_Israel.svg",
              "https://upload.wikimedia.org/wikipedia/en/b/ba/Flag_of_Germany.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Ofeq"
          ],
          "description": "Ofeq, also spelled Offek or Ofek (Hebrew: \u05d0\u05d5\u05e4\u05e7\u200e, lit. Horizon) is the designation of a series of Israeli reconnaissance satellites first launched in 1988. Most Ofeq satellites have been carried on top of Shavit rockets from Palmachim Airbase in Israel, on the Mediterranean coast. The Low Earth orbit satellites complete one earth orbit every 90 minutes. The satellite launches made Israel the eighth nation to gain an indigenous launch capability. Both the satellites and the launchers were designed and manufactured by Israel Aerospace Industries (IAI) with Elbit Systems' El-Op division supplying the optical payload."
      },
      {
          "id": 31,
          "name": "GSAT-11 & GEO-KOMPSAT-2A",
          "crew": false,
          "status": "past",
          "rocket": "Ariane 5 ECA",
          "country": "Korea, Republic of",
          "organizations": [
              6
          ],
          "destinations": [
              78
          ],
          "date": 2018,
          "images": [
              "http://www.arianespace.com/wp-content/uploads/2018/11/VA246-poster-686x1024.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/50/Aryabhata_Satellite.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/7/73/Blue_pencil.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/GSAT-11",
              "http://www.arianespace.com/mission/ariane-flight-va246/",
              "https://directory.eoportal.org/web/eoportal/satellite-missions/g/geo-kompsat-2",
              "https://www.isro.gov.in/Spacecraft/gsat-11-mission"
          ],
          "description": "GSAT-11 is an Indian geostationary communications satellite. The 5854 kg satellite is based on the new I-6K Bus and carry 40 transponders in the Ku-band and Ka-band frequencies (32 Ka \u00d7 Ku-Band Forward Link Transponders and 8 Ku \u00d7 Ka band Return Link Transponders), which are capable of providing up to 16 Gbit/s throughput. GSAT-11 is India's heaviest satellite."
      },
      {
          "id": 32,
          "name": "Soyuz 4",
          "crew": true,
          "status": "past",
          "rocket": "Soyuz",
          "country": "Russian Federation",
          "organizations": [
              10
          ],
          "destinations": [
              79
          ],
          "date": 1969,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/thumb/3/33/Soyuz45-1.jpg/260px-Soyuz45-1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/bc/Soyuz_TMA-7_spacecraft2edit1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/33/Soyuz45-1.jpg",
              "https://upload.wikimedia.org/wikipedia/en/9/99/Question_book-new.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a9/Flag_of_the_Soviet_Union.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e3/LK_ascent_from_Moon_drawing.png"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Soyuz_4"
          ],
          "description": "Soyuz 4 (Russian: \u0421\u043e\u044e\u0437 4, Ukrainian: \u0421\u043e\u044e\u0437 4, Union 4) was launched on 14 January 1969, carrying cosmonaut Vladimir Shatalov on his first flight. The aim of the mission was to dock with Soyuz 5, transfer two crew members from that spacecraft, and return to Earth. The previous three Soyuz flights were also docking attempts but all had failed for various reasons.\nThe radio call sign of the crew was Amur, while Soyuz 5 was Baikal. This referred to the trans-Siberian railway project called the Baikal-Amur Mainline, which was under construction at the time. The mission presumably served as encouragement to the workers on that project."
      },
      {
          "id": 33,
          "name": "Cygnus Orb-D1 (S.S. G. David Low)",
          "crew": false,
          "status": "past",
          "rocket": "Antares 110",
          "country": "United States",
          "organizations": [
              7,
              17
          ],
          "destinations": [
              79
          ],
          "date": 2013,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Cygnus_Orb-D1.1.jpg/220px-Cygnus_Orb-D1.1.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/1/14/Progress-HTV-Dragon-ATV_Collage.jpg",
              "https://upload.wikimedia.org/wikipedia/en/2/2d/Orb-D1_mission_emblem.png",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3a/Cygnus_iss.png",
              "https://upload.wikimedia.org/wikipedia/commons/f/ff/Antares_Orb-D1_launches_from_Wallops_%28201309180011HQ%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b4/Cygnus_PCM_Arrival_at_NASA%27s_Wallops_Flight_Facility_02.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/9/9f/Cygnus_Orb-D1.1.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Cygnus_Orb-D1"
          ],
          "description": "Cygnus Orb-D1, also known as Cygnus 1 and Orbital Sciences COTS Demo Flight, was the first flight of the Cygnus unmanned resupply spacecraft developed by Orbital Sciences Corporation. It was named after the late NASA astronaut and Orbital Sciences executive G. David Low. The flight was carried out by Orbital Sciences under contract to NASA as Cygnus' demonstration mission in the Commercial Orbital Transportation Services (COTS) program.  The mission launched on September 18th, 2013 at 10:58 AM. Cygnus was the seventh type of spacecraft to visit the ISS, after the manned Soyuz and Space Shuttle, and unmanned Progress, ATV, HTV and Dragon.\n\n"
      },
      {
          "id": 34,
          "name": "Cygnus CRS Orb-2",
          "crew": false,
          "status": "past",
          "rocket": "Antares 120",
          "country": "United States",
          "organizations": [
              7,
              17
          ],
          "destinations": [
              79
          ],
          "date": 2014,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Cygnus_CRS_Orb-2_at_ISS_before_grappling.jpg/1200px-Cygnus_CRS_Orb-2_at_ISS_before_grappling.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/en/1/14/Progress-HTV-Dragon-ATV_Collage.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f0/Orbital_Sciences_CRS_Flight_3_Patch.png",
              "https://upload.wikimedia.org/wikipedia/commons/f/f9/Antares_Orb-3_launch_failure_%28201410280009HQ%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/8/81/Antares_Orb-3_launch_failure_%28201410280011HQ%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/8/87/Antares_Fails_to_Reach_Orbit_with_Cygnus_CRS-3_after_Rocket_Explodes.webm",
              "https://upload.wikimedia.org/wikipedia/commons/5/53/Antares_Rocket_at_Sunrise%2C_October_26%2C_2014.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f2/Antares_Orb-3_launch_failure_%28201410280012HQ%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c8/Antares_CRS_Orb-3_raised_at_Pad-0A_%28201410250004HQ%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3a/Antares_CRS_Orb-3_vertical_at_Pad-0A_%28201410250005HQ%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e0/Antares_CRS_Orb-3_rollout_%28201410240003HQ%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f2/Fairing_installed_for_Orbital_CRS-3.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a6/Cygnus_spacecraft_loading_for_Orbital_CRS-3.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Cygnus_CRS_Orb-2"
          ],
          "description": "Cygnus CRS Orb-3, also known as Orbital Sciences CRS Flight 3 or Orbital 3, was an attempted flight of Cygnus, an automated cargo spacecraft developed by United States-based company Orbital Sciences, on 28 October 2014. The mission was intended to launch at 6:22 PM that evening. This flight, which would have been its fourth to the International Space Station and the fifth of an Antares launch vehicle, resulted in the Antares rocket exploding seconds after liftoff.\n\n"
      },
      {
          "id": 35,
          "name": "Sentinel-3A",
          "crew": false,
          "status": "past",
          "rocket": "Rokot / Briz-KM",
          "country": "France",
          "organizations": [
              21
          ],
          "destinations": [
              84
          ],
          "date": 2016,
          "images": [
              "https://space.skyrocket.de/img_sat/sentinel-3__1.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f7/Sentinel-3_spacecraft_model.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/People_icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/1/1f/United_Kingdom_ESA361630.tiff",
              "https://upload.wikimedia.org/wikipedia/commons/b/bc/Sentinel_1-IMG_5874-white.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d8/Bering_Sea_ESA376705.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/36/Kamchatka%2C_Russia_ESA374357.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/24/Cyclone_Debbie_ESA375411.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/24/Wikinews-logo.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Sentinel-3A",
              "https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-3"
          ],
          "description": "Sentinel-3  is an Earth observation satellite constellation developed by the European Space Agency as part of the Copernicus Programme.Copernicus, formerly Global Monitoring for Environment and Security, is the European programme to establish a European capacity for Earth observation designed to provide European policy makers and public authorities with accurate and timely information to better manage the environment, and to understand and mitigate the effects of climate change."
      },
      {
          "id": 36,
          "name": "Sentinel-5P",
          "crew": false,
          "status": "past",
          "rocket": "Rokot / Briz-KM",
          "country": "European Union",
          "organizations": [
              21
          ],
          "destinations": [
              84
          ],
          "date": 2017,
          "images": [
              "http://www.esa.int/var/esa/storage/images/esa_multimedia/images/2017/06/sentinel-5p/17040705-1-eng-GB/Sentinel-5P.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/24/Wikinews-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/3/37/People_icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/aa/Sentinel_5P_model.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/bc/Sentinel_1-IMG_5874-white.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Sentinel-5_Precursor",
              "https://sentinel.esa.int/web/sentinel/missions/sentinel-5p",
              "https://en.wikipedia.org/wiki/Copernicus_Programme"
          ],
          "description": "Sentinel-5 Precursor (Sentinel-5P) is an Earth observation satellite developed by ESA as part of the Copernicus Programme to close the gap in continuity of observations between Envisat and Sentinel-5."
      },
      {
          "id": 37,
          "name": "It's a Test",
          "crew": false,
          "status": "past",
          "rocket": "Electron ",
          "country": "United States",
          "organizations": [
              29
          ],
          "destinations": [
              84
          ],
          "date": 2017,
          "images": [
              "https://spacenews.com/wp-content/uploads/2018/06/DT7rwPoUQAASOcF-879x485.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/9/91/Electron_Orthographic.png",
              "https://upload.wikimedia.org/wikipedia/commons/2/2c/Rocket_Lab_Launch_Complex_1_%28Sept_2016%29.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/0c/Electron_rocket_logo.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Rocket_Lab#Electron_Launch_Vehicle"
          ],
          "description": "Electron is a two-stage orbital expendable launch vehicle (with an optional third stage) developed by the American aerospace company Rocket Lab to cover the commercial small satellite launch segment (CubeSats). Its Rutherford engines, manufactured in California, are the first electric-pump-fed engine to power an orbital rocket.In December 2016, Electron completed flight qualification. The first rocket was launched on 25 May 2017, reaching space but not achieving orbit due to a glitch in communication equipment on the ground. During its second flight on 21 January 2018, Electron reached orbit and deployed three CubeSats. The first commercial launch of Electron, and the third launch overall, occurred on 11 November 2018."
      },
      {
          "id": 38,
          "name": "ST-2 & INSAT-4G/GSAT-8",
          "crew": false,
          "status": "past",
          "rocket": "Ariane 5 ECA",
          "country": "India",
          "organizations": [
              35
          ],
          "destinations": [
              78
          ],
          "date": 2011,
          "images": [
              "https://space.skyrocket.de/img_sat/gsat-8__1.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/41/Flag_of_India.svg",
              "https://upload.wikimedia.org/wikipedia/commons/5/50/Aryabhata_Satellite.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/bd/Indian_Space_Research_Organisation_Logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b7/Flag_of_Europe.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/ST-2,https://en.wikipedia.org/wiki/GSAT-8"
          ],
          "description": "The GSAT satellites are India's indigenously developed communications satellites, used for digital audio, data and video broadcasting. As of 5 December 2018, 20 GSAT satellites of ISRO have been launched out of which 14 satellites are currently in service."
      },
      {
          "id": 39,
          "name": "Demo Flight",
          "crew": false,
          "status": "future",
          "rocket": "Vector-R",
          "country": "United States",
          "organizations": [
              41
          ],
          "destinations": [
              79
          ],
          "date": 2019,
          "images": [
              "https://i.ytimg.com/vi/Eqi3NUTEuB4/maxresdefault.jpg",
              "https://upload.wikimedia.org/wikipedia/en/d/db/Symbol_list_class.svg",
              "https://upload.wikimedia.org/wikipedia/commons/9/91/Wikiversity-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/f/fd/Portal-puzzle.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/5/50/Scalar_multiplication.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/29/Rectangular_hyperbola.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a6/Vector_add_scale.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d3/Universal_tensor_prod.svg",
              "https://upload.wikimedia.org/wikipedia/commons/8/87/Vector_components.svg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e6/Vector_addition3.svg",
              "https://upload.wikimedia.org/wikipedia/commons/0/02/Vector_norms2.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a6/Vector_components_and_base_change.svg",
              "https://upload.wikimedia.org/wikipedia/commons/f/fa/Wikibooks-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/df/Wikibooks-logo-en-noslogan.svg",
              "https://upload.wikimedia.org/wikipedia/en/9/94/Symbol_support_vote.svg",
              "https://upload.wikimedia.org/wikipedia/commons/6/66/Image_Tangent-plane.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a9/Heat_eqn.gif",
              "https://upload.wikimedia.org/wikipedia/commons/b/bb/Matrix.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/2f/Linear_subspaces_with_shading.svg",
              "https://upload.wikimedia.org/wikipedia/commons/8/8c/Affine_subspace.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d7/Example_for_addition_of_functions.svg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b9/Determinant_parallelepiped.svg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e8/Periodic_identity_function.gif",
              "https://upload.wikimedia.org/wikipedia/commons/6/66/Mobius_strip_illus.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Vector_Space_Systems",
              "https://www.prnewswire.com/news-releases/vector-to-conduct-dedicated-launch-of-alba-orbital-pocketqube-satellites-on-first-orbital-attempt-300610673.html"
          ],
          "description": "A vector space (also called a linear space) is a collection of objects called vectors, which may be added together and multiplied (\"scaled\") by numbers, called scalars. Scalars are often taken to be real numbers, but there are also vector spaces with scalar multiplication by complex numbers, rational numbers, or generally any field. The operations of vector addition and scalar multiplication must satisfy certain requirements, called axioms, listed below.Euclidean vectors are an example of a vector space.  They represent physical quantities such as forces: any two forces (of the same type) can be added to yield a third, and the multiplication of a force vector by a real multiplier is another force vector. In the same vein, but in a more geometric sense, vectors representing displacements in the plane or in three-dimensional space also form vector spaces. Vectors in vector spaces do not necessarily have to be arrow-like objects as they appear in the mentioned examples: vectors are regarded as abstract mathematical objects with particular properties, which in some cases can be visualized as arrows.\nVector spaces are the subject of linear algebra and are well characterized by their dimension, which, roughly speaking, specifies the number of independent directions in the space. Infinite-dimensional vector spaces arise naturally in mathematical analysis, as function spaces, whose vectors are functions. These vector spaces are generally endowed with additional structure, which may be a topology, allowing the consideration of issues of proximity and continuity. Among these topologies, those that are defined by a norm or inner product are more commonly used, as having a notion of distance between two vectors. This is particularly the case of Banach spaces and Hilbert spaces, which are fundamental in mathematical analysis.\nHistorically, the first ideas leading to vector spaces can be traced back as far as the 17th century's analytic geometry, matrices, systems of linear equations, and Euclidean vectors. The modern, more abstract treatment, first formulated by Giuseppe Peano in 1888, encompasses more general objects than Euclidean space, but much of the theory can be seen as an extension of classical geometric ideas like lines, planes and their higher-dimensional analogs.\nToday, vector spaces are applied throughout mathematics, science and engineering. They are the appropriate linear-algebraic notion to deal with systems of linear equations. They offer a framework for Fourier expansion, which is employed in image compression routines, and they provide an environment that can be used for solution techniques for partial differential equations. Furthermore, vector spaces furnish an abstract, coordinate-free way of dealing with geometrical and physical objects such as tensors. This in turn allows the examination of local properties of manifolds by linearization techniques. Vector spaces may be generalized in several ways, leading to more advanced notions in geometry and abstract algebra."
      },
      {
          "id": 40,
          "name": "Test Flight",
          "crew": false,
          "status": "future",
          "rocket": "LauncherOne",
          "country": "United States",
          "organizations": [
              40
          ],
          "destinations": [
              79
          ],
          "date": 2019,
          "images": [
              "https://bit.ly/2CIAjOM",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/en/4/48/Folder_Hexagonal_Icon.svg",
              "https://upload.wikimedia.org/wikipedia/en/f/fd/Portal-puzzle.svg",
              "https://upload.wikimedia.org/wikipedia/en/7/71/Virgin_Orbin_company_logo_2017.png",
              "https://upload.wikimedia.org/wikipedia/commons/f/fe/Virgin-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a1/Shuttle.svg",
              "https://upload.wikimedia.org/wikipedia/commons/7/7b/Crystal128-memory.svg",
              "https://upload.wikimedia.org/wikipedia/commons/1/18/Gas_giants_in_the_solar_system.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/8/85/Factory_USA.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Virgin_Orbit"
          ],
          "description": "Virgin Orbit is a company within the Virgin Group which plans to provide launch services for small satellites. The company was formed in 2017 to develop the air-launched LauncherOne rocket, launched from Cosmic Girl, which had previously been a project of Virgin Galactic. Based in Long Beach, California, Virgin Orbit has more than 300 employees led by president Dan Hart, a former vice president of government satellite systems at Boeing.Virgin Orbit focuses on small satellite launch, which is one of three capabilities being focused on by Virgin Galactic. These capabilities are: human spaceflight operations, small satellite launch, and advanced aerospace design, manufacturing, and test.\n\n"
      },
      {
          "id": 41,
          "name": "Maiden Flight",
          "crew": false,
          "status": "future",
          "rocket": "Firefly Alpha",
          "country": "United States",
          "organizations": [
              44
          ],
          "destinations": [
              79
          ],
          "date": 2019,
          "images": [
              "https://img.newatlas.com/firefly-alpha.jpeg?auto=format%2Ccompress&ch=Width%2CDPR&crop=entropy&fit=crop&h=347&q=60&w=616&s=16abac6bf462178a8be60c0b3d1ff8d4",
              "https://upload.wikimedia.org/wikipedia/commons/2/2c/Firefly_Alpha_Diagram.svg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Firefly_Aerospace/Firefly Alpha"
          ],
          "description": "Firefly Alpha (Firefly \u03b1) is two-stage orbital expendable launch vehicle developed by the American aerospace company Firefly Aerospace to cover the commercial small satellite launch market. Alpha is intended to provide launch options for both full vehicle and ride share customers."
      },
      {
          "id": 42,
          "name": "Test Flight",
          "crew": false,
          "status": "future",
          "rocket": "Terran 1",
          "country": "United States",
          "organizations": [
              46
          ],
          "destinations": [
              79
          ],
          "date": 2020,
          "images": [
              "https://bit.ly/2UlRbVq",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/2/26/Relativity_logo_black.png",
              "https://upload.wikimedia.org/wikipedia/commons/2/2a/Industry5.svg",
              "https://upload.wikimedia.org/wikipedia/commons/6/67/Relativity_Stargate_3D_Printer.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/57/LA_Skyline_Mountains2.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/Relativity_Space"
          ],
          "description": "Relativity Space is a private American aerospace manufacturer company headquartered in Los Angeles, California. It was founded in 2015 by Tim Ellis and Jordan Noone. Relativity is developing its own launchers and rocket engines for commercial orbital launch services."
      },
      {
          "id": 43,
          "name": "STS-31",
          "crew": false,
          "status": "past",
          "rocket": "Space Shuttle Discovery / OV-103",
          "country": "United States",
          "organizations": [
              7
          ],
          "destinations": [
              56,
              54,
              42,
              48,
              43,
              60,
              61,
              62,
              63,
              64,
              49,
              66,
              67,
              68,
              69,
              52,
              53,
              24,
              25,
              26,
              27,
              20,
              21,
              22,
              23,
              46,
              47,
              44,
              45,
              28,
              29,
              40,
              41,
              1,
              3,
              2,
              5,
              4,
              7,
              6,
              9,
              8,
              51,
              39,
              65,
              38,
              73,
              72,
              71,
              70,
              59,
              58,
              11,
              10,
              13,
              12,
              15,
              14,
              17,
              16,
              19,
              18,
              31,
              30,
              37,
              36,
              35,
              34,
              33,
              55,
              74,
              32,
              57,
              50
          ],
          "date": 1990,
          "images": [
              "https://upload.wikimedia.org/wikipedia/commons/0/01/1990_s31_IMAX_view_of_HST_release.jpg",
              "https://upload.wikimedia.org/wikipedia/en/4/4a/Commons-logo.svg",
              "https://upload.wikimedia.org/wikipedia/commons/c/c6/STS-121_seating_assignments.png",
              "https://upload.wikimedia.org/wikipedia/en/6/62/PD-icon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/a/a4/Text_document_with_red_question_mark.svg",
              "https://upload.wikimedia.org/wikipedia/en/9/99/Question_book-new.svg",
              "https://upload.wikimedia.org/wikipedia/commons/d/d6/RocketSunIcon.svg",
              "https://upload.wikimedia.org/wikipedia/commons/c/ca/Sts31_flight_insignia.png",
              "https://upload.wikimedia.org/wikipedia/commons/2/2f/Scanned_highres_STS031_STS031-76-39_copy.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b2/Eagle_nebula_pillars.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3b/Florida_from_STS-31.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/0/01/1990_s31_IMAX_view_of_HST_release.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/5/58/HST_over_Bahamas.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/3/3f/HST-SM4.jpeg",
              "https://upload.wikimedia.org/wikipedia/commons/e/e4/1990_s31_HST_Bay.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/bd/STS-31_Launch_-_GPN-2000-000684.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/f/f1/1990_s31_IMAX_view_of_HST_in_payload_bay.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/a/ac/1990_s31_HST_closeout.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/4/4d/Sts-31_crew.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b4/Liftoff_STS-31.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/2/20/Hubble_Solar_Array_Deployment_STS-31.jpg",
              "https://upload.wikimedia.org/wikipedia/commons/b/b2/Sts-31_Landing.jpg"
          ],
          "externallinks": [
              "https://en.wikipedia.org/wiki/STS-31"
          ],
          "description": "STS-31 was the 35th mission of the American Space Shuttle program, which launched the Hubble Space Telescope astronomical observatory into Earth orbit. The mission used the Space Shuttle Discovery (the tenth for this orbiter), which lifted off from Launch Complex 39B on 24 April 1990 from Kennedy Space Center, Florida.\nDiscovery's crew deployed the telescope on 25 April, and spent the rest of the mission tending to various scientific experiments in the shuttle's payload bay and operating a set of IMAX cameras to record the mission. Discovery's launch marked the first time since January 1986 that two Space Shuttles had been on the launch pad at the same time \u2013 Discovery on 39B and Columbia on 39A.\n\n"
      }
  ]
}


const attributes = {
  "organizations": ["Name", "Nationality", "Founding", "Public", "Employees", "Missions"],
  "destinations": ["Name", "Type", "Distance", "Composition", "Radius", "Discovery"],
  "missions": ["Name", "Manned", "Status", "Rocket", "Country", "Year"]
}

function gatherData(data, key) {
  //console.log(data);
  var mp = new Map();
  var filters = attributes[key];
  for (var x = 1; x < filters.length; x++) {
    mp.set(filters[x], new Set());
  }
  for (var i = 0; i < data.length; i++) {
    var curr = data[i];
    for (var j = 1; j < filters.length; j++) {
      var l = mp.get(filters[j]);
      if (curr[fields[filters[j]]] === undefined) {
        console.log(filters[j]);
        console.log(fields[filters[j]]);
        console.log(curr)
      }
      var ele = curr[fields[filters[j]]];
      if (fields[filters[j]] === 'missions') {
        l.add(ele.length);
      }
      else {
        l.add(ele);
      }
    }
  }
  //console.log(mp);
  return mp;
}

describe('PreviewCard', () => {
    const data = {
        "name": "National Aeronautics and Space Administration",
        "description": "The National Aeronautics and Space Administration is an independent agency of " +
                       "the executive branch of the United States federal government responsible for " +
                       "the civilian space program, as well as aeronautics and aerospace research." +
                       "NASA have many launch facilities but most are inactive. The most commonly" +
                       " used pad will be LC-39B at Kennedy Space Center in Florida.",
        "otherData": [["Type:", "Governmental"],
                      ["Key Figures:", "Dwight D. Eisenhower (Founder)\n" +
                                      "James Bridenstine (Administrator)\n"],
                      ["National Affiliations:", "USA"],
                      ["Founding Year", "1958"],
                      ["Number of employees: ", "17,336"]
        ],
        "funding": "$21.5 billion (FY 2019)",
        "missions" : [["Apollo 11", "/missions/apollo11"]],
        "destinations" : [["Moon", "/destinations/moon"]],
        "externalLinks": [
          "https://en.wikipedia.org/wiki/NASA",
        ],
        "images": ["https://images.pexels.com/photos/248797/pexels-photo-248797.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"]

    }
    // Creator: Alan
    it('receive props correctly', () => {
        const wrapper = shallow(<PreviewCard type="Organizations" data={data}/>);
        expect(wrapper.instance().props.type).to.equal("Organizations");
        wrapper.setProps({type: "Missions"});
        expect(wrapper.instance().props.type).to.equal("Missions");
    });
  });

  describe('CompCarousel', () => {
      const info = [1]

      // Creator: Alan
      it('receives props correctly', async () => {
          const wrapper = await shallow(<CompCarousel type="missions" info={info}/>);
          expect(wrapper.instance().props.info[0]).to.equal(1)
          info[0] = "newProp"
          wrapper.setProps({info: info});
          expect(wrapper.instance().props.info[0]).to.equal("newProp")
      });
      // Creator: Alan
       it('renders headers correctly', async () => {
        const wrapper = await shallow(<CompCarousel info={info} type="missions"/>);
        const inst = await wrapper.instance();
        info[0] = 1
        await inst.componentWillMount();
        expect(wrapper.find('h5').first().text()).to.equal("Nuclear Spectroscopic Telescope Array (NuSTAR)");
      });
  });

  describe('DropdownComp', () => {

    // Creator: Sahil
    it('receives props correctly', async () =>{
        const wrapper = await shallow(<DropdownComp type="Missions" visible={false} />)
        expect(wrapper.instance().props.visible).to.be.false;
    });

    // Creator: Sahil
    it('changes props correctly', async () =>{
      var mp = gatherData(missionData, "missions")
      const wrapper = await shallow(<DropdownComp type="Missions" visible={false} mp={mp}/>)
      wrapper.setProps({visible: true})
      expect(wrapper.instance().props.visible).to.be.true;
    });

    // Creator: Sahil
    it('finds mission filters', async () => {
      var mp = gatherData(missionData, "missions");
      const wrapper = await shallow(<DropdownComp type="Missions" visible={false} mp={mp}/>);
      wrapper.setProps({visible: true});
      expect(wrapper.find('DropdownButton').length).to.not.equal(0);
    });

    // Creator: Sahil
    it('gets filter options', async () => {
      var mp = gatherData(missionData, "missions");
      const wrapper = await shallow(<DropdownComp type="Missions" visible={false} mp={mp}/>);
      wrapper.setProps({visible: true});
      expect(wrapper.find('DropdownButton').first().prop('onClick')).to.not.equal(0);
    });

    // Creator: Sahil
    it('has search bar', async () => {
      var mp = gatherData(missionData, "missions");
      const wrapper = await mount(<DropdownComp type="Missions" visible={false} mp={mp}/>);
      wrapper.setProps({visible: true});
      expect(wrapper.find('Form').length).to.not.equal(0);
    });

    // Creator: Sahil
    it('properly hits search callback', async () => {
      var mp = gatherData(missionData, "missions");
      const wrapper = await mount(<DropdownComp type="Missions" visible={false} mp={mp}/>);
      wrapper.setProps({visible: true});
      expect(wrapper.find('Form').first().prop('onSubmit')).to.not.equal(0);
    });

  });

