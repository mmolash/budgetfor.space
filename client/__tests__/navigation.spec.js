import React from 'react';
import { configure, shallow } from 'enzyme';
import { expect } from 'chai'
import Adapter from "enzyme-adapter-react-16"
import Navigation from '../src/views/Navigation';
import {Navbar, Nav} from 'react-bootstrap'

configure({ adapter: new Adapter() });

describe('Navigation', () => {

  // Creator: Alan
  it('should verify navbar title', () => {
    const wrapper = shallow(<Navigation />);
    expect(wrapper.find(Navbar.Brand).first().text()).to.equal(" budgetfor.space");
  
  });
  // Creator: Alan
  it('should render correct number of tabs', () => {
    const wrapper = shallow(<Navigation />);
    expect(wrapper.find(Nav.Item).length).to.equal(6);
  })
});
