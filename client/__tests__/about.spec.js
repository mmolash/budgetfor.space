import React from 'react';
import { configure, shallow } from 'enzyme';
import { expect } from 'chai'
import Adapter from "enzyme-adapter-react-16"
import About from '../src/views/About';
import { Table } from 'react-bootstrap';


configure({ adapter: new Adapter() });

describe('About', () => {
    // Creator: Alan
  it('should count AboutMe cards', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('AboutCard').length).to.equal(5);
  });
    // Creator: Alan
  it('should render tables correctly', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find(Table).length).to.equal(2);
    expect(wrapper.find(Table).first().children().find('tr').length).to.equal(7);
  });
    // Creator: Sahil
  it('should have seven headings', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('h2').length).to.equal(7);
  });
    // Creator: Sahil
  it('first heading should be About Us', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('h2').first().text()).to.equal("About Us");
  });
    // Creator: Sahil
  it('second heading should be Data Integration', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('h2').at(1).text()).to.equal("Data Integration");
  });
    // Creator: Sahil
  it('third heading should be Meet the Team', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('h2').at(2).text()).to.equal("Meet the Team");
  });
    // Creator: Sahil
  it('fourth heading should be GitLab Statistics', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('h2').at(3).text()).to.equal("GitLab Statistics");
  });
    // Creator: Sahil
  it('fifth heading should be Data Sources', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('h2').at(4).text()).to.equal("Data Sources");
  });
    // Creator: Sahil
  it('sixth heading should be Tools', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('h2').at(5).text()).to.equal("Tools");
  });
    // Creator: Sahil
  it('seventh heading should be Links', () => {
    const wrapper = shallow(<About />);
    expect(wrapper.find('h2').at(6).text()).to.equal("Links");
  });
    
});