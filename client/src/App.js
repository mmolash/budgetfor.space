import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import DocumentTitle from 'react-document-title'
import Home from './views/Home.js'
import About from './views/About.js'
import ModelList from './views/ModelList.js'
import MissionModel from './views/MissionModel.js'
import OrganizationModel from './views/OrganizationModel.js'
import DestinationModel from './views/DestinationModel.js'
import NotFound from './views/NotFound.js'
import Navigation from './views/Navigation.js'
import ApiLanding from './views/ApiLanding.js'
import SearchResults from './views/SearchResults.js'
import Visualization from './views/Visualization.js'
import './App.css';
import 'typeface-roboto';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    
  }
});
const missionsCategories = ['All', 'Upcoming', 'Current', 'Previous'];
const organizationsCategories = ['All', '1', '2', '3']
const destinationsCategories = ['All', 'a', 'b', 'c']
class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
      <DocumentTitle title="Budget for Space">
        <div>
          <Router>
              <div>
                <Navigation/>
                <Switch>
                  <Route exact path="/" component={Home}/>
                  <Route exact path="/about" component={About}/>
                  <Route exact path="/api" component={ApiLanding}/>
                  <Route exact path="/missions" render={() => <ModelList type="Missions" filterCategories={missionsCategories}/>}/>
                  <Route exact path="/missions/:id" component={MissionModel}/>
                  <Route exact path="/organizations" render={() => <ModelList type="Organizations" filterCategories={organizationsCategories}/>}/>
                  <Route exact path="/organizations/:id" component={OrganizationModel}/>
                  <Route exact path="/destinations" render={() => <ModelList type="Destinations" filterCategories={destinationsCategories}/>}/>
                  <Route exact path="/destinations/:id" component={DestinationModel}/>
                  <Route exact path="/search" component={SearchResults}/>
                  <Route exact path="/visualizations" component={Visualization} />
                  <Route component={NotFound} />
                </Switch>
              </div>
          </Router>
        </div>
      </DocumentTitle>
      </MuiThemeProvider>
    );
  }
}

export default App;
