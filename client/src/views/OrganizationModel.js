import React from 'react';
import Loading from './Loading.js'
import NotFound from './NotFound.js'
import { Image } from 'react-bootstrap';
import DocumentTitle from 'react-document-title'
import {createLargeInfoGrid, createDataTable, createLink, createExternalLink } from '../components/ModelComp.js'
import { Grid } from '@material-ui/core'
import axios from "axios"
import { Carousel, Row, Col } from 'react-bootstrap'
import { API_ROUTE } from '../components/ApiRoute.js'

export default class OrganizationModel extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      model: null,
      loading: 0
    }

    axios({
      method: 'get',
      url: API_ROUTE + 'organization?id=' + this.props.match.params.id
    })
    .then((response) => {
      this.setState({ model: response.data, loading: 1 })
      console.log(response.data)
    }).catch((error) => {
      this.setState({loading: -1})
    })
  }

  render() {
    if (this.state.loading === 0) {
      return (<Loading/>)
    } else if (this.state.loading === -1) {
      return <NotFound />
    }

    const rows = [
      {id: "Nationality", info: this.state.model["nationality"]},
      {id: "Founded", info: this.state.model["year-founded"] === null ? "Unknown" : this.state.model["year-founded"]},
      {id: "Is Public", info: this.state.model["public"] ? "true" : "false"},
      {id: "Employees", info: this.state.model["employees"] === null ? "Unknown" : this.state.model["employees"]},
      {id: "Number of Missions", info: this.state.model["missions"] === null ? 0 : this.state.model["missions"].length}]

    return(
      <DocumentTitle title={this.state.model.name + " · Budget For Space"}>
      <section className="section">
        <div className="container">
            <header className="section-header">
              <div className="text-center" style={{paddingTop: '2.5em'}}>
                <h1>{this.state.model.name}</h1>
                <br></br>
                <Image src={this.state.model.images[0]}
                  height='300px' width='460px' />
                <hr />
                <Grid container spacing={24} justify="center">
                {createLargeInfoGrid("Description", this.state.model.description)}
                {createDataTable(rows)}
                {createLink("Missions", this.state.model.images, this.state.model.missions, "missions")}
                {createLink("Destinations", this.state.model.images, this.state.model.destinations, "destinations")}
              </Grid>
              <div className="text-center" style={{paddingTop: '2.5em'}}>
                <h2>External Links</h2>
                {createExternalLink(this.state.model)}
                <h2>Images</h2>
                {/* <div style={{width: "320px", height: "240px", textAlign: "center", margin: "auto"}}> */}
                <Row>
                <Col>
                <Carousel>
                  {this.state.model.images.map((d) => {
                    return (<Carousel.Item><img src={d} alt={"Wiki image " + d} height="210px" width="320px"/></Carousel.Item>)
                  })}
                </Carousel>
                </Col>
                </Row>
                {/* </div> */}
              </div>


              </div>
            </header>
          </div>
        </section>
      </DocumentTitle>
    )
  }
}
