import React from 'react'
import axios from "axios"
import { API_ROUTE } from '../components/ApiRoute.js'
import Loading from './Loading.js'
import SearchResult from '../components/SearchResult.js'
import {Tabs, Tab, Container} from 'react-bootstrap'

export default class SearchResults extends React.Component {
  constructor(props) {
    super(props)

    if (this.props.location.state !== undefined) {
      this.state = {
        searchVal: this.props.location.state.searchVal,
        modelData: {"organizations": null, "missions": null, "destinations": null},
        loading: 1,
        currentPage: 1,
        instancesPerPage: 9,
        activeKey: "organizations"
      }
    } else {
      this.state = {
        searchVal: "",
        modelData: {"organizations": null, "missions": null, "destinations": null},
        loading: 1,
        currentPage: 1,
        instancesPerPage: 9,
        activeKey: "organizations"
      }
    }

    this.handleClick = this.handleClick.bind(this);
    this.handleSelect = this.handleSelect.bind(this);

    const models = ["missions", "organizations", "destinations"]
    var result = {}

    models.forEach((e) => {
      axios({
        method: 'get',
        url: API_ROUTE + e + "?results-per-page=1000"
      }).then((response) => {
        result[e] = []

        let searchTerms = [...new Set(this.state.searchVal.toLowerCase().trim().split(" "))]

        response.data[e].forEach((item) => {
          let indices = []
          let desc_indices = []

          searchTerms.forEach((term) => {
            if (term !== "") {
              var start = 0, index;
              const cond = (e) => {
                return (e.index < index && e.index + e.length >= index) || (e.index === index) || (e.index > index && index + term.length >= e.index)
              }

              while ((index = item.name.toLowerCase().indexOf(term, start)) > -1) {
                let existing = indices.find(cond)

                if (existing !== undefined) {
                  if (existing.index === index) {
                    existing.length = Math.max(existing.length, term.length)
                  } else if (existing.index < index && existing.index + existing.length >= index) {
                    existing.length = index + term.length - existing.index
                  } else {
                    existing.length = (existing.index + existing.length - index)
                    existing.index = index
                  }
                } else {
                  indices.push({"index": index, "length": term.length});
                }

                start = index + term.length;
              }

              if (item.description !== null) {
                start = 0;
                while ((index = item.description.toLowerCase().indexOf(term, start)) > -1) {
                  let existing = desc_indices.find(cond)

                  if (existing !== undefined) {
                    if (existing.index === index) {
                      existing.length = Math.max(existing.length, term.length)
                    } else if (existing.index < index && existing.index + existing.length >= index) {
                      existing.length = index + term.length - existing.index
                    } else {
                      existing.length = (existing.index + existing.length - index)
                      existing.index = index
                    }
                  } else {
                    desc_indices.push({"index": index, "length": term.length});
                  }

                  start = index + term.length;
                }
              }
            }
          })

          if (indices.length > 0 || desc_indices.length > 0) {
            item["search-indices"] = indices
            item["description-search-indices"] = desc_indices
            result[e].push(item)
          }
        })

        this.setState({ modelData: result, loading: 1 })
      }).catch((error) => {
        this.setState({loading: -1})
        console.log(error)
      })
    })
  }

  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  handleSelect(key) {
    this.setState({
      activeKey: key,
      currentPage: 1
    })
  }

  render() {
    const data = this.state.modelData

    if (data.organizations === null || data.organizations === undefined || data.missions === null || data.missions === undefined || data.destinations === null || data.destinations === undefined) {
      return(<Loading />)
    }

    const { currentPage, instancesPerPage } = this.state;
    const lastInstance = currentPage * instancesPerPage;
    const firstInstance = lastInstance - instancesPerPage;

    // Total # pages is number of instances we have / instances per page.
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(data[this.state.activeKey].length / instancesPerPage); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map(number => {
      if (number === this.state.currentPage) {
        return (
          <div className="pages"
            key={number}
            id={number}
            onClick={this.handleClick}
            style={{display: "inline-block", backgroundColor: "#4060A8", margin: "7px", height: "24px", width: "24px", borderRadius: "50%", color: "white", cursor: "pointer"}}
          >
            {number}
          </div>
        );
      } else {
        return (
          <div className="pages"
            key={number}
            id={number}
            onClick={this.handleClick}
            style={{display: "inline-block", margin: "7pxad", height: "24px", width: "24px", cursor: "pointer"}}
          >
            {number}
          </div>
        );
      }
    });

    const groups = Object.keys(data).reduce((obj, key) => {
      const previewCards = Object.keys(data[key]).map((d) => {
        return (<SearchResult data={data[key][d]} link={"/" + key + "/" + data[key][d].id} type={key}/>)
      })

      let result = previewCards.slice(firstInstance, lastInstance)

      if (result.length === 0) {
        result = <p>No results found.</p>
      }

      obj[key] = <div><h1 style={{textTransform: "capitalize"}}>{key}</h1>{result}</div>
      return obj
    }, {})

    return (
      <div style={{backgroundColor: "#F8F8F9", minHeight: "100vh"}}>
        <Container style={{padding: "30px"}}>
          <Tabs activeKey={this.state.activeKey} onSelect={this.handleSelect} id="uncontrolled-tab-example">
            <Tab eventKey="organizations" title="Organizations">
              <br/>
              {groups.organizations}
            </Tab>
            <Tab eventKey="destinations" title="Destinations">
              <br/>
              {groups.destinations}
            </Tab>
            <Tab eventKey="missions" title="Missions">
              <br/>
              {groups.missions}
            </Tab>
          </Tabs>
        </Container>
        <div className="footer text-center" id="page-numbers" style={{marginTop: "20px"}}>
          {renderPageNumbers}
        </div>
      </div>
    )
  }
}
