import React from 'react'
import Loading from './Loading.js'
import NotFound from './NotFound.js'
import PreviewCard from '../components/PreviewCard.js'
import DropdownComp from '../components/DropdownComp.js'
import SearchResult from '../components/SearchResult.js'
import DocumentTitle from 'react-document-title'
import axios from "axios"
import { Grid } from '@material-ui/core';
import { API_ROUTE } from '../components/ApiRoute.js'
import {Container, Row, Col, Button} from 'react-bootstrap';
import { gatherData, attributes, fields  } from '../components/ModelHelper.js';



export default class ModelList extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      currentPage: 1,
      instancesPerPage: 9,
      modelData: null,
      defaultData: null,
      loading: 0,
      showDrop: false,
      dropInfo: {selected: []},
      filterCategories: ['default'],
      filter: "default",
      searchVal: "",
      sortAttribute: "",
      sortDirection: 0,
      searched: false,
      dropMore: "More V"
    };

    this.handleClick = this.handleClick.bind(this);
    this.dropdownToggler = this.dropdownToggler.bind(this);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.getData = this.getData.bind(this);
    this.sortBy = this.sortBy.bind(this);
    this.handleFilterChange = this.handleFilterChange.bind(this);
    this.triggerReset = this.triggerReset.bind(this);

    this.getData(this.props.type.toLowerCase())
  }

  componentWillReceiveProps(newProps) {
    this.setState({
      modelData: null,
      showDrop: false,
      loading: 0,
      currentPage: 1,
      searchVal: "",
      searched: false
    })

    this.getData(newProps.type.toLowerCase());
  }

  sortBy(attribute) {
    let direction = this.state.sortAttribute !== attribute ? 0 : this.state.sortDirection === 0 ? 1 : 0
    this.setState({sortDirection: direction, sortAttribute: attribute})

    let newData = {}

    if (direction === 0) {
      newData[this.props.type.toLowerCase()] = this.state.modelData[this.props.type.toLowerCase()].sort((a, b) => {
        if (a[fields[attribute]] === null && b[fields[attribute]] === null) return 0;
        if (a[fields[attribute]] === null) return -1;
        if (b[fields[attribute]] === null) return 1;
        if (attribute === "Missions") return a["missions"].length - b["missions"].length;
        if (!isNaN(a[fields[attribute]])) return a[fields[attribute]] - b[fields[attribute]]
        if (a[fields[attribute]].toString().toLowerCase() < b[fields[attribute]].toString().toLowerCase()) return -1;
        if (a[fields[attribute]].toString().toLowerCase() > b[fields[attribute]].toString().toLowerCase()) return 1;
        return 0;
      })
    } else {
      newData[this.props.type.toLowerCase()] = this.state.modelData[this.props.type.toLowerCase()].sort((a, b) => {
        if (a[fields[attribute]] === null && b[fields[attribute]] === null) return 0;
        if (a[fields[attribute]] === null) return 1;
        if (b[fields[attribute]] === null) return -1;
        if (attribute === "Missions") return b["missions"].length - a["missions"].length;
        if (!isNaN(a[fields[attribute]])) return b[fields[attribute]] - a[fields[attribute]]
        if (a[fields[attribute]].toString().toLowerCase() > b[fields[attribute]].toString().toLowerCase()) return -1;
        if (a[fields[attribute]].toString().toLowerCase() < b[fields[attribute]].toString().toLowerCase()) return 1;
        return 0;
      })
    }

    this.setState({
      modelData: newData
    })
  }

  getData(type) {
    axios({
      method: 'get',
      url: API_ROUTE + type + "?results-per-page=1000"
    })
    .then((response) => {
      this.setState({ modelData: response.data, loading: 1 })
    }).catch((error) => {
      this.setState({loading: -1})
    })

  }

  componentDidUpdate() {
    this.setState(this.filterCategories);
  }
  resetData(type) {
    axios({
      method: 'get',
      url: API_ROUTE + type + "?results-per-page=1000"
    })
    .then((response) => {
      this.setState({ defaultData: response.data, loading: 1 })
    }).catch((error) => {
      this.setState({loading: -1})
    })
  }
  // Handles whenever a user is trying to go to a different page.
  handleClick(event) {
    this.setState({
      currentPage: Number(event.target.id)
    });
  }

  dropdownToggler = () => {
    // syntax is needed since we're depending on a previous state, which might not be reliable
    this.setState((prevState, props) => {
      let next = "";
      if (prevState.dropMore === 'More V') {
        next = 'Less ^'
      } else {
        next = 'More V'
      }
      return {showDrop: !prevState.showDrop, dropMore: next}
    })
  }

  handleSearch(event) {
    event.preventDefault()

    if (this.state.searchVal !== "") {
      this.setState({
        modelData: null,
        loading: 0,
        currentPage: 1,
        sortAttribute: "",
        sortDirection: 0
      })

      let type = this.props.type.toLowerCase()

      axios({
        method: 'get',
        url: API_ROUTE + this.props.type.toLowerCase() + "?results-per-page=1000"
      }).then((response) => {
        var result = {}
        result[type] = []

        let searchTerms = [...new Set(this.state.searchVal.toLowerCase().trim().split(" "))]

        response.data[type].forEach((item) => {
          let indices = []
          let desc_indices = []

          searchTerms.forEach((term) => {
            if (term !== "") {
              var start = 0, index;
              const cond = (e) => {
                return (e.index < index && e.index + e.length >= index) || (e.index === index) || (e.index > index && index + term.length >= e.index)
              }

              while ((index = item.name.toLowerCase().indexOf(term, start)) > -1) {
                let existing = indices.find(cond)

                if (existing !== undefined) {
                  if (existing.index === index) {
                    existing.length = Math.max(existing.length, term.length)
                  } else if (existing.index < index && existing.index + existing.length >= index) {
                    existing.length = index + term.length - existing.index
                  } else {
                    existing.length = (existing.index + existing.length - index)
                    existing.index = index
                  }
                } else {
                  indices.push({"index": index, "length": term.length});
                }

                start = index + term.length;
              }

              if (item.description !== null) {
                start = 0;
                while ((index = item.description.toLowerCase().indexOf(term, start)) > -1) {
                  let existing = desc_indices.find(cond)

                  if (existing !== undefined) {
                    if (existing.index === index) {
                      existing.length = Math.max(existing.length, term.length)
                    } else if (existing.index < index && existing.index + existing.length >= index) {
                      existing.length = index + term.length - existing.index
                    } else {
                      existing.length = (existing.index + existing.length - index)
                      existing.index = index
                    }
                  } else {
                    desc_indices.push({"index": index, "length": term.length});
                  }

                  start = index + term.length;
                }
              }
            }
          })

          if (indices.length > 0 || desc_indices.length > 0) {
            item["search-indices"] = indices
            item["description-search-indices"] = desc_indices
            result[type].push(item)
          }

          this.setState({searched: true})
        })

        console.log(result)

        this.setState({ modelData: result, loading: 1 })
      }).catch((error) => {
        this.setState({loading: -1})
        console.log(error)
      })
    } else {
      this.getData(this.props.type.toLowerCase())
      this.setState({searched: false})
    }
  }

  handleChange(event) {
    this.setState({
      searchVal: event.target.value
    })
  }

  handleFilterChange(eventKey) {
    let elementToGet = eventKey.substring(0,eventKey.indexOf("+"));
    var elementTitle = eventKey.substring(eventKey.indexOf("+")+1);
    // handle inconsistent naming edge cases
    if (elementTitle.toLowerCase() === "manned") {
      elementTitle = "crew";
    } else if (elementTitle.toLowerCase() === "year") {
      elementTitle = "date";
    } else if (elementTitle.toLowerCase() === "founding") {
      elementTitle = "year-founded";
    } else if (elementTitle.toLowerCase() === "discovery") {
      elementTitle = "date-discovered"
    }
    let newData = this.state.modelData;
    let currData = this.state.modelData[this.props.type.toLowerCase()];
    let newList = []
    for (var i = 0; i < currData.length; i++) {
      let curr = currData[i];
      if (elementTitle === "crew") {
        // convert t/f into comparable strings
        if (curr[elementTitle.toLowerCase()] === true) {
          curr[elementTitle.toLowerCase()] = "manned"
        } else {
          curr[elementTitle.toLowerCase()] = "unmanned"
        }
      }
      if (curr[elementTitle.toLowerCase()] === undefined) {
        console.log(curr);
        console.log(elementTitle.toLowerCase());
      }
      if (elementTitle.toLowerCase() === "missions") {
        // compare integer lengths for num missions
        if (curr[elementTitle.toLowerCase()].length === parseInt(elementToGet)) {
          newList.push(curr);
        }
      } else {
        // normal case: compare values
        if (String(curr[elementTitle.toLowerCase()]).toLowerCase() === String(elementToGet).toLowerCase()) {
          newList.push(curr);
        }
      }

    }
    newData[this.props.type.toLowerCase()] = newList;
    this.setState({modelData: newData});
  }

  triggerReset() {
    this.getData(this.props.type.toLowerCase());
    this.setState({searchVal: "", searched: false, sortAttribute: "", sortDirection: 0})
  }

  render() {
    const key = this.props.type.toLowerCase()
    let data = []
    let mp = null;
    if (this.state.modelData !== null) {
      data = this.state.modelData[key];
      mp = gatherData(data, key, attributes, fields);
    }

    // Logic to determine how loading screen is rendered
    if (this.state.loading === -1) {
      return(<NotFound/>)
    } else if (this.state.loading === 0) {
      return(<Loading />)
    }
    // Code to determine pagination rendering instances
    let previewCards = null;
    if (!this.state.searched) {
      previewCards = Object.keys(data).map((d) => {
        return (<PreviewCard data={this.state.modelData[key][d]} link={"/" + key + "/" + data[d].id} type={this.props.type}/>)
      })
    } else {
      previewCards = Object.keys(data).map((d) => {
        return (<SearchResult data={this.state.modelData[key][d]} link={"/" + key + "/" + data[d].id} type={this.props.type}/>)
      })
    }

    const { currentPage, instancesPerPage } = this.state;
    const lastInstance = currentPage * instancesPerPage;
    const firstInstance = lastInstance - instancesPerPage;

    // Total # pages is number of instances we have / instances per page.
    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(previewCards.length / instancesPerPage); i++) {
      pageNumbers.push(i);
    }

    // Go to the page number and store that info (so can know which instances to grab later).
    const renderPageNumbers = pageNumbers.map(number => {
      if (number === this.state.currentPage) {
        return (
          <div className="pages"
            key={number}
            id={number}
            onClick={this.handleClick}
            style={{display: "inline-block", backgroundColor: "#4060A8", margin: "7px", height: "24px", width: "24px", borderRadius: "50%", color: "white", cursor: "pointer"}}
          >
            {number}
          </div>
        );
      } else {
        return (
          <div className="pages"
            key={number}
            id={number}
            onClick={this.handleClick}
            style={{display: "inline-block", margin: "7pxad", height: "24px", width: "24px", cursor: "pointer"}}
          >
            {number}
          </div>
        );
      }
    });

    // Instantiate an instance of pagination
    // Grab the instances of that page we're on.
    const currentInstance = previewCards.slice(firstInstance, lastInstance);
    return (
      <DocumentTitle title={this.props.type + " · Budget For Space"}>
        <div style={{backgroundColor: "#F8F8F9"}}>
          <header className="header bg-fixed" data-overlay="8">
            <Container className="text-left h-100">
              <Row className="align-items-end h-100">
                <Col>
                  <h1 style={{color: "white", marginBottom: "125px"}}>{this.props.type}</h1>
                </Col>
              </Row>
            </Container>
          </header>

          <section className="section">
            <Container style={{position: "relative", top: "-100px", backgroundColor: "#F8F8F9", borderRadius: "3px"}}>
              <div style={{borderBottom: "1px solid #DDDDDD", margin: "0 10px 0 10px"}}>
                <div className="textLeft justify-content-start d-flex" style={{position: "relative", top: "1px"}}>
                  {attributes[this.props.type.toLowerCase()].map((e) => {
                    return(
                      <div onClick={() => this.sortBy(e)} style={{padding: "20px", cursor: "pointer"}}>
                        {e}{e === this.state.sortAttribute && this.state.sortDirection === 1 && <span> ∨</span>}{e === this.state.sortAttribute && this.state.sortDirection === 0 && <span> ∧</span>}
                      </div>
                    )
                  })}
                  <div className="ml-auto" style={{padding: "20px"}}>
                    <Button variant="outline-primary" onClick={this.dropdownToggler}>{this.state.dropMore}</Button>
                  </div>
                </div>
              </div>

              <br/>
              <DropdownComp
                visible={this.state.showDrop}
                handleChange={this.handleChange}
                searchVal={this.state.searchVal}
                handleSearch={this.handleSearch}
                handleFilterChange={this.handleFilterChange}
                type={this.props.type}
                triggerReset={this.triggerReset}
                mp={mp}
              />

              <br/>

              <Grid container spacing={24}>
                {currentInstance}
              </Grid>

              <div className="footer text-center" id="page-numbers" style={{marginTop: "20px"}}>
                {renderPageNumbers}
              </div>
           </Container>
          </section>
        </div>
      </DocumentTitle>
    )
  }
}
