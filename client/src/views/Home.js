import React from 'react';
import {Button, Row, Col, Container, Form} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import {LinkContainer} from 'react-router-bootstrap'
import DocumentTitle from 'react-document-title'

export default class Home extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      searchVal: ""
    }

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.setState({
      searchVal: event.target.value
    })
  }

  render() {
    return (
      <DocumentTitle title="Budget For Space">
        <div>
          <Container style={{height: "100vh", marginTop: "-3.5em"}}>
            <Row className="align-items-center h-100">
              <Col>
                <Row className="align-items-center">
                  <Col lg="5">
                    <h1 style={{fontSize: "5em", textAlign: "left"}}>Budget for Space.</h1>
                    <p style ={{textAlign: "left", marginBottom: "5px"}}>We believe that as the earth becomes a less hospitable home,
                    space exploration is the most valuable investment we can
                    make in our future.</p>
                    <div style={{marginBottom: "20px", paddingRight: "20px"}}>
                      <Form>
                        <Form.Control className="search-bar" placeholder="" onChange={this.handleChange} value={this.state.searchVal}/>
                        <LinkContainer to={{pathname: "/search", state: {searchVal: this.state.searchVal}}} style={{width: "18px", position: "absolute", bottom: "67px", right: "38px", backgroundColor: "transparent", border: "none", padding: "0", outline: "none"}}>
                          <button type="submit">
                            <svg style={{width: "18px", height: "18px"}} version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" enable-background="new 0 0 129 129" className="search-icon">
                              <g>
                                <path class="search-icon-path" d="M51.6,96.7c11,0,21-3.9,28.8-10.5l35,35c0.8,0.8,1.8,1.2,2.9,1.2s2.1-0.4,2.9-1.2c1.6-1.6,1.6-4.2,0-5.8l-35-35   c6.5-7.8,10.5-17.9,10.5-28.8c0-24.9-20.2-45.1-45.1-45.1C26.8,6.5,6.5,26.8,6.5,51.6C6.5,76.5,26.8,96.7,51.6,96.7z M51.6,14.7   c20.4,0,36.9,16.6,36.9,36.9C88.5,72,72,88.5,51.6,88.5c-20.4,0-36.9-16.6-36.9-36.9C14.7,31.3,31.3,14.7,51.6,14.7z"/>
                              </g>
                            </svg>
                          </button>
                        </LinkContainer>
                      </Form>
                    </div>
                    <Link to="/about"><Button variant="primary" style={{borderRadius: "0", marginLeft: "0", backgroundColor: "#4060A8", borderColor: "#4060A8"}}>Learn more about our mission</Button></Link>
                  </Col>
                  <Col lg="7" className="d-none d-lg-block">
                    <img src="https://cdn.dribbble.com/users/14268/screenshots/4578050/spaceman.png" style={{width: "100%"}} alt="spaceman"/>
                  </Col>
                </Row>
              </Col>
            </Row>
            <a href="#second"><div style={{position: "absolute", top: "calc(100vh - 70px)", left: "calc(50vw - 15px)", transform: "rotate(-45deg)", borderLeft: "1px solid black", borderBottom: "1px solid black", width: "30px", height: "30px"}}></div></a>
          </Container>

          <div id="second" style={{backgroundColor: "#F8F8F9", padding: "40px", minHeight: "100vh"}}>
            <Container>
              <Row className="justify-content-center align-items-center" style={{minHeight: "calc(100vh - 80px)"}}>
                <Col className="text-center">
                  <h2 style={{marginBottom: "20px"}}>Explore our space exploration data.</h2>
                  <Row style={{position: "relative"}}>
                    <div style={{position: "absolute", left: "50%", transform: "translateX(-50%)", backgroundColor: "#4060A8", width: "80%", height: "100%"}}></div>
                    <Col md="4" style={{padding: "20px"}} className="d-flex">
                      <LinkContainer to="/missions" style={{padding: "30px", cursor: "pointer"}}>
                        <div className="flex-grow-1 home-card">
                          <img src="https://image.flaticon.com/icons/svg/178/178158.svg" style={{maxWidth: "25%", marginBottom: "15px"}} alt="missions"/>
                          <h4>Missions</h4>
                          <p>The launches and objectives we pursue in space travel.</p>
                          <div class="card-button"></div>
                        </div>
                      </LinkContainer>
                    </Col>
                    <Col md="4" style={{padding: "20px"}} className="d-flex">
                      <LinkContainer to="/organizations" style={{padding: "30px", cursor: "pointer"}}>
                        <div className="flex-grow-1 home-card">
                          <img src="https://image.flaticon.com/icons/svg/1642/1642501.svg" style={{maxWidth: "25%", marginBottom: "15px"}} alt="organizations"/>
                          <h4>Organizations</h4>
                          <p>The agencies that transform space exploration from ideas into action.</p>
                          <div class="card-button"></div>
                        </div>
                      </LinkContainer>
                    </Col>
                    <Col md="4" style={{padding: "20px"}} className="d-flex">
                      <LinkContainer to="/destinations" style={{padding: "30px", cursor: "pointer"}}>
                        <div className="flex-grow-1 home-card">
                          <img src="https://image.flaticon.com/icons/svg/1067/1067736.svg" style={{maxWidth: "25%", marginBottom: "15px"}} alt="destinations"/>
                          <h4>Destinations</h4>
                          <p>The places we go among the stars.</p>
                          <div class="card-button"></div>
                        </div>
                      </LinkContainer>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Container>
          </div>
        </div>
      </DocumentTitle>
    )
  }
}
