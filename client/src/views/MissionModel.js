import React from 'react';
import NotFound from './NotFound.js'
import Loading from './Loading.js'
import axios from 'axios'
import { Image } from 'react-bootstrap';
import DocumentTitle from 'react-document-title'
import {Grid} from '@material-ui/core'
import { Carousel } from 'react-bootstrap'
import {createLargeInfoGrid, createDataTable, createLink, createExternalLink} from '../components/ModelComp.js'
import {API_ROUTE} from '../components/ApiRoute.js'

export default class MissionModel extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      model: null,
      loading: 0
    }

    axios({
      method: 'get',
      url: API_ROUTE + 'mission?id=' + this.props.match.params.id
    })
    .then((response) => {
      this.setState({ model: response.data, loading: 1 })
    }).catch((error) => {
      this.setState({loading: -1})
    })
  }

  render() {
    if (this.state.loading === 0) {
      return (<Loading/>)
    } else if (this.state.loading === -1) {
      return <NotFound />
    }

    const rows = [
      {id: "Crew", info: this.state.model.crew ? "true" : "false"},
      {id: "Status", info: this.state.model.status},
      {id: "Rocket", info: this.state.model.rocket},
      {id: "Country", info: this.state.model.country},
      {id: "Date", info: this.state.model.date}]

    return(
      <DocumentTitle title={this.state.model.name + " · Budget For Space"}>
        <section className="section">
          <div className="container">
            <header className="section-header">
              <div className="text-center" style={{paddingTop: '2.5em'}}>
                <h1>{this.state.model.name}</h1>
                <br></br>
                <Image src={this.state.model.images[0]}
                  height='300px' width='460px' />
                <hr />
                <Grid container spacing={24}>
                {createLargeInfoGrid("Description", this.state.model.description)}
                {createDataTable(rows)}
                {createLink("Destination", this.state.model.images, this.state.model.destinations, "destinations")}
                {createLink("Organizations", this.state.model.images, this.state.model.organizations, "organizations")}
                </Grid>
                <div className="text-center" style={{paddingTop: '2.5em'}}>
                <h2>External Links</h2>
                {createExternalLink(this.state.model)}
                <h2>Images</h2>
                <div style={{width: "320px", height: "240px", textAlign: "center", margin: "auto"}}>
                  <Carousel>
                    {this.state.model.images.map((d) => {
                      return (<Carousel.Item><img src={d} alt={"Wiki image " + d} height="210px" width="320px"/></Carousel.Item>)
                    })}
                  </Carousel>
                </div>
              </div>
              </div>
            </header>
          </div>
        </section>
      </DocumentTitle>
    )
  }
}
