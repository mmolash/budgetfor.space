import React from 'react'
export default class Loading extends React.Component {
    render() {
        return (
            <div style={{textAlign: "center"}}>
                <img src={process.env.PUBLIC_URL +'img/rocket-animation.gif'} alt="rocket gif"/>
                <p>
                    Loading...
                </p>
            </div>
        )
    }
}
