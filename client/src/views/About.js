import React from 'react';
import axios from "axios";
import {Container, Row, Col, Image, Table} from 'react-bootstrap';
import DocumentTitle from 'react-document-title'

/*
  TODO:
  Update Pictures
  Update Bios
*/
const PUBLIC_ID = '11012790';
class AboutCard extends React.Component {
  render() {
    return (
      <Col>
        <div style={{height: "240px", width: "240px", overflow: "hidden", borderRadius: "120px", display: "flex", alignItems: "center", justifyContent: "middle", margin: "auto"}}>
          <Image src={process.env.PUBLIC_URL + "/img/headshots/" + this.props.image} style={{width: "100%", height: "100%", objectFit: "cover"}} />
        </div>
        <br/>
        <div>
        <h3>{this.props.name}</h3>
        <p><b>Bio:</b> {this.props.bio} </p>
        <p><b>Major Responbilities:</b> {this.props.majorResp} </p>
        </div>
      </Col>
    )
  }
}

/**
 * About Page Component
 */
class About extends React.Component {
  signal = axios.CancelToken.source(); // creates signal token for cancelling api call

  constructor(props) {
    super(props);
    this.state = {
      gitlabData: {},
      userCommitData: new Map(),
      userIssueData: new Map(),
      userTestData: new Map(),
      totalNumCommits: 0,
      totalNumIssues: 0,
      totalNumTests: 80,
      teamNames: ['Alan Zeng', 'Anthony Liu', 'Bill Yang', 'Michael Molash', 'Sahil Parikh'],
      imageURLs: ['./../images/0.JPG','../images/1.jpg','../images/2.jpg','../images/3.jpg','../images/4.jpg'],
      majorResp: ['Front-end, Back-end','Front-end', 'Back-end, AWS', 'Front-end, Back-end', 'Front-end, Back-end'],
      authorBio: [
        'Junior CS student who enjoys playing squash with TAs',
        'Junior CS student who thinks ball is life',
        'Junior Math student who is an aspiring dancing coder monkey',
        'Junior CS student who\'s unironically really into GANs',
        'Junior CS student who laughs at his own jokes so that you don’t have to'
      ],
    }
    this.getGitLabCommits = this.getGitLabCommits.bind(this);
  }
  performNameConversion = (name) => {
    switch(name) {
      case 'alanzeng17':
        return 'Alan Zeng';
      case 'sahilparikh98':
        return 'Sahil Parikh'
      default:
        return name;
    }
  }
  /* Perform GET request to GitLab API via Axios */
  getGitLabCommits = (pagenum) => {
    axios({
        method:'get',
        url:'https://gitlab.com/api/v4/projects/'+ PUBLIC_ID +'/repository/commits?ref_name=master&per_page=100&page='+pagenum,
        cancelToken: this.signal.token // token for cancelling api call mid unmount
      })
      .then((response) => {
        if (response === undefined || response.data.length === 0) {
          return; // base case
        }

        this.setState({gitlabData: response}); // pass response into state
        var commitMap = new Map();
        if (pagenum === 1) {
          for (var i = 0; i < this.state.teamNames.length; i++) {
            commitMap.set(this.state.teamNames[i], 0);
          }
        } else {
          commitMap = this.state.userCommitData; // build onto previous page
        }

        // Parse JSON Data, save in consumable format for front-end
        var data = response.data;
        for (i = 0; i < data.length; i++) {
          var name = data[i].author_name;
          name = this.performNameConversion(name);
          if (commitMap.has(name)) {
            commitMap.set(name, commitMap.get(name)+1);
          } else {
            console.log(name + " does not exist!"); // alert to non-crucial error
          }
        }
        this.setState({totalNumCommits: this.state.totalNumCommits+data.length});
        this.setState({userCommitData: commitMap}) // pass parsed stats into state

        if (data.length === 100) {
          this.getGitLabCommits(pagenum+1); // get next page
        }
    })
    .catch((response) => {
      console.log('Gitlab commit stats generation failed!');
    });
  }
  getGitLabIssues = (pagenum) => {
    axios({
      method:'get',
      url:'https://gitlab.com/api/v4/projects/'+ PUBLIC_ID +'/issues?scope=all&per_page=100&page='+pagenum,
      cancelToken: this.signal.token // token for cancelling api call mid unmount
    })
    .then((response) => {
      if (response === undefined || response.data.length === 0) {
        return; // base case
      }

      this.setState({gitlabData: response}); // pass response into state
      var issueMap = new Map();
      if (pagenum === 1) {
        for (var i = 0; i < this.state.teamNames.length; i++) {
          issueMap.set(this.state.teamNames[i], 0);
        }
      } else {
        issueMap = this.state.userIssueData; // build onto previous page
      }

      // Parse JSON Data, save in consumable format for front-end
      var data = response.data;
      for (i = 0; i < data.length; i++) {
        //TODO: Figure out Issue Stats
        var name = data[i].author.name;
        name = this.performNameConversion(name)
        if (issueMap.has(name)) {
          issueMap.set(name, issueMap.get(name)+1);
        }
      }
      this.setState({totalNumIssues: this.state.totalNumIssues+data.length});
      this.setState({userIssueData: issueMap}) // pass parsed stats into state

      if (data.length === 100) {
        this.getGitLabIssues(pagenum+1); // get next page
      }
    })
    .catch((response) => {
      console.log('Gitlab commit stats generation failed!');
    });
  }
  /* Function that runs when component successfully mounts */
  componentDidMount() {
    // fetch gitlab data
    this.getGitLabCommits(1);
    this.getGitLabIssues(1);
  }
  /* Function that runs when component tries to unmount */
  componentWillUnmount() {
    // Cancels linked asynchronous calls with the cancelToken above
    // This is necessary since the Api call will not automatically cancel
    // upon component unmounting, which would cause a potential memory leak
    this.signal.cancel('Api is being canceled');
  }
  render() {
    return (
      <DocumentTitle title="About · Budget For Space">
        <section className="section">
          <div className="container">
            <header className="section-header">
            <div style={{paddingTop: '.5em'}}>
              <h2>About Us</h2>
              <p>
                As climate change becomes an increasing concern for
                the future of our planet, space exploration is more
                important than ever. However, government support for
                space related technologies continues to fall. In order
                to increase public awareness about this lack of governmental
                concern, we decided to create a web application that’d display
                information related to space exploration and technologies.
                Users will be able to explore historical and current data
                on missions to space, the organizations involved in these
                missions, and the destinations traveled to. The missions
                shown will focus specifically on the exploration of potential
                solutions to major problems (climate change, overpopulation,
                etc.) that may no longer be viable to solve on earth.
              </p>
              <h2>Data Integration</h2>
              <p>
                Missions, Organizations, and Destinations are utilized together in order to
                show the full story surrounding space travel. We will focus on missions designed
                to make space for everyone, and highlight projects that are direct stepping stones
                for the advancement of humanity.
              </p>
              <br></br>
              <h2>Meet the Team</h2>
              <Container fluid>
                <Row id='aboutCards'>
                  <AboutCard
                    name={this.state.teamNames[0]}
                    image="0.png"
                    bio={this.state.authorBio[0]}
                    majorResp={this.state.majorResp[0]}
                  />
                  <AboutCard
                    name={this.state.teamNames[1]}
                    image="1.png"
                    bio={this.state.authorBio[1]}
                    majorResp={this.state.majorResp[1]}
                  />
                  <AboutCard
                    name={this.state.teamNames[2]}
                    image="2.png"
                    bio={this.state.authorBio[2]}
                    majorResp={this.state.majorResp[2]}
                  />
                </Row>
                <br /><br />
                <Row>
                  <AboutCard
                    name={this.state.teamNames[3]}
                    image="3.png"
                    bio={this.state.authorBio[3]}
                    majorResp={this.state.majorResp[3]}
                  />
                  <AboutCard
                    name={this.state.teamNames[4]}
                    image="4.png"
                    bio={this.state.authorBio[4]}
                    majorResp={this.state.majorResp[4]}
                  />
                </Row>
              </Container>
              <br />
              <h2>GitLab Statistics</h2>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Name</th>
                    <th># of Commits</th>
                    <th># of Issues</th>
                    <th># of Tests</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Alan Zeng</td>
                    <td>{this.state.userCommitData.get(this.state.teamNames[0])}</td>
                    <td>{this.state.userIssueData.get(this.state.teamNames[0])}</td>
                    <td>10</td>
                    {/* <td>{this.state.userTestData.get(this.state.teamNames[0])}</td> */}
                  </tr>
                  <tr>
                    <td>Anthony Liu</td>
                    <td>{this.state.userCommitData.get(this.state.teamNames[1])}</td>
                    <td>{this.state.userIssueData.get(this.state.teamNames[1])}</td>
                    <td>61</td>
                    {/* <td>{this.state.userTestData.get(this.state.teamNames[1])}</td> */}
                  </tr>
                  <tr>
                    <td>Bill Yang</td>
                    <td>{this.state.userCommitData.get(this.state.teamNames[2])}</td>
                    <td>{this.state.userIssueData.get(this.state.teamNames[2])}</td>
                    <td>20</td>
                    {/* <td>{this.state.userTestData.get(this.state.teamNames[2])}</td> */}
                  </tr>
                  <tr>
                    <td>Michael Molash</td>
                    <td>{this.state.userCommitData.get(this.state.teamNames[3])}</td>
                    <td>{this.state.userIssueData.get(this.state.teamNames[3])}</td>
                    <td>25</td>
                    {/* <td>{this.state.userTestData.get(this.state.teamNames[3])}</td> */}
                  </tr>
                  <tr>
                    <td>Sahil Parikh</td>
                    <td>{this.state.userCommitData.get(this.state.teamNames[4])}</td>
                    <td>{this.state.userIssueData.get(this.state.teamNames[4])}</td>
                    <td>25</td>
                    {/* <td>{this.state.userTestData.get(this.state.teamNames[4])}</td> */}
                  </tr>
                  <tr>
                    <td>Total</td>
                    <td>{this.state.totalNumCommits}</td>
                    <td>{this.state.totalNumIssues}</td>
                    <td>{this.state.totalNumTests}</td>
                  </tr>
                </tbody>
              </Table>
              <br />
              <h2>Data Sources</h2>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Name</th>
                    <td colSpan="3">Description</td>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td><a href='https://launchlibrary.net/docs/1.4/api.html#tippitytop'>LaunchLibrary</a></td>
                    <td>Information about various missions and launches, such as statistics, agencies, and destinations.</td>
                  </tr>
                  <tr>
                    <td><a href='https://spacelaunchnow.me/api/3.3.0/'>SpaceLaunchNow</a></td>
                    <td>Information about various missions and launches, such as statistics, agencies, and destinations.</td>
                  </tr>
                  <tr>
                    <td><a href='https://api.nasa.gov/api.html#Images'>NASA Images</a></td>
                    <td>High quality photo gallery of various destinations from NASA.</td>
                  </tr>
                  <tr>
                    <td><a href='https://exoplanetarchive.ipac.caltech.edu/docs/program_interfaces.html#data'>NASA EXO</a></td>
                    <td>Information about celestial bodies and other space points of interest.</td>
                  </tr>
                  <tr>
                  <td><a href='https://en.wikipedia.org/'>Wikipedia</a></td>
                    <td>Further data about specific destinations not covered by other APIs.</td>
                  </tr>
                </tbody>
              </Table>
              <h2>Tools</h2>
              <p><a href='https://reactjs.org/'>React:</a> Front-end Framework</p>
              <p><a href='https://aws.amazon.com/'>AWS:</a> Webhosting</p>
              <p><a href='https://www.getpostman.com/'>Postman:</a> Testing Backend API calls</p>
              <p><a href='http://flask.pocoo.org/'>Flask:</a> Backend API server</p>
              <p><a href='https://www.namecheap.com/'>NameCheap:</a> Obtained URL</p>
              <p><a href='https://gitlab.com/'>GitLab:</a> Version Control</p>
              <p><a href='https://jestjs.io/en/'>Jest:</a> Front-end testing</p>
              <h2>Links</h2>
              <p><b>GitLab: </b><a href='https://gitlab.com/mmolash/budgetfor.space'>https://gitlab.com/mmolash/budgetfor.space</a></p>
              <p><b>Postman API: </b><a href='https://documenter.getpostman.com/view/6487730/S11LrcXh'>https://documenter.getpostman.com/view/6487730/S11LrcXh</a></p>
            </div>
            </header>
          </div>
        </section>
      </DocumentTitle>
    )
  }
}

export default About;
