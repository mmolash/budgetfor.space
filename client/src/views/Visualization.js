import React from 'react';
import WorldMap from '../components/WorldMap.js'
import LineChart from '../components/LineChart.js'
import {Container, Button} from 'react-bootstrap'
import HorizontalBarChart from '../components/HorizontalBarChart.js'
import DonutChart from '../components/DonutChartOther.js'
import BubbleChart from '../components/BubbleChart.js'
import HorizontalBarChartOther from '../components/HorizontalBarChartOther.js';

export default class Visualization extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      other: false
    }
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick() {
    this.setState((prevState, props) => {
      return {other: !prevState.other}
    });
  }
  render() {
    // Make World Map the last Visualization
    if (this.state.other === false) {
      return (
        <section className="section">
          <div style={{textAlign: "center"}}>
            <section className="section">
            <h1 className="text-center">budgetfor.space</h1>
            <Button onClick={this.handleClick} variant="dark">Toggle Visualizations</Button>
            <hr></hr>
            <Container>
            <h3 className="text-center">Number of Missions per Year</h3>
            <LineChart></LineChart>
            </Container>
            </section>
            <div>
              <h1 className="text-center">Number of Missions by Organization</h1>
              <HorizontalBarChart></HorizontalBarChart>
            </div>
            <div>
              <h3>Heat Map for Number of Organizations per Country</h3>
              <WorldMap></WorldMap>
            </div>
          </div>
        </section>
      )
    } else {
      return (
        <section className="section">
          <div style={{textAlign: "center"}}>
            <h1 className="text-center">catchmeoutside.today</h1>
            <Button onClick={this.handleClick} variant="light">Toggle Visualizations</Button>
            <hr></hr>
            <div>
              <h3 className="text-center">Categories per Groups</h3>
              <DonutChart />
            </div>
            <Container>
              <h3 className="text-center">Number of Members per Organization</h3>
              <BubbleChart></BubbleChart>
            </Container>
            <div>
              <h3 className="text-center">Rating of Spaces</h3>
              <HorizontalBarChartOther></HorizontalBarChartOther>
            </div>
          </div>
        </section>
      )
    }
  }
}
