import React from 'react';
import {IndexLinkContainer, LinkContainer} from 'react-router-bootstrap'
import {Navbar, Nav} from 'react-bootstrap'

export default class Navigation extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      navClass: ""
    }
  }

  componentDidMount() {
    window.onscroll = () => {
      if(window.pageYOffset !== 0) {
        this.setState({
          navClass: "scrolled"
        })
      } else {
        this.setState({
          navClass: ""
        })
      }
    };
  }

  componentWillUnmount() {
    window.onscroll = null;
  }

  render() {
    return (
      <Navbar className={this.state.navClass} fixed="top" bg="clear" variant="light" expand="md" collapseOnSelect>
        <IndexLinkContainer to="/"><Navbar.Brand><img alt="logo" src={process.env.PUBLIC_URL + "/img/logo.png"} width="25" height="25" className="d-inline-block align-middle"/>{' budgetfor.space'}</Navbar.Brand></IndexLinkContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav activeKey={1} className="ml-auto">
            <Nav.Item>
              <LinkContainer to="/missions"><Nav.Link>Missions</Nav.Link></LinkContainer>
            </Nav.Item>
            <Nav.Item>
              <LinkContainer to="/organizations"><Nav.Link>Organizations</Nav.Link></LinkContainer>
            </Nav.Item>
            <Nav.Item>
              <LinkContainer to="/destinations"><Nav.Link>Destinations</Nav.Link></LinkContainer>
            </Nav.Item>
            <Nav.Item>
              <LinkContainer to="/visualizations"><Nav.Link>Visualizations</Nav.Link></LinkContainer>
            </Nav.Item>
            <Nav.Item>
              <LinkContainer to="/about"><Nav.Link>About</Nav.Link></LinkContainer>
            </Nav.Item>
            <Nav.Item>
              <LinkContainer to="/api"><Nav.Link>API</Nav.Link></LinkContainer>
            </Nav.Item>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    )
  }
}
