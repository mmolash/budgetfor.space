import React from 'react';

export default class NotFound extends React.Component {
  render() {
    return (
      <section className="section">
        <p>404</p>
      </section>
    )
  }
}
