import React from 'react';
import {Button} from 'react-bootstrap'
import DocumentTitle from 'react-document-title'
import Sky from 'react-sky'

const spaceIcons = {
    0: "https://image.flaticon.com/icons/svg/124/124574.svg",
    1: "https://image.flaticon.com/icons/svg/124/124570.svg",
    2: "https://image.flaticon.com/icons/svg/124/124567.svg",
    3: "https://image.flaticon.com/icons/svg/124/124560.svg",
    4: "https://image.flaticon.com/icons/svg/124/124559.svg",
    5: "https://image.flaticon.com/icons/svg/124/124582.svg",
    6: "https://image.flaticon.com/icons/svg/124/124558.svg",
    7: "https://image.flaticon.com/icons/svg/124/124588.svg",
    8: "https://image.flaticon.com/icons/svg/124/124542.svg",
    9: "https://image.flaticon.com/icons/svg/124/124573.svg",
    10: "https://image.flaticon.com/icons/svg/124/124586.svg",
    11: "https://image.flaticon.com/icons/svg/124/124548.svg",
    12: "https://image.flaticon.com/icons/svg/124/124555.svg",
    13: "https://image.flaticon.com/icons/svg/305/305891.svg"
  }

export default class ApiLanding extends React.Component {
    render() {
      return (
        <DocumentTitle title="API · Budget For Space">
          <div style={{backgroundColor: "black", width: "100%", height: "calc(100vh - 3.5em)"}}>
            <div style={{backgroundColor: "none", display: "flex", alignItems: "center", justifyContent: "center", textAlign: "center", width: "100%", height: "100%", flexDirection: "column", position: "relative", zIndex: "2"}}>
              <h1 style={{color: "white"}}>Budget for Space API</h1>
              <p style={{color: "white", marginBottom: "1em"}}>We offer an extensive, flexible API for our data.</p>
              <a href="https://documenter.getpostman.com/view/6487730/S11LrcXh"><Button variant="outline-light" style={{borderRadius: "0"}}>API Documentation</Button></a>
              <Sky images={spaceIcons} how={25} time={20} size={'50px'}/>
            </div>

          </div>
        </DocumentTitle>
      )
    }
  }
