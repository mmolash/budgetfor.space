import React from 'react';
import Loading from './Loading.js'
import NotFound from './NotFound.js'
import axios from 'axios'
import { Image } from 'react-bootstrap';
import DocumentTitle from 'react-document-title'
import { Grid } from '@material-ui/core'
import { Carousel } from 'react-bootstrap'
import { createDataTable, createLink } from '../components/ModelComp.js'
import { API_ROUTE } from '../components/ApiRoute.js'

export default class DestinationModel extends React.Component{
  constructor(props) {
    super(props)

    this.state = {
      model: null,
      loading: 0
    }

    axios({
      method: 'get',
      url: API_ROUTE + 'destination?id=' + this.props.match.params.id
    })
    .then((response) => {
      this.setState({ model: response.data, loading: 1 })
    }).catch((error) => {
      this.setState({loading: -1})
    })
  }

  render() {
    if (this.state.loading === 0) {
      return (<Loading/>)
    } else if (this.state.loading === -1) {
      return <NotFound />
    }

    const rows = [
      {id: "Type", info: this.state.model.type},
      {id: "Distance", info: this.state.model.distance},
      {id: "Composition", info: this.state.model.composition},
      {id: "Radius", info: this.state.model.radius},
      {id: "Date Discovered", info: this.state.model["date-discovered"]}
    ]
    /*
      Rubric:
      data
      external links
      embedded images (e.g. Bing, Flickr, Google)
      embedded videos (e.g. Bing, Google, Vimeo)
      embedded maps (e.g. Google Maps)
      embedded social network feeds (e.g. Facebook, Twitter)
      links to Model #2
      links to Model #3
    */
    return(
      <DocumentTitle title={this.state.model.name + " · Budget For Space"}>
      <section className="section">
        <div className="container">
            <header className="section-header">
              <div className="text-center" style={{paddingTop: '2.5em'}}>
                <h1>{this.state.model.name}</h1>
                <br></br>
                <Image src={this.state.model.images[0]}
                  height='300px' width='460px' />
                <hr />
              </div>
              <Grid container spacing={24}>
                {createDataTable(rows)}
                {createLink("Missions", this.state.model.images, this.state.model.missions, "Missions")}
                {createLink("Organizations", this.state.model.images, this.state.model.organizations, "Organizations")}
              </Grid>
              <div className="text-center" style={{paddingTop: '2.5em'}}>
                {/*<h2>External Links</h2>
                {createExternalLink(this.state.model)}*/}
                <h2>Images</h2>
                <div style={{width: "320px", height: "240px", textAlign: "center", margin: "auto"}}>
                  <Carousel>
                    {this.state.model.images.map((d) => {
                      return (<Carousel.Item><img src={d} alt={"Wiki image " + d} height="210px" width="320px"/></Carousel.Item>)
                    })}
                  </Carousel>
                </div>
              </div>
            </header>
          </div>
        </section>
      </DocumentTitle>
    )
  }
}
