import React from 'react';
import { Carousel } from 'react-bootstrap';
import {Link} from 'react-router-dom'
import {API_ROUTE} from './ApiRoute'
import axios from 'axios'

export default class CompCarousel extends React.Component {
    constructor(props) {
        super(props)
        var l = []
        var x = []
        for (var i = 0; i < this.props.info.length; i++) {
            l.push('')
            x.push('')
        }
        this.state = {
            nameList: l,
            imageList: x
        }
    }
    async getData(index, arrI) {
        return axios({
        method: 'get',
        url: API_ROUTE + this.props.type.substring(0, this.props.type.length - 1).toLowerCase() +'?id=' + index
        })
        .then((response) => {
            const newNames = this.state.nameList.slice() //copy the array
            newNames[arrI] = response.data.name //execute the manipulations
            const newImages = this.state.imageList.slice() //copy the array
            newImages[arrI] = response.data.images[0] //execute the manipulations
            this.setState({nameList: newNames, imageList: newImages})
        }).catch((err) => {
            console.log(err);
        })
    
    }
    async componentWillMount() {
        for (var i = 0; i < this.props.info.length; i++) {
            await this.getData(this.props.info[i], i);
        }
    }

    render() {
        const generateItems = () => {
            let info = this.props.info;
            let table = []
            for (var i = 0; i < info.length; i++) {
                var idx = info[i];
                let link = "/"+this.props.type+"/"+idx;
                table.push(
                    <Carousel.Item style={{height: "220px"}}>
                            <div style={{width: "100%", paddingTop: "100%", position: "relative", textAlign: "center", height: "200px"}}>
                            <img src={this.state.imageList[i]} 
                            style={{width: "100%", position: "absolute", 
                            top: "0", left: "0", bottom: "0", right: "0", 
                            height: "100%", objectFit: "cover", overflow: "hidden",
                            marginLeft: "auto", marginRight: "auto", display: "block"}} 
                            alt="card"/>
                            </div>
                        <Carousel.Caption>
                            <Link to={link} style={{color: "#ffffff"}}>
                                <h5 style={{color: "white", "text-shadow": "-1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000"}}>
                                  {this.state.nameList[i]}
                                </h5>
                            </Link>
                        </Carousel.Caption>
                    </Carousel.Item>
                )
                
            }
            return table;
        }

        return (

        <Carousel fade="true" style={{width: "320px"}}>
            {generateItems()}
        </Carousel>
        )
    }
}
