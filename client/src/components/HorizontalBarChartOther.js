import React from 'react'
import rd3 from 'react-d3-library'
import * as d3 from 'd3'

const RD3Component = rd3.Component;



async function loadChart() {
    var node = document.createElement('div');

    const spacesData = await d3.json("https://api.catchmeoutside.today/spaces?page=1")
    const pageTwoData = await d3.json("https://api.catchmeoutside.today/spaces?page=2")

    pageTwoData.forEach((element) => {
        spacesData.push(element);
    });
    
    var data = []
    spacesData.forEach((element) => {
        data.push({name: element["name"], value: element["rating"]});
    });

    console.log(data)

    var margin = ({top: 30, right: 0, bottom: 10, left: 300})

    var width = 800;
    var height = data.length * 25 + margin.top + margin.bottom

    var svg = d3.select(node).append("svg")
      .attr("width", width)
      .attr("height", height);
    
    

    const x = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.value)])
        .range([margin.left, width - margin.right])
    
    const y = d3.scaleBand()
        .domain(data.map(d => d.name))
        .range([margin.top, height - margin.bottom])
        .padding(0.1)
    
    const xAxis = g => g
        .attr("transform", `translate(0,${margin.top})`)
        .call(d3.axisTop(x).ticks(width / 80))
        .call(g => g.select(".domain").remove())

    const yAxis = g => g
        .attr("transform", `translate(${margin.left},0)`)
        .call(d3.axisLeft(y).tickSizeOuter(0))

    const format =  x.tickFormat(20);

    
    svg.append("g")
            .attr("fill", "steelblue")
        .selectAll("rect")
        .data(data)
        .join("rect")
            .attr("x", x(0))
            .attr("y", d => y(d.name))
            .attr("width", d => x(d.value) - x(0))
            .attr("height", y.bandwidth());
    
    svg.append("g")
            .attr("fill", "white")
            .attr("text-anchor", "end")
            .style("font", "12px sans-serif")
        .selectAll("text")
        .data(data)
        .join("text")
            .attr("x", d => x(d.value) - 4)
            .attr("y", d => y(d.name) + y.bandwidth() / 2)
            .attr("dy", "0.35em")
            .text(d => format(d.value));
    
    svg.append("g")
        .call(xAxis);
    
    svg.append("g")
        .call(yAxis);

    return node;
    
    
}

export default class HorizontalBarChartOther extends React.Component {
    constructor(props) {
      super(props);
      this.state = {d3: ''}
    }

  
    componentDidMount() {
      loadChart().then((result) => {
        this.setState({d3: result});
      })
    }
  
    render() {
      return(<div><RD3Component data={this.state.d3}/></div>)
    }
  }