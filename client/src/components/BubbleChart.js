import React from 'react'
import rd3 from 'react-d3-library'
import * as d3 from 'd3'
import axios from 'axios'

const RD3Component = rd3.Component;

function guid() {
    function _p8(s) {
        var p = (Math.random().toString(16)+"000000000").substr(2,8);
        return s ? "-" + p.substr(0,4) + "-" + p.substr(4,4) : p ;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

async function loadChart(data) {
  console.log(data)

  var node = document.createElement('div');

  const div = d3
    .select('body')
    .append('div')
    .attr('class', 'tooltip')
    .style('opacity', 0)
    .style('background-color', 'rgba(0, 0, 0, 0.5)')
    .style('padding', '10px')
    .style('border-radius', '5px')
    .style('color', 'white')

  var diameter = 960
  // var color = d3.scale.linear()
  //   .domain([0, 29509])
  //   .range(["blue", "green"]);

  console.log(d3.interpolateRdYlBu(10000/29509))

  let format = d3.format(",d")

  let pack = data => d3.pack()
    .size([diameter - 2, diameter - 2])
    .padding(3)(d3.hierarchy({children: data})
    .sum(d => d.count))

  const root = pack(data);

  const svg = d3.select(node).append("svg")
      .attr("width", diameter)
      .attr("height", diameter)
      .attr("font-size", 10)
      .attr("font-family", "sans-serif")
      .attr("text-anchor", "middle");

  const leaf = svg.selectAll("g")
    .data(root.leaves())
    .join("g")
      .attr("transform", d => `translate(${d.x + 1},${d.y + 1})`);

  leaf.append("circle")
      .attr("id", d => (d.leafUid = guid()))
      .attr("r", d => d.r)
      .attr("fill-opacity", 0.7)
      .attr("fill", d => d3.interpolateRainbow(d.data.count/40000))
      .style('cursor', 'pointer')
      .on('mouseover', d => {
        div
          .transition()
          .duration(200)
          .style('opacity', 0.9)
        div
          .html(d.data.name + '<br/>' + d.data.count)
          .style('left', d3.event.pageX + 28 + 'px')
          .style('top', d3.event.pageY - 28 + 'px');
      })
      .on("mousemove", function() {
          div
            .style("top", (d3.event.pageY - 28)+"px")
            .style("left",(d3.event.pageX + 28)+"px");
      })
      .on('mouseout', () => {
        div
          .transition()
          .duration(100)
          .style('opacity', 0);
      });

  leaf.append("clipPath")
      .attr("id", d => (d.clipUid = guid()))
    .append("use")
      .attr("xlink:href", d => d.leafUid.href);


  leaf.append("title")
      .text(d => `${d.data.name}\n${format(d.data.count)}`);

  console.log(node)

  return node
}

export default class BubbleChart extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          data: {},
          d3: ''
      }
      this.getData = this.getData.bind(this);
      this.gatherData = this.gatherData.bind(this);
  }
  getData = (pagenum) => {
      return axios({
          method: 'get',
          url: "https://api.catchmeoutside.today/groups?page=" + (pagenum)
        })
        .then((response) => {
          if (response === undefined || response.data.length === 0) {
              return []; // base case
            }
          var newData = this.state.data;
          newData[pagenum] = response.data;

          if (response.data.length > 0) {
              return response.data;
          } else {
              return [];
          }
        }).catch((error) => {
          return [];
        });
  }

  async gatherData(data) {
      console.log(data)
      var temp = []
      for (var i = 1; i <= 9; i++) {
          var arr = this.state.data[i];
          for (var j = 0; j < arr.length; j++) {
              temp.push({name: arr[j].name, count: arr[j]["member_count"]})
          }
      }

      return temp
  }

  async componentDidMount() {
      var pages = [1,2,3,4,5,6,7,8,9,10];
      //await this.getData(pagenum)
      const promises = pages.map(page => this.getData(page));
      let temp = []
      await Promise.all(promises).then((res) => {
        temp = res
      });
      var data = await this.gatherData(temp);
      console.log(data)

      loadChart(data).then((result) => {
        this.setState({d3: result});
      })
  }

  render() {
    console.log(this.state.d3)
    return(<div><RD3Component data={this.state.d3}/></div>)
  }
}
