const gatherData = (data, key, attributes, fields) => {
    var mp = new Map();
    var filters = attributes[key];
    for (var x = 1; x < filters.length; x++) {
      mp.set(filters[x], new Set());
    }
    for (var i = 0; i < data.length; i++) {
      var curr = data[i];
      for (var j = 1; j < filters.length; j++) {
        var l = mp.get(filters[j]);
        if (curr[fields[filters[j]]] === undefined) {
          console.log(filters[j]);
          console.log(fields[filters[j]]);
          console.log(curr)
        }
        var ele = curr[fields[filters[j]]];
        if (fields[filters[j]] === 'missions') {
          l.add(ele.length);
        }
        else {
          l.add(ele);
        }
      }
    }
    console.log(mp);
    return mp;
  }

  const attributes = {
    "organizations": ["Name", "Nationality", "Founding", "Public", "Employees", "Missions"],
    "destinations": ["Name", "Type", "Distance", "Composition", "Radius", "Discovery"],
    "missions": ["Name", "Manned", "Status", "Rocket", "Country", "Year"]
  }
  
  const fields = {
    "Name": "name",
    "Nationality": "nationality",
    "Founding": "year-founded",
    "Public": "public",
    "Employees": "employees",
    "Missions": "missions",
    "Type": "type",
    "Distance": "distance",
    "Composition": "composition",
    "Radius": "radius",
    "Discovery": "date-discovered",
    "Manned": "crew",
    "Status": "status",
    "Rocket": "rocket",
    "Country": "country",
    "Year": "date"
  }
export { gatherData, attributes, fields };