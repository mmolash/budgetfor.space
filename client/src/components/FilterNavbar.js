import React from 'react';
import { Nav } from 'react-bootstrap'

export default class FilterNavbar extends React.Component {
    handleOnSelect = (eventKey, event) => {
        console.log("handle")
        console.log(eventKey)
        this.props.updateFilter(eventKey);
    }
    render() {
        const renderFilter = Object.keys(this.props.filterCategories).map((d) => {
            return (<Nav.Item><Nav.Link eventKey={this.props.filterCategories[d]} onSelect={this.handleOnSelect}>{this.props.filterCategories[d]}</Nav.Link></Nav.Item>)
        })
        return (
            <Nav fill variant="tabs" defaultActiveKey="All">
                {renderFilter}
            </Nav>
        )
    }
}