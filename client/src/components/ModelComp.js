import React from 'react';
import {Card, CardContent, Typography, Grid, Table, TableBody, TableCell, TableRow} from '@material-ui/core'
import CompCarousel from '../components/CompCarousel.js'
const createInfoGrid = (title, description) => {
    return (
      <Grid item xs={6}>
      <Card style={{height: "200px", overflow:"scroll"}} >
      <CardContent>
        <Typography variant="h5" component="h2">
          {title}
        </Typography>
        <hr />
        <Typography component="p">
          {description}
        </Typography>
      </CardContent>
    </Card>
    </Grid>
    )
}
const createLargeInfoGrid = (title, description) => {
  return (
    <Grid item xs={12}>
    <Card id='largeInfo' style={{height: "200px", overflow:"scroll"}} >
    <CardContent>
      <Typography variant="h5" component="h2">
        {title}
      </Typography>
      <hr />
      <Typography component="p">
        {description}
      </Typography>
    </CardContent>
  </Card>
  </Grid>
  )
}
const createDataTable = (rows) => {
    for (var i = 0; i < rows.length; i++) {
      if (rows[i].info === null || rows[i].info === undefined || rows[i].info === "" || rows[i].info === "null")
        rows[i].info = "N/A"
    }
    return (
      <Grid item xs={4}>
      <Card id='dataTable' style={{height: "320px", overflow:"scroll"}} >
      <CardContent>
        <Typography variant="h5" component="h2">
          Attribute Data
        </Typography>
        <Table>
        <TableBody>
          {rows.map(row => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row">
                {row.id}
              </TableCell>
              <TableCell align="right">{row.info}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      </CardContent>
    </Card>
    </Grid>
    )
  }
//TODO: Each "link" needs
const createLink = (name, pics, links, type) => {
  return (
    <Grid item xs={4}>
    <Card style={{height: "320px"}} >
    <CardContent>
    <Typography variant="h5" component="h2">
        {name}
    </Typography>
    <br />
    <div style={{margin: "auto"}}>

    <CompCarousel images={pics} info={links} type={type}></CompCarousel>
    </div>
    </CardContent>
    </Card>
    </Grid>
  )
}

const createExternalLink = (model) => {
    let list = []
    var modelLinks = undefined
    if(model.externalLinks) {
      modelLinks = model.externalLinks
    } else if(model.externallinks) {
      modelLinks = model.externallinks
    }
    if(modelLinks) {
      for (let i = 0; i < modelLinks.length; i++) {
        list.push(<p><a href={modelLinks[i]}>{modelLinks[i]}</a></p>)
      }
    }
    return list;
}

export {createInfoGrid, createLargeInfoGrid,createDataTable, createLink, createExternalLink}
