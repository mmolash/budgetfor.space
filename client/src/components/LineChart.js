import React from 'react'
import { API_ROUTE } from '../components/ApiRoute.js'
import rd3 from 'react-d3-library'
import * as d3 from 'd3'

const RD3Component = rd3.Component;

async function loadChart() {
  var node = document.createElement('div');

  var width = 960,
      height = 500;

  var svg = d3.select(node).append("svg")
      .attr("width", width)
      .attr("height", height);

  const margin = ({top: 20, right: 30, bottom: 30, left: 40})
  const data = await d3.json(API_ROUTE + "missions?results-per-page=1000")

  var years = []
  data["missions"].forEach((element) => {
    years.push(element["date"])
  })

  var info = {}
  years.forEach((element) => {
    if (element in info)
      info[element] += 1
    else
      info[element] = 1
  })

  for (var i = d3.min(Object.keys(info), d => d); i < d3.max(Object.keys(info), d => d); i++) {
    if (!(i in info))
      info[i] = 0
  }

  var res = []
  Object.keys(info).forEach((key) => {
    res.push({year: key, missions: info[key]})
  })

  console.log(res)

  const x = d3.scaleLinear()
    .domain([d3.min(res, d => d.year), d3.max(res, d => d.year)]).nice()
    .range([margin.left, width - margin.right])

  const y = d3.scaleLinear()
    .domain([0, d3.max(Object.values(info), d => d)]).nice()
    .range([height - margin.bottom, margin.top])

  const xAxis = g => g
    .attr("transform", `translate(0,${height - margin.bottom})`)
    .call(d3.axisBottom(x).ticks(width / 80).tickSizeOuter(0).tickFormat(d3.format("d")))

  const yAxis = g => g
      .attr("transform", `translate(${margin.left},0)`)
      .call(d3.axisLeft(y))
      .call(g => g.select(".domain").remove())
      .call(g => g.select(".tick:last-of-type text").clone()
          .attr("x", 3)
          .attr("text-anchor", "start")
          .attr("font-weight", "bold")
          .text(data.y))

  const line = d3.line()
    .defined(d => d !== undefined)
    .x(d => x(d.year))
    .y(d => y(d.missions))

  svg.append("g")
      .call(xAxis);

  svg.append("g")
      .call(yAxis);

  svg.append("path")
      .datum(res)
      .attr("fill", "none")
      .attr("stroke", "steelblue")
      .attr("stroke-width", 1.5)
      .attr("stroke-linejoin", "round")
      .attr("stroke-linecap", "round")
      .attr("d", line);

  return node
}

export default class LineChart extends React.Component {
  constructor(props) {
    super(props);
    this.state = {d3: ''}
  }

  componentDidMount() {
    loadChart().then((result) => {
      this.setState({d3: result});
    })
  }

  render() {
    return(<div><RD3Component data={this.state.d3}/></div>)
  }
}
