import React from 'react';
import {Container, Row, Col, Button, Dropdown, DropdownButton, Form} from 'react-bootstrap';

const attributes = {
    "organizations": ["Nationality", "Founding", "Public", "Employees", "Missions"],
    "destinations": ["Type", "Distance", "Composition", "Radius", "Discovery"],
    "missions": ["Manned", "Status", "Rocket", "Country", "Year"]
  }
export default class DropdownComp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            filterNames: [],
            titles: {
                "Nationality": "Nationality",
                "Founding": "Year Founded",
                "Public": "Public",
                "Employees": "Employees",
                "Missions": "Num Missions",
                "Type": "Type",
                "Distance": "Distance",
                "Composition": "Composition",
                "Radius": "Radius",
                "Discovery": "Date Discovered",
                "Manned": "Crew",
                "Status": "Status",
                "Rocket": "Rocket",
                "Country": "Country",
                "Year": "Date"
              },
              defs: {
                "Nationality": "Nationality",
                "Founding": "Year Founded",
                "Public": "Public",
                "Employees": "Employees",
                "Missions": "Num Missions",
                "Type": "Type",
                "Distance": "Distance",
                "Composition": "Composition",
                "Radius": "Radius",
                "Discovery": "Date Discovered",
                "Manned": "Crew",
                "Status": "Status",
                "Rocket": "Rocket",
                "Country": "Country",
                "Year": "Date"
              }
        }
        this.handleFilterChange = this.handleFilterChange.bind(this);
        this.triggerReset = this.triggerReset.bind(this);
    }
    handleFilterChange = (eventKey) => {
        let elementToSet = eventKey.substring(0,eventKey.indexOf("+"));
        let elementTitle = eventKey.substring(eventKey.indexOf("+")+1);
        let newTitles = this.state.titles;

        newTitles[elementTitle] = elementToSet;
        this.setState({titles: newTitles});

        this.props.handleFilterChange(eventKey);
        //console.log(newTitles);
    }
    triggerReset = () => {
        var copy = JSON.parse(JSON.stringify(this.state.defs));
        this.setState({titles: copy})
        this.props.triggerReset();
    }
    generateButtons = () => {
        var newLayer = [];
        for (var d = 0; d < attributes[this.props.type.toLowerCase()].length; d++) {
            var index = attributes[this.props.type.toLowerCase()][d];
            //console.log("index: " + index);
            var layer = [];
            var mpList = Array.from(this.props.mp.get(index)).sort();
            if (index === 'Public') {
                mpList = ['true', 'false']
            } else if (index === "Manned") {
                mpList = ['Unmanned', 'Manned']
            }
            let def = index;
            if (def === 'Manned') {
                def = "Crew";
            }
            // mpList.unshift(def); // add default option to beginning of array
            //console.log(mpList);
            mpList.sort(function(a, b) {
                if (String(a).length === String(b).length) {
                    return a - b;
                } else {
                    return String(a).length - String(b).length;
                }
            })
            for (var i = 0; i < mpList.length; i++) {
                let curr = mpList[i];
                layer.push(<Dropdown.Item eventKey={curr +"+"+index} onSelect={this.handleFilterChange} as="button">{curr}</Dropdown.Item>);
            }
            newLayer.push(<Col xs={2}><DropdownButton id="dropdown-item-button" title={this.state.titles[index]}>{layer}</DropdownButton></Col>);
         }
         return newLayer;
    }
    render() {
        if (this.props.visible === false) {
            return (<div></div>);
        }


        return (
            <Container style={{marginBottom: '20px', marginLeft: '10px'}}>
                <Row className="justify-content-md-center" style={{marginBottom: "10px"}}>
                    <Col style={{margin: "0 10px 10px 10px"}} md="6">
                        <Form onSubmit={this.props.handleSearch}>
                            <Form.Control className="search-bar" placeholder="" onChange={this.props.handleChange} value={this.props.searchVal}/>
                            <button type="submit" style={{width: "18px", position: "absolute", top: "7px", right: "18px", backgroundColor: "transparent", border: "none", padding: "0", outline: "none"}}>
                            <svg style={{width: "18px", height: "18px"}} version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" enable-background="new 0 0 129 129" className="search-icon">
                                <g>
                                <path class="search-icon-path" d="M51.6,96.7c11,0,21-3.9,28.8-10.5l35,35c0.8,0.8,1.8,1.2,2.9,1.2s2.1-0.4,2.9-1.2c1.6-1.6,1.6-4.2,0-5.8l-35-35   c6.5-7.8,10.5-17.9,10.5-28.8c0-24.9-20.2-45.1-45.1-45.1C26.8,6.5,6.5,26.8,6.5,51.6C6.5,76.5,26.8,96.7,51.6,96.7z M51.6,14.7   c20.4,0,36.9,16.6,36.9,36.9C88.5,72,72,88.5,51.6,88.5c-20.4,0-36.9-16.6-36.9-36.9C14.7,31.3,31.3,14.7,51.6,14.7z"/>
                                </g>
                            </svg>
                            </button>
                        </Form>
                    </Col>
                </Row>
                <Row className="justify-content-md-center">
                    {this.generateButtons()}
                    <Col xs={2}>
                        <Button class="danger" id="dropdown-item-button" onClick={this.triggerReset} title="Reset">Reset</Button>
                    </Col>
                </Row>
            </Container>
        )
    }
}