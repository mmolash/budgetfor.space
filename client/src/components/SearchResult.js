import React from 'react'
import {Col} from 'react-bootstrap'
import {LinkContainer} from 'react-router-bootstrap'

export default class SearchResult extends React.Component {
  render() {
    const data = this.props.data

    var name = <h6>{data.name}</h6>
    if (data["search-indices"] !== undefined && data["search-indices"].length > 0) {
      const sorted = data["search-indices"].sort((a, b) => {
        return a.index - b.index;
      })

      const text = sorted.map((item, index) => {
        if (index < sorted.length - 1)
          return (<span><span style={{color: "#4060A8", fontWeight: "800"}}>{data.name.substring(item.index, item.index + item.length)}</span>{data.name.substring(item.index + item.length, sorted[index + 1].index)}</span>)
        else
          return (<span><span style={{color: "#4060A8", fontWeight: "800"}}>{data.name.substring(item.index, item.index + item.length)}</span>{data.name.substring(item.index + item.length)}</span>)
      })
      name = <h6>{data.name.substring(0, sorted[0].index)}{text}</h6>
    }

    var description = ""
    if (data.description !== null) {
      description = <p>{data.description.substring(0, 200)}</p>
    }
    if (data["description-search-indices"] !== undefined && data["description-search-indices"].length > 0) {
      const sorted = data["description-search-indices"].sort((a, b) => {
        return a.index - b.index;
      })


      const desc = data.description.substring(Math.max(0, sorted[0].index - 10), Math.max(0, sorted[0].index - 10) + 200) + " ..."

      const text = sorted.map((item, index) => {
        if (index < sorted.length - 1)
          return (<span><span style={{color: "#4060A8", fontWeight: "800"}}>{desc.substring(item.index - Math.max(0, sorted[0].index - 10), item.index - Math.max(0, sorted[0].index - 10) + item.length)}</span>{desc.substring(item.index - Math.max(0, sorted[0].index - 10) + item.length, sorted[index + 1].index - Math.max(0, sorted[0].index - 10))}</span>)
        else
          return (<span><span style={{color: "#4060A8", fontWeight: "800"}}>{desc.substring(item.index - Math.max(0, sorted[0].index - 10), item.index - Math.max(0, sorted[0].index - 10) + item.length)}</span>{desc.substring(item.index - Math.max(0, sorted[0].index - 10) + item.length - Math.max(0, sorted[0].index - 10))}</span>)
      })
      description = <p>{desc.substring(0, sorted[0].index - Math.max(0, sorted[0].index - 10))}{text}</p>
    }

    return (
      <Col xl="12" className="d-flex" style={{padding: "15px"}}>
        <LinkContainer to={this.props.link} style={{backgroundColor: "white", borderRadius: "3px", overflow: "hidden", boxShadow: "0px 5px 1px #4060A8", cursor: "pointer"}}>
          <div className="flex-grow-1">
            <div style={{padding: "10px"}}>
              <span style={{textDecoration: "underline", cursor: "pointer", fontSize: "2em"}}>{name}</span>
              {description}
            </div>
          </div>
        </LinkContainer>
      </Col>
    );
  }
}
