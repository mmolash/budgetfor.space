import React from 'react';
//import {Card, CardActionArea, CardContent, CardMedia, Typography, Grid} from '@material-ui/core'
//import DynamicFont from 'react-dynamic-font';
import { Redirect } from 'react-router';
import NotFound from '../views/NotFound.js'
import {Col} from 'react-bootstrap'
import {LinkContainer} from 'react-router-bootstrap'

const fields = {
  "Organizations": ["nationality", "year-founded", "public", "employees", "num-missions"],
  "Missions": ["crew", "status", "rocket", "country", "date"],
  "Destinations": ["type", "distance", "composition", "radius", "date-discovered"]
}

export default class PreviewCard extends React.Component {
  handleOnClick = () => {
    // some action...
    // then redirect
    this.setState({redirect: true});
  }
  constructor(props) {
    super(props);
    this.state = {
      redirect: false
    }
  }
  render() {
    if (this.state.redirect) {
      return <Redirect push to={this.props.link}  />;
    }
    const data = this.props.data

    if (data === null || data === undefined) {
      return (<NotFound />)
    }

    return(
      <Col md="4" className="d-flex" style={{padding: "15px"}}>
        <LinkContainer to={this.props.link} style={{backgroundColor: "white", borderRadius: "3px", overflow: "hidden", boxShadow: "0px 5px 1px #4060A8", cursor: "pointer"}}>
          <div className="flex-grow-1">
            <div style={{width: "100%", paddingTop: "100%", position: "relative"}}>
              <img src={data.images[0]} style={{width: "100%", position: "absolute", top: "0", left: "0", bottom: "0", right: "0", height: "100%", objectFit: "cover", overflow: "hidden"}} alt="card"/>
            </div>
            <div style={{padding: "10px"}}>
              <h6>{data.name}</h6>
              {fields[this.props.type].map((d) => {
                return(<div style={{marginTop: "-7px"}}><small><b style={{"text-transform": "capitalize"}}>{d.replace("-", " ") + ": "}</b>{data[d] === null || data[d] === "null" ? "unknown" : d === "crew" ? data[d] ? "manned" : "unmanned" : data[d] === true ? "true" : data[d] === false ? "false" : d === "num-missions" ? data["missions"] === null ? 0 : data["missions"].length : data[d]}</small></div>)
              })}
            </div>
            <div style={{backgroundColor: "#4060A8", width: "24px", height: "24px", borderRadius: "50%", position: "absolute", bottom: "25px", right: "25px", color: "white", textAlign: "center"}}>→</div>
          </div>
        </LinkContainer>
      </Col>
    )

    /*return(
      <Grid item xs={4}>
      <Card onClick={this.handleOnClick} style={{height: "400px"}}>
      <CardActionArea>
        <CardMedia style={{height:"200px", overflow: "hidden",backgroundColor: '#ffffff'}} src={data.images[0]}>
            <img
            src={data.images[0]}
            style={{maxWidth:"100%", height: "auto",
            display: "block", marginLeft: "auto", marginRight: "auto"}}
            alt={data.name}
          />
        </CardMedia>
        <CardContent style={{fontSize: "30px"}}>
            <DynamicFont content={data.name} />
        </CardContent>
      </CardActionArea>
      <CardContent>
        <Typography>
          {fields[this.props.type].map((d) => {
            return(<div><b>{d}:</b> {data[d] === null ? "Unknown" : data[d] === true ? "true" : data[d] === false ? "false" : d === "num-missions" ? data["missions"] === null ? 0 : data["missions"].length : data[d]}</div>)
          })}
        </Typography>
      </CardContent>
    </Card>
    </Grid>
    )*/
  }
}
