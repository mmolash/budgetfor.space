import React from 'react';
import Datamap from 'datamaps/dist/datamaps.all.min.js';
import { gatherData, attributes, fields } from './ModelHelper.js'
import axios from "axios"
import { API_ROUTE } from '../components/ApiRoute.js'

class WorldDatamap extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            fills: {
                1: '#D8BDBD',
                2: '#D49E9D',
                3: '#D49E9D',
                4: '#D07F7E',
                5: '#CB5F5E',
                6: '#C7403F',
                7: '#C3211F',
                8: '#BF0200',
                defaultFill: '#dddddd'
            },
            data: {},
            countryCount: new Map(),
            apiData: null
        }
        this.getData = this.getData.bind(this);
        this.createCountryCounts = this.createCountryCounts.bind(this);
        //this.getData();

    }
    async getData() {
        const type = 'organizations';
        await axios({
            method: 'get',
            url: API_ROUTE + type + "?results-per-page=1000"
          })
          .then((response) => {
            this.setState({apiData: response.data});
          }).catch((error) => {
        });
    }
    getFillKey(countryCode) {

    }
    convertCountryToCode(country) {
        const convert = {
            "United States": "USA",
            "India": "IND",
            "Iran, Islamic Republic of": "IRN",
            "Israel": "ISR",
            "Japan": "JPN",
            "Korea, Democratic People's Republic of": "PRK",
            "Korea, Republic of": "KOR",
            "France": "FRA",
            "Russian Federation": "RUS",
            "China": "CHN",
            "Belarus": "",
            "Luxembourg": "",
            "Canada": ""
        }
        return convert[country]
    }
    createCountryCounts() {
        if (this.state.apiData !== null) {
            var maxVal = -1;
            var md = gatherData(this.state.apiData['organizations'], 'organizations', attributes, fields);
            var countries = Array.from(md.get('Nationality'));
            for (var i = 0; i < this.state.apiData['organizations'].length; i++) {
                var curr = this.state.apiData['organizations'][i]['nationality'];
                if (curr !== undefined && curr !== null) {
                    if (this.state.countryCount.has(curr)) {
                        maxVal = Math.max(this.state.countryCount.get(curr)+1, maxVal);
                        this.state.countryCount.set(curr, this.state.countryCount.get(curr)+1);
                    } else {
                        this.state.countryCount.set(curr, 1);
                    }
                }
            }
            var newData = this.state.data;
            for (var j = 0; j < countries.length; j++) {
                let count = this.state.countryCount.get(countries[j]);
                var code = this.convertCountryToCode(countries[j]);
                if (count >= 7 * (maxVal / 8.0)) {
                    newData[code] = {fillKey: 8, count: count};
                }
                else if (count >= 6 * (maxVal / 8.0)) {
                    newData[code] = {fillKey: 7, count: count};
                }
                else if (count >= 5 * (maxVal / 8.0)) {
                    newData[code] = {fillKey: 6, count: count};
                }
                else if (count >= 4 * (maxVal / 8.0)) {
                    newData[code] = {fillKey: 5, count: count};
                }
                else if (count >= 3 * (maxVal / 8.0)) {
                    newData[code] = {fillKey: 4, count: count};
                }
                else if (count >= 2 * (maxVal / 8.0)) {
                    newData[code] = {fillKey: 3, count: count};
                }
                else if (count >= 1 * (maxVal / 8.0)) {
                    newData[code] = {fillKey: 2, count: count};
                } else if (count > 0){
                    newData[code] = {fillKey: 1, count: count};
                }
            }
            this.setState({data: newData});
        }
    }
    async componentDidMount() {
        await this.getData();
        this.createCountryCounts();
        var map = new Datamap(Object.assign({}, {
            ...this.props
        }, {
            element: this.refs.container,
            geographyConfig: {
                highlightBorderColor: '#bada55',
               popupTemplate: function(geography, data) {
                    if (data.count > 0) {
                        return '<div class="hoverinfo">' + geography.properties.name + '\n\nNumber of Missions:' +  data.count + ' ';
                    } else {
                        return '<div class="hoverinfo">' + geography.properties.name + '\n\nNumber of Missions:' +  0 + ' ';
                    }
               }
            },
            projection: 'mercator',
            responsive: true,
            fills: this.state.fills,
            data: this.state.data
        }));
        this.map = map;
    }
    render() {
        const style = {
            width: '60%',
            height: '50%',
            position: "absolute",
            right: "135px",
            bottom: "110px"

        };
        return (<div ref="container" style={style}></div>)
    }
}

export default class MapView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    update = (region) => {
    }

    render() {
        return (
            <div className="App">
                <WorldDatamap/>
            </div>
        );
    }
}
