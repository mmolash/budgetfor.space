import React from 'react'
import DonutChart from 'react-d3-donut'
import axios from 'axios'

export default class DonutChartOther extends React.Component {
    constructor(props) {
        super(props);
        this.state= {
            data: {},
            maxIndex: 0,
            donutData: [],
            donutMap: new Map(),
            fakeData: [{count:20, name:"asd", color:"#000000", },{count:20, color:"#123123", name:"asd"},{count:20, name:"asd", color:"#5555555", },{count:20, color:"#abc123", name:"asd"}]
        }
        this.getData = this.getData.bind(this);
    }
    getData = (pagenum) => {
        return axios({
            method: 'get',
            url: "https://api.catchmeoutside.today/groups?page=" + (pagenum)
          })
          .then((response) => {
            if (response === undefined || response.data.length === 0) {
                return null; // base case
              }
            var newData = this.state.data;
            newData[pagenum] = response.data;
            this.setState({data: newData, maxIndex: pagenum});
            if (response.data.length > 0) {
                return "continue";
            } else {
                return null;
            }
          }).catch((error) => {
            return null;
          });

    }
    async gatherData() {
        const colors = ['#d32f2f', '#c2185b', '#7b1fa2', '#303f9f', '#64b5f6', '#00acc1', 
        '#00796b', '#1b5e20', '#cddc39', '#f9a825', '#f4511e', '#4e342e', '#d50000',
        '#f48fb1', '#e1bee7', '#673ab7', '#3f51b5', '#00bcd4', '#a5d6a7', '#aed581', '#827717',
        '#76ff03', '#ffeb3b', '#ff8f00', '#bf360c', '#3e2723', '#757575', '#bdbdbd']
        //console.log(this.state.data);
        var colorIndex = 0;
        for (var i = 1; i <= 9; i++) {
            var arr = this.state.data[i];
            for (var j = 0; j < arr.length; j++) {
                var obj = null;
                var curr = arr[j];
                if (this.state.donutMap.has(curr["categories"])) {
                    obj = this.state.donutMap.get(curr["categories"]);
                    obj["count"] += 1;
                } else {
                    obj = {}
                    obj["count"] = 1;
                    obj["name"] = curr["categories"];
                    obj["color"] = colors[colorIndex];
                    colorIndex++;
                    this.state.donutMap.set(curr["categories"], obj);
                }
            }
        }
    }
    async buildData() {
        console.log(this.state.donutMap);
        var it = this.state.donutMap.keys();
        var n = it.next().value;
        var donutData = []
        while (n !== undefined)  {
            //if (this.state.donutMap.get(n)["count"] > 10) {
                donutData.push(this.state.donutMap.get(n));
            //}
            n = it.next().value;
        }
        donutData.sort(function(a, b) {
            return b["count"] - a["count"];
        })
        this.setState({donutData: donutData});
        //console.log(this.state.donutData);
        //console.log(this.state.fakeData);
    }
    async componentDidMount() {
        var pages = [1,2,3,4,5,6,7,8,9,10];
        //await this.getData(pagenum)
        const promises = pages.map(page => this.getData(page));
        await Promise.all(promises);
        await this.gatherData();
        await this.buildData();
    }
    render() {
      if (this.state.donutData.length === 0) {
          return <div></div>
      }
      else {
          console.log ("updated");
          console.log (this.state.donutData);
      }
      return(<div>

      <DonutChart
        innerRadius={150}
        outerRadius={200}
        transition={true}
        svgClass="example6"
        pieClass="pie6"
        displayTooltip={true}
        strokeWidth={3}
        data={this.state.donutData}
      >
      </DonutChart>
      </div>
      )
    }
  }