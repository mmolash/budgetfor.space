dummy:
	echo hello

build-front:
	cd client; npm install
	cd client; npm run build

test-front:
	cd client; npm install
	cd client; npm test

test-back:
	cd server; pip3 install -r requirements.txt
	cd server/test; python3 apitests.py

test-postman:
	newman run postman.json

deploy:
	cd client; npm install
	cd client; npm run build
	cd client; npm run deploy
  
