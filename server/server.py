from flask import Flask, redirect
from flask import request
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
from sqlalchemy import create_engine

engine = create_engine(
    "postgresql+psycopg2://postgres_ro:budgetforspace@rds-postgresql-budgetforspace.cx3cgiza4dug.us-east-2.rds.amazonaws.com:5432/budgetforspace"
)
connection = engine.connect()

app = Flask(__name__)
api = Api(app)
CORS(app)

# contains API args that are used in all models
all_model_args = {
    "page": {"type": int, "help": "page must be an integer", "default": 1},
    "results-per-page": {
        "type": int,
        "help": "results-per-page must be an integer",
        "default": 12,
    },
}

# contains API args for the Organization model
orgs_unique_args = {
    "name": {"type": str},
    "nationality": {"type": str},
    "public": {"type": str, "choices": ["true", "false"]},
    "year-founded": {"type": str, "help": "years must be in the form yyyy"},
    "num-missions": {"type": str},
    "employees": {"type": str},
}

# contains API args for the Destination model
dests_unique_args = {
    "name": {"type": str},
    "type": {"type": str},
    "distance": {"type": str},
    "date-discovered": {"type": str},
    "composition": {"type": str},
    "radius": {"type": str},
}

# contains API args for the Mission model
missions_unique_args = {
    "name": {"type": str},
    "date": {"type": str, "help": "Date must be in the format yyyy"},
    "crew": {"type": str, "choices": ["true", "false"]},
    "status": {"type": str, "choices": ["ongoing", "past", "future"]},
    "rocket": {"type": str},
    "country": {"type": str},
}

# stores paths to all args
type_args = {
    "missions": missions_unique_args,
    "organizations": orgs_unique_args,
    "destinations": dests_unique_args,
}


def get_parsed_args(t):
    """
    adds all args for a model and parses them
    """

    parser = reqparse.RequestParser()
    args = type_args[t]  # get args for that specific model

    for arg_name in args:
        parser.add_argument(arg_name, **args[arg_name])  # add all model-specific args

    for arg_name in all_model_args:
        parser.add_argument(
            arg_name, **all_model_args[arg_name]
        )  # add all args that are common to all models

    return parser.parse_args()


def get_start_stop(args):
    """
    gets start and stop for the SQL call from the API options
    """

    start = (args["page"] - 1) * args["results-per-page"] + 1
    stop = start + args["results-per-page"] - 1
    return start, stop


def query_builder(model, conditionals, result_range):
    """
    builds the SQL query from the conditionals
    """
    return """with results as
(
	select row_number() over(order by id) as rownum, *
	from {} {}
)
select * from results where rownum between {} and {}
""".format(
        model, conditionals, result_range[0], result_range[1]
    )


def build_api_return_value(model, sql_result):
    """
    builds the JSON-style return value for the API call
    """
    data = {
        model: [
            {i: row[i] for i in row.keys() if i != "rownum" and i != "num-missions"}
            for row in sql_result
        ]
    }
    return data


class Organizations(Resource):
    def get(self):
        model = "organizations"
        args = get_parsed_args(model)

        conditionals = "where id > 0"

        # TODO: sanitize inputs
        # TODO: num-missions
        if args["name"] is not None:
            conditionals += " and name ILIKE '{}'".format(args["name"])
        if args["nationality"] is not None:
            conditionals += " and nationality ILIKE '{}'".format(args["nationality"])
        if args["public"] is not None:
            conditionals += " and public = {}".format(args["public"])
        if args["year-founded"] is not None:
            parsed = list(map(int, args["year-founded"].split(",")))
            if len(parsed) == 1:
                parsed += parsed
            conditionals += ' and "year-founded" between {} and {}'.format(
                min(parsed[0], parsed[1]), max(parsed[0], parsed[1])
            )
        if args["num-missions"] is not None:
            parsed = args["num-missions"].split(",")
            if len(parsed) == 1:
                parsed += parsed
            conditionals += " and cardinality(missions) between {} and {}".format(
                min(parsed[0], parsed[1]), max(parsed[0], parsed[1])
            )
        if args["employees"] is not None:
            parsed = list(map(int, args["employees"].split(",")))
            if len(parsed) == 1:
                parsed += parsed
            conditionals += ' and "employees" between {} and {}'.format(
                min(parsed[0], parsed[1]), max(parsed[0], parsed[1])
            )

        if conditionals == "where id > 0":
            conditionals = ""

        result_range = get_start_stop(args)  # returns (start, stop) tuple

        sql_query = query_builder(model, conditionals, result_range)

        result = connection.execute(sql_query)
        return build_api_return_value(model, result)


class Organization(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", type=int, required=True)
        args = parser.parse_args()

        result = connection.execute(
            "select * from organizations where id={}".format(args["id"])
        )
        return dict(result.first())

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", type=int)
        parser.add_argument("name")
        args = parser.parse_args()
        result = None
        if args["id"] is not None and args["name"] is not None:
            result = connection.execute(
                "select * from organizations where id={} and name='{}'".format(
                    args["id"], args["name"]
                )
            )
        elif args["id"] is not None:
            result = connection.execute(
                "select * from organizations where id={}".format(args["id"])
            )
        elif args["name"] is not None:
            result = connection.execute(
                "select * from organizations where name='{}'".format(args["name"])
            )
        else:
            return {"err_msg": "invalid input: no name or id provided"}
        try:
            organization = dict(result.first())
            if len(organization) == 0:
                raise Exception
            else:
                return organization
        except:
            return {
                "No Such Element": "A organization of that name and/or id was not found"
            }


class Destinations(Resource):
    def get(self):
        model = "destinations"
        args = get_parsed_args(model)

        conditionals = "where id > 0"

        # TODO: sanitize inputs
        # TODO: num-missions
        if args["name"] is not None:
            conditionals += " and name ILIKE '{}'".format(args["name"])
        if args["type"] is not None:
            conditionals += " and type = '{}'".format(args["type"])
        if args["composition"] is not None:
            conditionals += " and composition = '{}'".format(args["composition"])
        if args["distance"] is not None:
            parsed = list(map(int, args["distance"].split(",")))
            if len(parsed) == 1:
                parsed += parsed
            conditionals += " and distance between {} and {}".format(
                min(parsed[0], parsed[1]), max(parsed[0], parsed[1])
            )
        if args["radius"] is not None:
            parsed = list(map(int, args["radius"].split(",")))
            if len(parsed) == 1:
                parsed += parsed
            conditionals += " and radius between {} and {}".format(
                min(parsed[0], parsed[1]), max(parsed[0], parsed[1])
            )
        if args["date-discovered"] is not None:
            parsed = list(map(int, args["date-discovered"].split(",")))
            if len(parsed) == 1:
                parsed += parsed
            conditionals += ' and "date-discovered" between {} and {}'.format(
                min(parsed[0], parsed[1]), max(parsed[0], parsed[1])
            )

        if conditionals == "where id > 0":
            conditionals = ""

        result_range = get_start_stop(args)  # returns (start, stop) tuple

        sql_query = query_builder(model, conditionals, result_range)  # builds SQL query

        result = connection.execute(sql_query)
        return build_api_return_value(
            model, result
        )  # builds JSON return value from SQL response


class Destination(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", type=int, required=True)
        args = parser.parse_args()

        result = connection.execute(
            "select * from destinations where id={}".format(args["id"])
        )
        return dict(result.first())

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", type=int)
        parser.add_argument("name")
        args = parser.parse_args()
        result = None
        if args["id"] is not None and args["name"] is not None:
            result = connection.execute(
                "select * from destinations where id={} and name='{}'".format(
                    args["id"], args["name"]
                )
            )
        elif args["id"] is not None:
            result = connection.execute(
                "select * from destinations where id={}".format(args["id"])
            )
        elif args["name"] is not None:
            result = connection.execute(
                "select * from destinations where name='{}'".format(args["name"])
            )
        else:
            return {"err_msg": "invalid input: no name or id provided"}
        try:
            destination = dict(result.first())
            if len(destination) == 0:
                raise Exception
            else:
                return destination
        except:
            return {
                "No Such Element": "A destination of that name and/or id was not found"
            }


class Missions(Resource):
    def get(self):
        model = "missions"
        args = get_parsed_args(model)
        # TODO add parsing in actual results
        conditionals = "where id > 0"

        # TODO: sanitize inputs
        # TODO: num-missions
        if args["name"] is not None:
            conditionals += " and name ILIKE '{}'".format(args["name"])
        if args["crew"] is not None:
            conditionals += " and crew = {}".format(args["crew"])
        if args["status"] is not None:
            conditionals += " and status = '{}'".format(args["status"])
        if args["rocket"] is not None:
            conditionals += " and rocket ILIKE '{}'".format(args["rocket"])
        if args["country"] is not None:
            conditionals += " and country ILIKE '{}'".format(args["country"])
        if args["date"] is not None:
            parsed = list(map(int, args["date"].split(",")))
            if len(parsed) == 1:
                parsed += parsed
            conditionals += " and date between {} and {}".format(
                min(parsed[0], parsed[1]), max(parsed[0], parsed[1])
            )

        if conditionals == "where id > 0":
            conditionals = ""

        result_range = get_start_stop(args)  # returns (start, stop) tuple

        sql_query = query_builder(model, conditionals, result_range)

        result = connection.execute(sql_query)
        return build_api_return_value(model, result)


class Mission(Resource):
    def get(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", type=int, required=True)
        args = parser.parse_args()

        result = connection.execute(
            "select * from missions where id={}".format(args["id"])
        )
        return dict(result.first())

    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", type=int)
        parser.add_argument("name")
        args = parser.parse_args()
        result = None
        if args["id"] is not None and args["name"] is not None:
            result = connection.execute(
                "select * from missions where id={} and name='{}'".format(
                    args["id"], args["name"]
                )
            )
        elif args["id"] is not None:
            result = connection.execute(
                "select * from missions where id={}".format(args["id"])
            )
        elif args["name"] is not None:
            result = connection.execute(
                "select * from missions where name='{}'".format(args["name"])
            )
        else:
            return {"err_msg": "invalid input: no name or id provided"}
        try:
            mission = dict(result.first())
            if len(mission) == 0:
                raise Exception
            else:
                return mission
        except:
            return {"No Such Element": "A mission of that name and/or id was not found"}


class Test(Resource):
    def get(self):
        return {"test": "test"}


class HomePage(Resource):
    def get(self):
        return redirect("https://www.budgetfor.space/api", code=302)


api.add_resource(Destinations, "/destinations")
api.add_resource(Destination, "/destination")
api.add_resource(Organizations, "/organizations")
api.add_resource(Organization, "/organization")
api.add_resource(Missions, "/missions")
api.add_resource(Mission, "/mission")
api.add_resource(Test, "/test")
api.add_resource(HomePage, "/")

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
