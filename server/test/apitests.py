import sys

sys.path.append("../")

import unittest
from unittest.mock import patch, Mock, ANY
import server
import requests
from server import (
    Organizations,
    Organization,
    Missions,
    Mission,
    Destinations,
    Destination,
)


class ApiTests(unittest.TestCase):
    # Creator: Michael
    @patch("server.create_engine")
    def test_orgs_args_setup(self, mock_create):
        with patch("server.reqparse") as mock_parse:
            mock_parse.RequestParser.return_value.parse_args.return_value = {
                "name": None,
                "nationality": None,
                "public": None,
                "year-founded": None,
                "num-missions": None,
                "employees": None,
                "page": 1,
                "results-per-page": 12,
            }

            test = Organizations()
            test.get()

            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "name", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "nationality", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "public", type=str, choices=["true", "false"]
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "year-founded", type=str, help="years must be in the form yyyy"
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "num-missions", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "employees", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "page", type=int, help="page must be an integer", default=1
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "results-per-page",
                type=int,
                help="results-per-page must be an integer",
                default=12,
            )

    # Creator: Michael
    @patch("server.create_engine")
    def test_org_args_setup(self, mock_create):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": 1
                }

                test = Organization()
                test.get()

                mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                    "id", type=int, required=True
                )

    # Creator: Michael
    @patch("server.create_engine")
    def test_dests_args_setup(self, mock_create):
        with patch("server.reqparse") as mock_parse:
            mock_parse.RequestParser.return_value.parse_args.return_value = {
                "name": None,
                "type": None,
                "distance": None,
                "date-discovered": None,
                "composition": None,
                "radius": None,
                "page": 1,
                "results-per-page": 12,
            }

            test = Destinations()
            test.get()

            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "name", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "type", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "distance", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "date-discovered", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "composition", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "radius", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "page", type=int, help="page must be an integer", default=1
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "results-per-page",
                type=int,
                help="results-per-page must be an integer",
                default=12,
            )

    # Creator: Michael
    @patch("server.create_engine")
    def test_dest_args_setup(self, mock_create):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": 1
                }

                test = Destination()
                test.get()

                mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                    "id", type=int, required=True
                )

    # Creator: Michael
    @patch("server.create_engine")
    def test_missions_args_setup(self, mock_create):
        with patch("server.reqparse") as mock_parse:
            mock_parse.RequestParser.return_value.parse_args.return_value = {
                "name": None,
                "date": None,
                "crew": None,
                "status": None,
                "rocket": None,
                "country": None,
                "page": 1,
                "results-per-page": 12,
            }

            test = Missions()
            test.get()

            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "name", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "date", type=str, help="Date must be in the format yyyy"
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "crew", type=str, choices=["true", "false"]
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "status", type=str, choices=["ongoing", "past", "future"]
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "rocket", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "country", type=str
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "page", type=int, help="page must be an integer", default=1
            )
            mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                "results-per-page",
                type=int,
                help="results-per-page must be an integer",
                default=12,
            )

    # Creator: Michael
    @patch("server.create_engine")
    def test_mission_args_setup(self, mock_create):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": 1
                }

                test = Mission()
                test.get()

                mock_parse.RequestParser.return_value.add_argument.assert_any_call(
                    "id", type=int, required=True
                )

    # Creator: Michael
    def test_orgs_sql(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": "test name",
                    "nationality": "test country",
                    "public": "true",
                    "year-founded": "1909",
                    "num-missions": "10",
                    "employees": "100",
                    "page": 2,
                    "results-per-page": 5,
                }

                test = Organizations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from organizations where id > 0 and name ILIKE 'test name' and nationality ILIKE 'test country' and public = true and "year-founded" between 1909 and 1909 and cardinality(missions) between 10 and 10 and "employees" between 100 and 100
)
select * from results where rownum between 6 and 10
"""
                )

    # Creator: Michael
    def test_orgs_sql(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": "test name",
                    "nationality": "test country",
                    "public": "true",
                    "year-founded": "1909",
                    "num-missions": "10",
                    "employees": "100",
                    "page": 2,
                    "results-per-page": 5,
                }

                test = Organizations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from organizations where id > 0 and name ILIKE 'test name' and nationality ILIKE 'test country' and public = true and "year-founded" between 1909 and 1909 and cardinality(missions) between 10 and 10 and "employees" between 100 and 100
)
select * from results where rownum between 6 and 10
"""
                )

    # Creator: Michael
    def test_org_sql(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": 10101
                }

                test = Organization()
                test.get()

                mock_connection.execute.assert_called_with(
                    "select * from organizations where id=10101"
                )

    # Creator: Michael
    def test_dests_sql(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": "test dest",
                    "type": "planet",
                    "distance": "999,1001",
                    "date-discovered": "2001,100",
                    "composition": "vacuum",
                    "radius": "1",
                    "page": 20,
                    "results-per-page": 1,
                }

                test = Destinations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from destinations where id > 0 and name ILIKE 'test dest' and type = 'planet' and composition = 'vacuum' and distance between 999 and 1001 and radius between 1 and 1 and "date-discovered" between 100 and 2001
)
select * from results where rownum between 20 and 20
"""
                )

    # Creator: Michael
    def test_dest_sql(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": 10102
                }

                test = Destination()
                test.get()

                mock_connection.execute.assert_called_with(
                    "select * from destinations where id=10102"
                )

    # Creator: Michael
    def test_missions_sql(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": "test miss",
                    "date": "101,101",
                    "crew": "true",
                    "status": "future",
                    "rocket": "Saturn V",
                    "country": "United States",
                    "page": 1,
                    "results-per-page": 12,
                }

                test = Missions()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from missions where id > 0 and name ILIKE 'test miss' and crew = true and status = 'future' and rocket ILIKE 'Saturn V' and country ILIKE 'United States' and date between 101 and 101
)
select * from results where rownum between 1 and 12
"""
                )

    # Creator: Michael
    def test_mission_sql(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": 1
                }

                test = Mission()
                test.get()

                mock_connection.execute.assert_called_with(
                    "select * from missions where id=1"
                )

    # Creator: Sahil
    def test_mission_name(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": "JUICE (JUpiter ICy moons Explorer)",
                    "date": None,
                    "crew": None,
                    "status": None,
                    "rocket": None,
                    "country": None,
                    "page": 1,
                    "results-per-page": 12,
                }

                test = Missions()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from missions where id > 0 and name ILIKE 'JUICE (JUpiter ICy moons Explorer)'
)
select * from results where rownum between 1 and 12
"""
                )

    # Creator: Sahil
    def test_mission_crew(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "date": None,
                    "crew": "true",
                    "status": None,
                    "rocket": None,
                    "country": None,
                    "page": 1,
                    "results-per-page": 12,
                }

                test = Missions()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from missions where id > 0 and crew = true
)
select * from results where rownum between 1 and 12
"""
                )

    # Creator: Sahil
    def test_mission_status_ongoing(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "date": None,
                    "crew": None,
                    "status": "ongoing",
                    "rocket": None,
                    "country": None,
                    "page": 1,
                    "results-per-page": 12,
                }

                test = Missions()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from missions where id > 0 and status = 'ongoing'
)
select * from results where rownum between 1 and 12
"""
                )

    # Creator: Sahil
    def test_mission_status_past(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "date": None,
                    "crew": None,
                    "status": "past",
                    "rocket": None,
                    "country": None,
                    "page": 1,
                    "results-per-page": 12,
                }

                test = Missions()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from missions where id > 0 and status = 'past'
)
select * from results where rownum between 1 and 12
"""
                )

    # Creator: Sahil
    def test_mission_status_future(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "date": None,
                    "crew": None,
                    "status": "future",
                    "rocket": None,
                    "country": None,
                    "page": 1,
                    "results-per-page": 12,
                }

                test = Missions()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from missions where id > 0 and status = 'future'
)
select * from results where rownum between 1 and 12
"""
                )

    # Creator: Sahil
    def test_mission_status_rocket(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "date": None,
                    "crew": None,
                    "status": None,
                    "rocket": "Saturn V",
                    "country": None,
                    "page": 1,
                    "results-per-page": 12,
                }

                test = Missions()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from missions where id > 0 and rocket ILIKE 'Saturn V'
)
select * from results where rownum between 1 and 12
"""
                )

    # Creator: Sahil
    def test_mission_status_country(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "date": None,
                    "crew": None,
                    "status": None,
                    "rocket": None,
                    "country": "China",
                    "page": 1,
                    "results-per-page": 12,
                }

                test = Missions()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from missions where id > 0 and country ILIKE 'China'
)
select * from results where rownum between 1 and 12
"""
                )

    # Creator: Sahil
    def test_mission_status_date(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "date": "2012,2013",
                    "crew": None,
                    "status": None,
                    "rocket": None,
                    "country": None,
                    "page": 1,
                    "results-per-page": 12,
                }

                test = Missions()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from missions where id > 0 and date between 2012 and 2013
)
select * from results where rownum between 1 and 12
"""
                )

    # Creator: Sahil
    def test_organization_name(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": "test name",
                    "nationality": None,
                    "public": None,
                    "year-founded": None,
                    "num-missions": None,
                    "employees": None,
                    "page": 2,
                    "results-per-page": 5,
                }

                test = Organizations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from organizations where id > 0 and name ILIKE 'test name'
)
select * from results where rownum between 6 and 10
"""
                )

    # Creator: Sahil
    def test_organization_nationality(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "nationality": "China",
                    "public": None,
                    "year-founded": None,
                    "num-missions": None,
                    "employees": None,
                    "page": 2,
                    "results-per-page": 5,
                }

                test = Organizations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from organizations where id > 0 and nationality ILIKE 'China'
)
select * from results where rownum between 6 and 10
"""
                )

    # Creator: Sahil
    def test_organization_public(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "nationality": None,
                    "public": "true",
                    "year-founded": None,
                    "num-missions": None,
                    "employees": None,
                    "page": 2,
                    "results-per-page": 5,
                }

                test = Organizations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from organizations where id > 0 and public = true
)
select * from results where rownum between 6 and 10
"""
                )

    # Creator: Sahil
    def test_organization_year_founded(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "nationality": None,
                    "public": None,
                    "year-founded": "1909",
                    "num-missions": None,
                    "employees": None,
                    "page": 2,
                    "results-per-page": 5,
                }

                test = Organizations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from organizations where id > 0 and "year-founded" between 1909 and 1909
)
select * from results where rownum between 6 and 10
"""
                )

    # Creator: Sahil
    def test_organization_num_missions(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "nationality": None,
                    "public": None,
                    "year-founded": None,
                    "num-missions": "10",
                    "employees": None,
                    "page": 2,
                    "results-per-page": 5,
                }

                test = Organizations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from organizations where id > 0 and cardinality(missions) between 10 and 10
)
select * from results where rownum between 6 and 10
"""
                )

    # Creator: Sahil
    def test_organization_employees(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "nationality": None,
                    "public": None,
                    "year-founded": None,
                    "num-missions": None,
                    "employees": "10",
                    "page": 2,
                    "results-per-page": 5,
                }

                test = Organizations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from organizations where id > 0 and "employees" between 10 and 10
)
select * from results where rownum between 6 and 10
"""
                )

    # Creator: Sahil
    def test_missions_default_get(self):
        response = requests.get("http://api.budgetfor.space/missions")
        self.assertEqual(len(response.json()["missions"]), 12)

    # Creator: Sahil
    def test_missions_get_name(self):
        response = requests.get(
            "http://api.budgetfor.space/missions?name=JUICE%20(JUpiter%20ICy%20moons%20Explorer)"
        )
        self.assertEqual(len(response.json()["missions"]), 1)

    # Creator: Bill
    def test_destinations_default_get(self):
        response = requests.get("http://api.budgetfor.space/destinations")
        self.assertGreater(len(response.json()["destinations"]), 0)

    # Creator: Bill
    def test_destinations_get_name(self):
        response = requests.get("http://api.budgetfor.space/destinations?name=Achiras")
        self.assertEqual(len(response.json()["destinations"]), 1)

    # Creator Bill
    def test_destination_type(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "type": "planet",
                    "distance": None,
                    "composition": None,
                    "radius": None,
                    "date-discovered": None,
                    "page": 1,
                    "results-per-page": 5,
                }

                test = Destinations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from destinations where id > 0 and type = 'planet'
)
select * from results where rownum between 1 and 5
"""
                )

    # Creator Bill
    def test_destination_distance(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "type": None,
                    "distance": "35786",
                    "composition": None,
                    "radius": None,
                    "date-discovered": None,
                    "page": 2,
                    "results-per-page": 1,
                }

                test = Destinations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from destinations where id > 0 and distance between 35786 and 35786
)
select * from results where rownum between 2 and 2
"""
                )

    # Creator Bill
    def test_destination_composition(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "type": None,
                    "distance": None,
                    "composition": "hydrogen",
                    "radius": None,
                    "date-discovered": None,
                    "page": 1,
                    "results-per-page": 20,
                }

                test = Destinations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from destinations where id > 0 and composition = 'hydrogen'
)
select * from results where rownum between 1 and 20
"""
                )

    # Creator Bill
    def test_destination_radius(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "type": None,
                    "distance": None,
                    "composition": None,
                    "radius": "22387",
                    "date-discovered": None,
                    "page": 1,
                    "results-per-page": 20,
                }

                test = Destinations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from destinations where id > 0 and radius between 22387 and 22387
)
select * from results where rownum between 1 and 20
"""
                )

    # Creator Bill
    def test_destination_date_discovered(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": None,
                    "type": None,
                    "distance": None,
                    "composition": None,
                    "radius": None,
                    "date-discovered": "2013",
                    "page": 1,
                    "results-per-page": 20,
                }

                test = Destinations()
                test.get()

                mock_connection.execute.assert_called_with(
                    """with results as
(
	select row_number() over(order by id) as rownum, *
	from destinations where id > 0 and "date-discovered" between 2013 and 2013
)
select * from results where rownum between 1 and 20
"""
                )

    # Creator: Bill
    def test_mission_post_id_name(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": "SpX CRS-1",
                    "id": "4",
                }

                test = Mission()
                test.post()

                mock_connection.execute.assert_called_with(
                    """select * from missions where id=4 and name='SpX CRS-1'"""
                )

    # Creator: Bill
    def test_mission_post_id(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": "4",
                    "name": None,
                }

                test = Mission()
                test.post()

                mock_connection.execute.assert_called_with(
                    """select * from missions where id=4"""
                )

    # Creator: Bill
    def test_mission_post_name(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": None,
                    "name": "SpX CRS-1",
                }

                test = Mission()
                test.post()

                mock_connection.execute.assert_called_with(
                    """select * from missions where name='SpX CRS-1'"""
                )

    # Creator: Bill
    def test_mission_post_empty(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": None,
                    "name": None,
                }

                test = Mission()
                response = test.post()

                self.assertEqual(
                    response["err_msg"], "invalid input: no name or id provided"
                )

    def test_mission_post_notfound(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": 100,
                    "name": "xxxxxxxxxx",
                }

                test = Mission()
                response = test.post()

                self.assertEqual(
                    response["No Such Element"],
                    "A mission of that name and/or id was not found",
                )

    # Creator: Bill
    def test_destination_post_id_name(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "name": "Kepler-96 b",
                    "id": "16",
                }

                test = Destination()
                test.post()

                mock_connection.execute.assert_called_with(
                    """select * from destinations where id=16 and name='Kepler-96 b'"""
                )

    # Creator: Bill
    def test_destination_post_id(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": "16",
                    "name": None,
                }

                test = Destination()
                test.post()

                mock_connection.execute.assert_called_with(
                    """select * from destinations where id=16"""
                )

    # Creator: Bill
    def test_destination_post_name(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": None,
                    "name": "Kepler-96 b",
                }

                test = Destination()
                test.post()

                mock_connection.execute.assert_called_with(
                    """select * from destinations where name='Kepler-96 b'"""
                )

    # Creator: Bill
    def test_destination_post_empty(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": None,
                    "name": None,
                }

                test = Destination()
                response = test.post()

                self.assertEqual(
                    response["err_msg"], "invalid input: no name or id provided"
                )

    def test_destination_post_notfound(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": 100,
                    "name": "xxxxxxxxxx",
                }

                test = Destination()
                response = test.post()

                self.assertEqual(
                    response["No Such Element"],
                    "A destination of that name and/or id was not found",
                )

    # Creator: Bill
    def test_organization_post_name(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": None,
                    "name": "SpX CRS-1",
                }

                test = Organization()
                test.post()

                mock_connection.execute.assert_called_with(
                    """select * from organizations where name='SpX CRS-1'"""
                )

    # Creator: Bill
    def test_organization_post_empty(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": None,
                    "name": None,
                }

                test = Organization()
                response = test.post()

                self.assertEqual(
                    response["err_msg"], "invalid input: no name or id provided"
                )

    def test_organization_post_notfound(self):
        with patch("server.reqparse") as mock_parse:
            with patch.object(server, "connection") as mock_connection:
                mock_parse.RequestParser.return_value.parse_args.return_value = {
                    "id": 100,
                    "name": "xxxxxxxxxx",
                }

                test = Organization()
                response = test.post()

                self.assertEqual(
                    response["No Such Element"],
                    "A organization of that name and/or id was not found",
                )


if __name__ == "__main__":
    unittest.main()
