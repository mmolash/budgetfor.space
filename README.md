# budgetfor.space
A website dedicated to increasing public awareness about the necessity of funding space exploration  
API Documentation: https://documenter.getpostman.com/view/6487730/S11LrcXh

Git SHA for grading: f523989b324552e1a37e4820e851fe97dcf9faaf

Frontend: 'client/src/index.js'  
Backend: 'server/server.py'  

# Member 1
Name: Alan Zeng  
EID: az6746  
GitLab ID: alanzeng17  
Estimated Completion Time: 12 hours  
Actual completion time: 5 hours
Comments: N/A  

# Member 2
Name: Sahil Parikh  
EID: sp39827  
GitLab ID: sahilparikh  
Estimated Completion Time: 10 hours  
Actual completion time: 3 hours
Comments: N/A  

# Member 3
Name: Michael Molash  
EID: mdm5382  
GitLab ID: mmolash  
Estimated Completion Time: 12 hours  
Actual Completion Time: 5 hours
Comments: N/A  

# Member 4
Name: Anthony Liu  
EID: azl83  
GitLab ID: anthony.liu  
Estimated Completion Time: 10 hours  
Actual Completion Time: 3 hours
Comments: N/A  

# Member 5
Name: Bill Yang  
EID: bly263  
GitLab ID: billyang98  
Estimated Completion Time: 11 hours  
Actual Completion Time: 3 hours
Comments: N/A  

Link to website: https://budgetfor.space  
Link to GitLab Pipelines: https://gitlab.com/mmolash/budgetfor.space/pipelines  
Path to front-end code: client/  
Path to back-end server: server/server.py  
