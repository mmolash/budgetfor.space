import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# import page
import time


class Tests(unittest.TestCase):
    url = "https://www.budgetfor.space"
    about_url = "https://www.budgetfor.space/about"
    orgs_url = "https://www.budgetfor.space/organizations"
    dests_url = "https://www.budgetfor.space/destinations"
    missions_url = "https://www.budgetfor.space/missions"
    api_url = "https://www.budgetfor.space/api"

    def setUp(self):
        self.driver = webdriver.Chrome("./chromedriver")

    # Creator: Michael
    def test_home_title(self):
        self.driver.get(self.url)
        self.assertIn("Budget For Space", self.driver.title)

    # Creator: Michael
    def test_missions_nav(self):
        self.driver.get(self.missions_url)
        link = self.driver.find_element_by_link_text("Missions")
        self.assertIsNotNone(link)
        link.click()
        self.assertEqual("missions", self.driver.current_url[-8:])

    # Creator: Michael
    def test_orgs_nav(self):
        self.driver.get(self.orgs_url)
        link = self.driver.find_element_by_link_text("Organizations")
        self.assertIsNotNone(link)
        link.click()
        self.assertEqual("organizations", self.driver.current_url[-13:])

    # Creator: Michael
    def test_dests_nav(self):
        self.driver.get(self.dests_url)
        link = self.driver.find_element_by_link_text("Destinations")
        self.assertIsNotNone(link)
        link.click()
        self.assertEqual("destinations", self.driver.current_url[-12:])

    # Creator: Michael
    def test_about_nav(self):
        self.driver.get(self.about_url)
        link = self.driver.find_element_by_link_text("About")
        self.assertIsNotNone(link)
        link.click()
        self.assertEqual("about", self.driver.current_url[-5:])

    # Creator: Anthony
    def test_api_nav(self):
        self.driver.get(self.api_url)
        link = self.driver.find_element_by_link_text("API")
        self.assertIsNotNone(link)
        link.click()
        self.assertEqual("api", self.driver.current_url[-3:])

    # Creator: Michael
    def test_home_navbar(self):
        self.driver.get(self.url)
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Creator: Michael
    def test_missions_navbar(self):
        self.driver.get(self.missions_url)
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Creator: Michael
    def test_orgs_navbar(self):
        self.driver.get(self.orgs_url)
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Creator: Michael
    def test_dests_navbar(self):
        self.driver.get(self.dests_url)
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Creator: Michael
    def test_about_navbar(self):
        self.driver.get(self.about_url)
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Creator: Anthony
    def test_api_navbar(self):
        self.driver.get(self.api_url)
        nav = self.driver.find_element_by_tag_name("nav")
        self.assertIsNotNone(nav)

    # Creator: Anthony
    def test_home_imgs(self):
        self.driver.get(self.url)
        image = self.driver.find_element_by_tag_name("img")
        self.assertIsNotNone(image)

    # Creator: Michael
    def test_missions_imgs(self):
        self.driver.get(self.missions_url)
        image = self.driver.find_element_by_tag_name("img")
        self.assertIsNotNone(image)

    # Creator: Michael
    def test_orgs_imgs(self):
        self.driver.get(self.orgs_url)
        image = self.driver.find_element_by_tag_name("img")
        self.assertIsNotNone(image)

    # Creator: Michael
    def test_dests_imgs(self):
        self.driver.get(self.dests_url)
        image = self.driver.find_element_by_tag_name("img")
        self.assertIsNotNone(image)

    # Creator: Anthony
    def test_about_imgs(self):
        self.driver.get(self.about_url)
        image = self.driver.find_element_by_tag_name("img")
        self.assertIsNotNone(image)

    # Creator: Anthony
    def test_api_imgs(self):
        self.driver.get(self.api_url)
        image = self.driver.find_element_by_tag_name("img")
        self.assertIsNotNone(image)

    # Creator: Anthony
    def test_button(self):
        self.driver.get(self.url)
        button = self.driver.find_element_by_class_name("btn-primary")
        button.click()
        self.assertEqual("https://www.budgetfor.space/about", self.driver.current_url)

    # Creator: Anthony
    def test_nav_home_button(self):
        self.driver.get(self.url)
        # XPath returns results in a list, so get the first element (only expecting one result).
        home_link = self.driver.find_elements_by_xpath("//a[@href='/']")
        home_link[0].click()
        self.assertEqual("https://www.budgetfor.space/", self.driver.current_url)

    # Creator: Anthony
    def test_nav_missions_button(self):
        self.driver.get(self.url)
        # XPath returns results in a list, so get the first element (only expecting one result).
        missions_link = self.driver.find_elements_by_xpath("//a[@href='/missions']")
        missions_link[0].click()
        self.assertEqual(
            "https://www.budgetfor.space/missions", self.driver.current_url
        )

    # Creator: Anthony
    def test_nav_orgs_button(self):
        self.driver.get(self.url)
        # XPath returns results in a list, so get the first element (only expecting one result).
        orgs_link = self.driver.find_elements_by_xpath("//a[@href='/organizations']")
        orgs_link[0].click()
        self.assertEqual(
            "https://www.budgetfor.space/organizations", self.driver.current_url
        )

    # Creator: Anthony
    def test_nav_dest_button(self):
        self.driver.get(self.url)
        # XPath returns results in a list, so get the first element (only expecting one result).
        dest_link = self.driver.find_elements_by_xpath("//a[@href='/destinations']")
        dest_link[0].click()
        self.assertEqual(
            "https://www.budgetfor.space/destinations", self.driver.current_url
        )

    # Creator: Anthony
    def test_nav_about_button(self):
        self.driver.get(self.url)
        # XPath returns results in a list, so get the first element (only expecting one result).
        about_link = self.driver.find_elements_by_xpath("//a[@href='/about']")
        about_link[0].click()
        self.assertEqual("https://www.budgetfor.space/about", self.driver.current_url)

    # Creator: Anthony
    def test_nav_api_button(self):
        self.driver.get(self.url)
        # XPath returns results in a list, so get the first element (only expecting one result).
        api_link = self.driver.find_elements_by_xpath("//a[@href='/api']")
        api_link[0].click()
        self.assertEqual("https://www.budgetfor.space/api", self.driver.current_url)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
